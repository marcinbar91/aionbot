﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using MB.GetKey;
namespace BOT
{
    public class Potion
    {
        static int skillbar;
        static string key;
        static string effect;
        static int percent;
        static int cooldown;
        static int casttime;
        static DateTime date;




        public static async Task Use()
        {
            for (int rows = 0; rows < Program.bot.DGVpotion.Rows.Count; rows++)
            {
                ReadPotionDataGridView(rows);
                if (effect == "HP")
                    if (Calc.Percent(Value.Player.HP.Cur, Value.Player.HP.Max) <= percent)
                        if (cooldown < Calc.Subtract(date.AddSeconds(1)))
                        {
                            int temp = Value.Player.HP.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = skillbar;
                                Key.KeyPress(key);
                                await Task.Delay(50);
                            } while (Value.Player.HP.Cur <= temp);
                            Log.Write("Potion - HP", Enums.Colors.Use);
                            await Task.Delay(casttime * 1000);
                            Program.bot.DGVpotion.Rows[rows].Cells[4].Value = DateTime.Now;
                        }
                if (effect == "MP")
                    if (Calc.Percent(Value.Player.MP.Cur, Value.Player.MP.Max) <= percent)
                        if (cooldown < Calc.Subtract(date.AddSeconds(1)))
                        {
                            int temp = Value.Player.MP.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = skillbar;
                                Key.KeyPress(key);
                                await Task.Delay(50);
                            } while (Value.Player.MP.Cur <= temp);
                            Log.Write("Potion - MP", Enums.Colors.Use);
                            await Task.Delay(casttime * 1000);
                            Program.bot.DGVpotion.Rows[rows].Cells[4].Value = DateTime.Now;
                        }
                if (effect == "Vigor")
                    if (Calc.Percent(Value.Player.HP.Cur, Value.Player.HP.Max) < percent && Calc.Percent(Value.Player.MP.Cur, Value.Player.MP.Max) < percent)
                        if (cooldown < Calc.Subtract(date.AddSeconds(1)))
                        {
                            int temp = Value.Player.HP.Cur;
                            int temp2 = Value.Player.MP.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = skillbar;
                                Key.KeyPress(key);
                                await Task.Delay(50);
                            } while (Value.Player.HP.Cur <= temp && Value.Player.MP.Cur <= temp2);
                            Log.Write("Potion - Vigor", Enums.Colors.Use);
                            await Task.Delay(casttime * 1000);
                            Program.bot.DGVpotion.Rows[rows].Cells[4].Value = DateTime.Now;
                        }
                if (effect == "Flight")
                    if (Calc.Percent(Value.Player.Flight.Cur, Value.Player.Flight.Max) <= percent)
                        if (cooldown < Calc.Subtract(date.AddSeconds(1)))
                        {
                            int temp = Value.Player.Flight.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = skillbar;
                                Key.KeyPress(key);
                                await Task.Delay(50);
                            } while (Value.Player.Flight.Cur <= temp);
                            Log.Write("Potion - Flight", Enums.Colors.Use);
                            await Task.Delay(casttime * 1000);
                            Program.bot.DGVpotion.Rows[rows].Cells[4].Value = DateTime.Now;
                        }
            }
        }

        private static void ReadPotionDataGridView(int row)
        {
            skillbar = Convert.ToInt32(Program.bot.DGVpotion.Rows[row].Cells[0].Value.ToString());
            key = Program.bot.DGVpotion.Rows[row].Cells[1].Value.ToString();
            cooldown = Convert.ToInt32(Program.bot.DGVpotion.Rows[row].Cells[2].Value);
            casttime = (int)Convert.ToInt32(Program.bot.DGVpotion.Rows[row].Cells[3].Value);
            date = Convert.ToDateTime(Program.bot.DGVpotion.Rows[row].Cells[4].Value.ToString());
            effect = (string)Program.bot.DGVpotion.Rows[row].Cells[5].Value;
            percent = Convert.ToInt32(Program.bot.DGVpotion.Rows[row].Cells[6].Value);


        }

    }
}
