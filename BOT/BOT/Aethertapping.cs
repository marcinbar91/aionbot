﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BOT
{
    public class Aethertapping
    {
        public static List<PointXYZ> ListBoxToList(System.Windows.Forms.ListBox listbox)
        {
            List<PointXYZ> arr = new List<PointXYZ>();
            for (int i = 0; i < listbox.Items.Count; i++)
            {
                string[] par = listbox.Items[i].ToString().Split(';');
                arr.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
            }
            return arr;
        }

        public static List<PointXYZ> CreatListFromFile(string fileName)
        {
            List<PointXYZ> script = new List<PointXYZ>();
            string[] lines = File.ReadAllLines(fileName);
            foreach (string line in lines)
            {
                if (GetCommand(line).Contains("go"))
                {
                    string[] par = GetParameters(line);
                    script.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
                }
            }
            return script;
        }

        public static ScriptItem CreatePathToSafeSpot(List<PointXYZ> script, List<PointXYZ> safeList)
        {
            List<PointXYZ> list = new List<PointXYZ>();
            List<PointXYZ> list2 = new List<PointXYZ>();

            PointXYZ character = new PointXYZ(Value.PlayerPosX, Value.PlayerPosY, Value.PlayerPosZ);
            PointXYZ safe = FindNearPoint(safeList, character);
            PointXYZ nearScript = FindNearPoint(script, character);
            PointXYZ safeScript = FindNearPoint(script, safe);

            
            int indexNearScript = IndexOf(script, nearScript);
            int indexsafeScript = IndexOf(script, safeScript);

            if (indexsafeScript - indexNearScript == 0)
            {
                list.Add(safe);
                return new list;
            }

            for (int i = indexNearScript; i < script.Count; i++)
            {
                list.Add(script[i]);
                if (safeScript.Compare(script[i]))
                {
                    list.Add(safe);
                    break;
                }

                if (i == script.Count - 1 && !Program.bot.CBaetherReturn.Checked)
                    i = -1;
                else if (i == script.Count - 1)
                    list.Clear();
            }

            for (int i = indexNearScript; i >= 0; i--)
            {
                list2.Add(script[i]);
                if (safeScript.Compare(script[i]))
                {
                    list2.Add(safe);
                    break;
                }

                if (i == 0 && !Program.bot.CBaetherReturn.Checked)
                    i = script.Count;
                else if (i == 0)
                    list2.Clear();
            }

            if (!list2.Any() || (list.Count < list2.Count && list.Any()))
                return list;
            else
                return list2;
        }









        public static int IndexOf(List<PointXYZ> script, PointXYZ safeScript)
        {
            foreach (var item in script)
            {
                if (item.X == safeScript.X && item.Y == safeScript.Y && item.Z == safeScript.Z)
                {
                    return script.IndexOf(item);
                }
            }
            return -1;
        }

        public static PointXYZ FindNearPoint(List<PointXYZ> script, PointXYZ nearsafe)
        {
            PointXYZ near = new PointXYZ();
            float dis = float.MaxValue;

            foreach (PointXYZ scripts in script)
            {
                float dis2 = scripts.Distance(nearsafe.X, nearsafe.Y, nearsafe.Z);
                if (dis > dis2)
                {
                    dis = dis2;
                    near = scripts;
                }
            }
            return near;
        }




        private static string[] GetParameters(string s)
        {
            string parameters = Regex.Match(s, @"(?<=\()(.*?)(?=\))").ToString();

            return parameters.Split(';');
        }

        private static string GetCommand(string s)
        {
            return Regex.Match(s, @"[^(\)]*").ToString();
        }
    }

}
