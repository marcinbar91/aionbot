﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOT
{
    public class Hunting
    {
        private static bool shouldStop = false;

        public static async Task Go(float xCoord, float yCoord, float zCoord)
        {
            double dis = Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, xCoord, yCoord);
            while (dis > 5 && Bot.Status != Enums.BotStatus.Idle)
            {
                Key.SelectTarget();
                await Task.Delay(100);
                if (Value.Target.HP.Cur == 0 && Bot.Status != Enums.BotStatus.Idle)
                {
                    await Buff.Use();
                    await Potion.Use();
                    Key.GoForward();
                    dis = Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, xCoord, yCoord);
                    Value.Player.Camera.Set(xCoord, yCoord);
                    await Task.Delay(100);
                }
                else
                {
                    await Kill();
                    await GetLoot();
                }
            }
        }

        private static async Task Kill()
        {
            Key.GoForward(false);
            while (Value.Target.HP.Cur != 0 && Bot.Status != Enums.BotStatus.Idle)
            {
                await Buff.Use();
                await Potion.Use();
                await Skill.Use2();
                await Task.Delay(1);
            }
        }

        private static async Task GetLoot()
        {
            while (Value.Target.Has == 1 && Value.Target.HP.Cur == 0 && Properties.Settings.Default.GetLoot && Bot.Status != Enums.BotStatus.Idle)
            {
                Key.Loot();
                await Task.Delay(100);
            }
        }
    }
}
