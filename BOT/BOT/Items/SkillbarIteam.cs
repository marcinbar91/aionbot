﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOT
{
    public class SkillbarIteam
    {
        public List<int> ID = new List<int>();

        public List<int> Row = new List<int>();
        public List<int> Colummn = new List<int>();
        public List<int> Skillbar = new List<int>();

        public List<string> Name = new List<string>();
        public List<string> Image = new List<string>();
        public List<string> Type = new List<string>();
        public List<int> CastTime = new List<int>();
        public List<int> Cooldown = new List<int>();
        public List<int> UsageCost = new List<int>();
        public List<string> UsageType = new List<string>();



        public SkillbarIteam()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            string[] AllLines = File.ReadAllLines("skills.txt");
            for (int skillbar = 1; skillbar <= 10; skillbar++)
            {
                for (int row = 1; row <= 3; row++)
                {
                    for (int column = 1; column <= 12; column++)
                    {

                        int i = Value.Skillbar.GetID(skillbar, row, column);
                        if (i != 0)
                        {
                            ID.Add(i);
                            Colummn.Add(column);
                            Row.Add(row);
                            Skillbar.Add(skillbar);
                            Parallel.For(0, AllLines.Length, (x, loopState) =>
                            {
                                string[] str = AllLines[x].Split(',');
                                if (ID.Last() == Convert.ToInt32(str[0]))
                                {
                                    Name.Add(str[1]);
                                    Image.Add(str[2]);
                                    Type.Add(str[3]);
                                    CastTime.Add(Convert.ToInt32(str[4]));
                                    Cooldown.Add(Convert.ToInt32(str[5]));
                                    UsageCost.Add(Convert.ToInt32(str[6]));
                                    UsageType.Add(str[7]);
                                    loopState.Break();
                                }
                            });
                        }
                    }
                }
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
        }
    }
}