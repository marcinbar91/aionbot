﻿namespace BOT
{
    public class Ability
    {
        public uint Address { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }

        public uint LastUseTimestamp { get; set; }

        public int Cooldown { get; set; }

        public uint CooldownEnd { get; set; }

        public int CastTime { get; set; }

        public Ability(uint address, int id, string name, uint lastUseTimestamp, int cooldown, uint cooldownEnd, int castTime)
        {
            Address = address;
            ID = id;
            Name = name;
            LastUseTimestamp = lastUseTimestamp;
            Cooldown = cooldown;
            CooldownEnd = cooldownEnd;
            CastTime = castTime;
        }
    }
}