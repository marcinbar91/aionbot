﻿namespace BOT
{
    public class InventoryItem
    {
        public uint Address { get; set; }

        public string Name { get; set; }

        public uint ID { get; set; }

        public int Count { get; set; }

        public InventoryItem(uint address, string name, uint id, int count)
        {
            Address = address;
            Name = name;
            ID = id;
            Count = count;
        }
    }
}