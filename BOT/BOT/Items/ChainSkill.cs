﻿namespace BOT
{
    public class ChainSkill
    {
        public uint Address { get; set; }

        public int AbilityID { get; set; }

        public bool IsElapsed { get; set; }

        public ChainSkill(uint address, int abilityID, bool isElapsed)
        {
            Address = address;
            AbilityID = abilityID;
            IsElapsed = isElapsed;
        }
    }
}