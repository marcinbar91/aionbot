﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOT
{
    class RefreshForm
    {
        private static byte pdp = 0;
        private static byte pclass;

        public static void Do()
        {
            Program.bot.UIThread(delegate
            {
                Program.bot.LBloggin.Text = "Loggin " + Value.Player.Loggin;

                GetClass(Value.Player.Class);

                string gb = Value.Player.Name + "-" + Value.Player.Lvl;
                if (Program.bot.GBplayer.Text != gb)
                    Program.bot.GBplayer.Text = gb;

                if (Value.Target.Has == 1)
                {
                    string gbt = Value.Target.Name + "-" + Value.Target.Lvl + "    Distance: " + Value.Target.Distance;
                    if (Program.bot.GBtarget.Text != gbt)
                        Program.bot.GBtarget.Text = gbt;
                }
                else
                    if (Program.bot.GBtarget.Text != "No Target")
                        Program.bot.GBtarget.Text = "No Target";

                Program.bot.LBflytype.Text = "Fly Stat: " + Value.Player.Flight.Type;

                Program.bot.DRWhp.Maximum = Value.Player.HP.Max;
                Program.bot.DRWhp.Value = Value.Player.HP.Cur;
                Program.bot.DRWmp.Maximum = Value.Player.MP.Max;
                Program.bot.DRWmp.Value = Value.Player.MP.Cur;
                Program.bot.DRWdp.Maximum = Value.Player.DP.Max;
                Program.bot.DRWdp.Value = Value.Player.DP.Cur;
                Program.bot.DRWexp.Maximum = Value.Player.EXP.Max;
                Program.bot.DRWexp.Value = Value.Player.EXP.Cur;
                Program.bot.DRWfly.Maximum = Value.Player.Flight.Max;
                Program.bot.DRWfly.Value = Value.Player.Flight.Cur;
                Program.bot.DRWtargethp.Maximum = Value.Target.HP.Max;
                Program.bot.DRWtargethp.Value = Value.Target.HP.Cur;
                Program.bot.DRWtargetmp.Maximum = Value.Target.MP.Max;
                Program.bot.DRWtargetmp.Value = Value.Target.MP.Cur;

                Program.bot.DRWhp.Txt = Value.Player.HP.Cur.ToString("N0") + " / " + Value.Player.HP.Max.ToString("N0");
                Program.bot.DRWmp.Txt = Value.Player.MP.Cur.ToString("N0") + " / " + Value.Player.MP.Max.ToString("N0");
                Program.bot.DRWdp.Txt = Value.Player.DP.Cur.ToString("N0") + " / " + Value.Player.DP.Max.ToString("N0");
                Program.bot.DRWexp.Txt = Value.Player.EXP.Cur.ToString("N0") + " /" + Value.Player.EXP.Max.ToString("N0");
                Program.bot.DRWfly.Txt = Value.Player.Flight.Cur.ToString("N0") + " / " + Value.Player.Flight.Max.ToString("N0");


                Program.bot.DRWtargethp.Txt = Value.Target.HP.Cur.ToString("N0") + " / " + Value.Target.HP.Max.ToString("N0");
                Program.bot.DRWtargetmp.Txt = Value.Target.MP.Cur.ToString("N0") + " / " + Value.Target.MP.Max.ToString("N0");

                if (Value.Player.DP.Cur >= 2000 && Value.Player.DP.Cur < 3000)
                {
                    if (pdp != 1)
                        Program.bot.PNLdplevel.BackgroundImage = Properties.Resources.DP1;
                    pdp = 1;
                }
                else if (Value.Player.DP.Cur >= 3000 && Value.Player.DP.Cur < 4000)
                {

                    if (pdp != 2)
                        Program.bot.PNLdplevel.BackgroundImage = Properties.Resources.DP2;
                    pdp = 2;
                }
                else if (Value.Player.DP.Cur == 4000)
                {

                    if (pdp != 3)
                        Program.bot.PNLdplevel.BackgroundImage = Properties.Resources.DP3;
                    pdp = 3;
                }
                else if (Value.Player.DP.Cur < 2000)
                {
                    pdp = 0;
                    Program.bot.PNLdplevel.BackgroundImage = null;
                }
            });
        }

        private static void GetClass(int cl)
        {
            if (pclass != cl)
                switch (cl)
                {
                    case 0:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Warrior;
                        pclass = 0;
                        break;
                    case 1:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Gladiator;
                        pclass = 1;
                        break;
                    case 2:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Templar;
                        pclass = 2;
                        break;
                    case 3:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Scout;
                        pclass = 3;
                        break;
                    case 4:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Assassin;
                        pclass = 4;
                        break;
                    case 5:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Ranger;
                        pclass = 5;
                        break;
                    case 6:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Mage;
                        pclass = 6;
                        break;
                    case 7:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Sorcerer;
                        pclass = 7;
                        break;
                    case 8:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Spiritmaster;
                        pclass = 8;
                        break;
                    case 9:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Priest;
                        pclass = 9;
                        break;
                    case 10:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Cleric;
                        pclass = 10;
                        break;
                    case 11:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Chanter;
                        pclass = 11;
                        break;
                    case 12:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Engineer;
                        pclass = 12;
                        break;
                    case 13:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Etertech;
                        pclass = 13;
                        break;
                    case 14:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Gunner;
                        pclass = 14;
                        break;
                    case 15:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Artist;
                        pclass = 15;
                        break;
                    case 16:
                        Program.bot.PNLclass.BackgroundImage = Properties.Resources.Bard;
                        pclass = 16;
                        break;
                }
        }
    }
}
