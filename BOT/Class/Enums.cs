﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOT
{
    public class Enums
    {
        public enum Colors : byte
        {
            System = 1,
            Hunting = 2,
            Gather = 3,
            Use = 4,
        }
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        public enum BotStatus : byte
        {
            Idle,
            DeathWaypoint,
            ActionWaypoint,
        }

        public enum Class : int
        {
            Warrior = 0,
            Gladiator = 1,
            Templar = 2,
            Scout = 3,
            Assassin = 4,
            Ranger = 5,
            Mage = 6,
            Sorcerer = 7,
            Spiritmaster = 8,
            Priest = 9,
            Cleric = 10,
            Chanter = 11,
            Engineer = 12,
            Ethertech = 13,
            Gunner = 14,
            Artist = 15,
            Bard = 16,
        }
        public enum AionAttitude : uint
        {
            Passive = 0,
            Hostile = 8,
            Friendly = 38,
            Utility = 294,
        }
        public enum Moving : int
        {
            Yes = 4,
            No = 0,
        }
        public enum TypeTarget : int
        {
            Player = 1,
            NPC = 2,
            Mob = 2,
            Gather = 7,
        }
        public enum Type2Target : int
        {
            Player = 0,
            Mob = 13,
            NPC = 15,
            Gather = 44,
        }
        public enum Flying : int
        {
            No = 0,
            Fly = 1,
            NoFlyGlide = 2,
            FlyGlide = 3,

        }
    }
}
