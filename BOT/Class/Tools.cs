﻿using System;
using System.Diagnostics;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace BOT
{
    class Tools
    {
        #region PCid
        public static string GetCPUId()
        {
            #region CPUid
            string cpuInfo = String.Empty;
            string temp = String.Empty;
            ManagementClass mc = new ManagementClass("Win32_Processor");
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                if (cpuInfo == String.Empty)
                {
                    cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
                }
            }
            return cpuInfo;
            #endregion
        }
        public static string GetMotherBoardID()
        {
            #region Mboard
            ManagementObjectCollection mbCol = new ManagementClass("Win32_BaseBoard").GetInstances();
            ManagementObjectCollection.ManagementObjectEnumerator mbEnum = mbCol.GetEnumerator();
            mbEnum.MoveNext();
            return ((ManagementObject)(mbEnum.Current)).Properties["SerialNumber"].Value.ToString();
            #endregion
        }
        public static string GetMacAddress()
        {
            #region MacAddress
            string macs = "";
            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface ni in interfaces)
            {
                PhysicalAddress pa = ni.GetPhysicalAddress();
                macs += pa.ToString();
            }
            return macs;
            #endregion
        }
        public static string GetGenericID()
        {
            #region UID
            string ID = GetCPUId() + GetMotherBoardID() + GetMacAddress();
            HMACSHA1 hmac = new HMACSHA1();
            hmac.Key = Encoding.ASCII.GetBytes(GetMotherBoardID());
            hmac.ComputeHash(Encoding.ASCII.GetBytes(ID));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hmac.Hash.Length; i++)
            {
                sb.Append(hmac.Hash[i].ToString("X2"));
            }
            return sb.ToString();
            #endregion
        }

        #endregion PCid



        public static void Sleep(uint length)
        {
            DateTime start = DateTime.Now;
            TimeSpan restTime = new TimeSpan(10000);
            while (true)
            {
                System.Windows.Forms.Application.DoEvents();
                TimeSpan remainingTime = start.Add(TimeSpan.FromMilliseconds(length)).Subtract(DateTime.Now);
                if (remainingTime > restTime)
                    System.Threading.Thread.Sleep(restTime);
                else
                {
                    if (remainingTime.Ticks > 0)
                        System.Threading.Thread.Sleep(remainingTime);
                    break;
                }


            }
        }

        #region Timer
        static Stopwatch sw;
        static DateTime startTime = DateTime.Now;

        public static void StartTimer()
        {
            sw = new Stopwatch();
            sw.Start();
            //startTime = DateTime.Now;
        }

        public static double StopTimer()
        {
            sw.Stop();
            //Console.WriteLine((DateTime.Now.Subtract(startTime).TotalSeconds).ToString());
            //return (DateTime.Now.Subtract(startTime).TotalSeconds);

            Console.WriteLine(sw.ElapsedMilliseconds);
            return sw.ElapsedMilliseconds;
        }
        #endregion Timer

        public static void CPUsage()
        {
            BackgroundWorker myWorker = new BackgroundWorker();
            myWorker.DoWork += new DoWorkEventHandler(CPUsage_DoWork);
            myWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CPUsage_RunWorkerCompleted);
            myWorker.WorkerSupportsCancellation = true;

            if (!myWorker.IsBusy)
            {
                myWorker.RunWorkerAsync();
            }

        }

        protected static void CPUsage_DoWork(object sender, DoWorkEventArgs e)
        {
            float cpu = 100;
            //float ram = 100;
            Process process = Process.GetCurrentProcess();
            //PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            PerformanceCounter cpuCounter = new PerformanceCounter("Process", "% Processor Time", process.ProcessName);
            //ramCounter.NextValue();
            cpuCounter.NextValue();
            Thread.Sleep(1000);
            cpu = (float)Math.Round(cpuCounter.NextValue() / Environment.ProcessorCount, 1);
            //ram = (float)Math.Round(ramCounter.NextValue(), 1);
            e.Result = cpu;
        }
        protected static void CPUsage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
          //  Program.bot.StatCpu.Text = "CPU: " + e.Result + "%";
        }


        public static void CaptureScreenshot()
        {
            Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
            Graphics gfxScreenshot = Graphics.FromImage(bmp);

            // Take the screenshot from the upper left corner to the right bottom corner.
            gfxScreenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y,
                                        0, 0,
                                        Screen.PrimaryScreen.Bounds.Size,
                                        CopyPixelOperation.SourceCopy);

            // Save the screenshot to the specified path that the user has chosen.
            bmp.Save("Screenshot.bmp", ImageFormat.Bmp);
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

        public static void CaptureApplication()
        {
            var proc = Launcher.ProcessList[0];
            var rect = new Rect();
            GetWindowRect(proc.MainWindowHandle, ref rect);

            Console.WriteLine(proc);
            Console.WriteLine(proc);
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.left, rect.top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

            bmp.Save("Screenshot.bmp", ImageFormat.Bmp);
            //bmp.Save("c:\\tmp\\test.bmp", ImageFormat.Bmp);
        }




    }
}
