﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BOT
{
    class Log : Enums
    {
        public static void Write(string text, Colors color)
        {
            var frm1 = Form.ActiveForm as MetroMyBot;
            if (frm1 != null)
            {
                Color col = new Color();
                switch (color)
                {
                    case Colors.System:
                        col = frm1.LogSystem.BackColor;
                        break;
                    case Colors.Hunting:
                        col = frm1.LogHunting.BackColor;
                        break;
                    case Colors.Gather:
                        col = frm1.LogGather.BackColor;
                        break;
                    case Colors.Use:
                        col = frm1.LogUse.BackColor;
                        break;
                }

                frm1.UIThread(delegate
                {
                    string data = DateTime.Now.ToString("HH:mm:ss tt");
                    frm1.RTBlog.SelectionStart = frm1.RTBlog.TextLength;
                    frm1.RTBlog.SelectionLength = 0;
                    frm1.RTBlog.SelectionColor = col;
                    frm1.RTBlog.AppendText(data + "\t" + text + Environment.NewLine);
                    frm1.RTBlog.SelectionColor = frm1.RTBlog.ForeColor;
                });


            }
        }

        public static void Write(string text, int value, byte color)
        {
            var frm1 = Form.ActiveForm as MetroMyBot;
            if (frm1 != null)
            {
                Color col = new Color();
                switch (color)
                {
                    case 1:
                        col = frm1.LogSystem.BackColor;
                        break;
                    case 2:
                        col = frm1.LogHunting.BackColor;
                        break;
                    case 3:
                        col = frm1.LogGather.BackColor;
                        break;
                    case 4:
                        col = frm1.LogUse.BackColor;
                        break;


                }

                frm1.UIThread(delegate
                {
                    string data = DateTime.Now.ToString("HH:mm:ss tt");
                    frm1.RTBlog.SelectionStart = frm1.RTBlog.TextLength;
                    frm1.RTBlog.SelectionLength = 0;
                    frm1.RTBlog.SelectionColor = col;
                    frm1.RTBlog.AppendText(data + "\t" + text + value + Environment.NewLine);
                    frm1.RTBlog.SelectionColor = frm1.RTBlog.ForeColor;
                });

            }
        }
    }



}
