﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using MB.GetKey;
namespace BOT
{
    public class Setting
    {
        string path;
        public static string defaultpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + "default" + ".ini";
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        private void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }
        private void IniWriteValue(string Section, string Key, int Value)
        {
            WritePrivateProfileString(Section, Key, Value.ToString(), this.path);
        }
        private void IniWriteValue(string Section, string Key, decimal Value)
        {
            WritePrivateProfileString(Section, Key, Value.ToString(), this.path);
        }

        private string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();
        }

        private int IniReadValueKey(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString().ConvertToInt();
        }


        public void SaveSettingIni(string appPath)
        {
            path = appPath;
            IniWriteValue("Waypoint", Program.bot.TBactionway.Name, Program.bot.TBactionway.Text);
            IniWriteValue("Waypoint", Program.bot.TBdeathway.Name, Program.bot.TBdeathway.Text);
            IniWriteValue("Key", Program.bot.CBforward.Name, Program.bot.CBforward.Text);
            IniWriteValue("Key", Program.bot.CBselect.Name, Program.bot.CBselect.Text);
            IniWriteValue("Key", Program.bot.CBloot.Name, Program.bot.CBloot.Text);
            IniWriteValue("Key", Program.bot.CBrest.Name, Program.bot.CBrest.Text);
            IniWriteValue("Key", Program.bot.CBselectyourself.Name, Program.bot.CBselectyourself.Text);


            //AttackSkill
            DataGridView dgv = Program.bot.DGVskill;
            IniWriteValue("NR", dgv.Name, dgv.RowCount);
            for (int rows = 0; rows < dgv.Rows.Count; rows++)
            {
                IniWriteValue("AttackSkill" + rows, dgv.Columns[0].Name, dgv.Rows[rows].Cells[0].Value.ToString());
                IniWriteValue("AttackSkill" + rows, dgv.Columns[1].Name, dgv.Rows[rows].Cells[1].Value.ToString());
                IniWriteValue("AttackSkill" + rows, dgv.Columns[2].Name, dgv.Rows[rows].Cells[2].Value.ToString());
                IniWriteValue("AttackSkill" + rows, dgv.Columns[3].Name, dgv.Rows[rows].Cells[3].Value.ToString());
                IniWriteValue("AttackSkill" + rows, dgv.Columns[4].Name, dgv.Rows[rows].Cells[4].Value.ToString());
                IniWriteValue("AttackSkill" + rows, dgv.Columns[5].Name, dgv.Rows[rows].Cells[5].Value.ToString());
                IniWriteValue("AttackSkill" + rows, dgv.Columns[6].Name, dgv.Rows[rows].Cells[6].Value.ToString());
            }

            //BuffSkill
            dgv = Program.bot.DGVbuff;
            IniWriteValue("NR", dgv.Name, dgv.RowCount);
            for (int rows = 0; rows < dgv.Rows.Count; rows++)
            {
                IniWriteValue("BuffSkill" + rows, dgv.Columns[0].Name, dgv.Rows[rows].Cells[0].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[1].Name, dgv.Rows[rows].Cells[1].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[2].Name, dgv.Rows[rows].Cells[2].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[3].Name, dgv.Rows[rows].Cells[3].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[4].Name, dgv.Rows[rows].Cells[4].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[5].Name, dgv.Rows[rows].Cells[5].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[6].Name, dgv.Rows[rows].Cells[6].Value.ToString());
                IniWriteValue("BuffSkill" + rows, dgv.Columns[7].Name, dgv.Rows[rows].Cells[7].Value.ToString());
            }

            //Potion
            dgv = Program.bot.DGVpotion;
            IniWriteValue("NR", dgv.Name, dgv.RowCount);
            for (int rows = 0; rows < dgv.Rows.Count; rows++)
            {
                IniWriteValue("Potion" + rows, dgv.Columns[0].Name, dgv.Rows[rows].Cells[0].Value.ToString());
                IniWriteValue("Potion" + rows, dgv.Columns[1].Name, dgv.Rows[rows].Cells[1].Value.ToString());
                IniWriteValue("Potion" + rows, dgv.Columns[2].Name, dgv.Rows[rows].Cells[2].Value.ToString());
                IniWriteValue("Potion" + rows, dgv.Columns[3].Name, dgv.Rows[rows].Cells[3].Value.ToString());
                IniWriteValue("Potion" + rows, dgv.Columns[4].Name, dgv.Rows[rows].Cells[4].Value.ToString());
                IniWriteValue("Potion" + rows, dgv.Columns[5].Name, dgv.Rows[rows].Cells[5].Value.ToString());
                IniWriteValue("Potion" + rows, dgv.Columns[6].Name, dgv.Rows[rows].Cells[6].Value.ToString());
            }

        }

        public void LoadSettingIni(string appPath)
        {
            path = appPath;
            Program.bot.TBactionway.Text = IniReadValue("Waypoint", Program.bot.TBactionway.Name);
            Program.bot.TBdeathway.Text = IniReadValue("Waypoint", Program.bot.TBdeathway.Name);

            //Attack Skill
            DataGridView dgv = Program.bot.DGVskill;
            int val = Convert.ToInt32(IniReadValue("NR", dgv.Name));
            dgv.AllowUserToAddRows = true;
            for (int rows = 0; rows < val; rows++)
            {
                DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
                row.Cells[0].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[0].Name);
                row.Cells[1].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[1].Name);
                row.Cells[2].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[2].Name);
                row.Cells[3].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[3].Name);
                row.Cells[4].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[4].Name);
                row.Cells[5].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[5].Name);
                row.Cells[6].Value = IniReadValue("AttackSkill" + rows, dgv.Columns[6].Name);
                dgv.Rows.Add(row);
            }
            dgv.AllowUserToAddRows = false;

            //BuffSkill
            dgv = Program.bot.DGVbuff;
            val = Convert.ToInt32(IniReadValue("NR", dgv.Name));
            dgv.AllowUserToAddRows = true;
            for (int rows = 0; rows < val; rows++)
            {
                DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
                row.Cells[0].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[0].Name);
                row.Cells[1].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[1].Name);
                row.Cells[2].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[2].Name);
                row.Cells[3].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[3].Name);
                row.Cells[4].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[4].Name);
                row.Cells[5].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[5].Name);
                row.Cells[6].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[6].Name);
                row.Cells[7].Value = IniReadValue("BuffSkill" + rows, dgv.Columns[7].Name);
                dgv.Rows.Add(row);
            }
            dgv.AllowUserToAddRows = false;

            //Potion
            dgv = Program.bot.DGVpotion;
            val = Convert.ToInt32(IniReadValue("NR", dgv.Name));
            dgv.AllowUserToAddRows = true;
            for (int rows = 0; rows < val; rows++)
            {
                DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
                row.Cells[0].Value = IniReadValue("Potion" + rows, dgv.Columns[0].Name);
                row.Cells[1].Value = IniReadValue("Potion" + rows, dgv.Columns[1].Name);
                row.Cells[2].Value = IniReadValue("Potion" + rows, dgv.Columns[2].Name);
                row.Cells[3].Value = IniReadValue("Potion" + rows, dgv.Columns[3].Name);
                row.Cells[4].Value = IniReadValue("Potion" + rows, dgv.Columns[4].Name);
                row.Cells[5].Value = IniReadValue("Potion" + rows, dgv.Columns[5].Name);
                row.Cells[6].Value = IniReadValue("Potion" + rows, dgv.Columns[6].Name);
                dgv.Rows.Add(row);
            }
            dgv.AllowUserToAddRows = false;
        }

        #region Default Setting
        public static void LoadDefaultSetting()
        {
            Program.bot.TBactionway.Text = Properties.Settings.Default.TBcombatway;
            Program.bot.TBdeathway.Text = Properties.Settings.Default.TBdeathway;

            Program.bot.CBforward.SetKeys(Properties.Settings.Default.CBForward.ConvertToKey());
            Program.bot.CBloot.SetKeys(Properties.Settings.Default.CBloot.ConvertToKey());
            Program.bot.CBrest.SetKeys(Properties.Settings.Default.CBrest.ConvertToKey());
            Program.bot.CBselect.SetKeys(Properties.Settings.Default.CBselect.ConvertToKey());
            Program.bot.CBselectyourself.SetKeys(Properties.Settings.Default.CBselectyourself.ConvertToKey());

            Program.bot.CBfly.SetKeys(Properties.Settings.Default.CBfly.ConvertToKey());
            Program.bot.CBland.SetKeys(Properties.Settings.Default.CBland.ConvertToKey());
        }


        public static void SaveDefaultSetting()
        {
            Properties.Settings.Default.TBcombatway = Program.bot.TBactionway.Text;
            Properties.Settings.Default.TBdeathway = Program.bot.TBdeathway.Text;

            Properties.Settings.Default.CBForward = Program.bot.CBforward.GetKeys(0).ToString();
            Properties.Settings.Default.CBloot = Program.bot.CBloot.GetKeys(0).ToString();
            Properties.Settings.Default.CBrest = Program.bot.CBrest.GetKeys(0).ToString();
            Properties.Settings.Default.CBselect = Program.bot.CBselect.GetKeys(0).ToString();
            Properties.Settings.Default.CBselectyourself = Program.bot.CBselectyourself.GetKeys(0).ToString();

            Properties.Settings.Default.CBfly = Program.bot.CBfly.GetKeys(0).ToString();
            Properties.Settings.Default.CBland = Program.bot.CBland.GetKeys(0).ToString();
            Properties.Settings.Default.Save();
        }
        #endregion Default Setting 

    }
}