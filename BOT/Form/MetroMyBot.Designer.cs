﻿namespace BOT
{
    partial class MetroMyBot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroMyBot));
            this.BTNStartBot = new MetroFramework.Controls.MetroButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bOTToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.targetInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alwaysOnTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TBCMainWindow = new MetroFramework.Controls.MetroTabControl();
            this.TBPInformation = new MetroFramework.Controls.MetroTabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.BTNHide = new MetroFramework.Controls.MetroButton();
            this.BTNfullscreen = new MetroFramework.Controls.MetroButton();
            this.TBwindowx = new MetroFramework.Controls.MetroTextBox();
            this.TBwindowy = new MetroFramework.Controls.MetroTextBox();
            this.label23 = new MetroFramework.Controls.MetroLabel();
            this.BTNresize = new MetroFramework.Controls.MetroButton();
            this.label24 = new MetroFramework.Controls.MetroLabel();
            this.label21 = new MetroFramework.Controls.MetroLabel();
            this.TBwindowheight = new MetroFramework.Controls.MetroTextBox();
            this.label22 = new MetroFramework.Controls.MetroLabel();
            this.TBwindowwidth = new MetroFramework.Controls.MetroTextBox();
            this.GBtarget = new System.Windows.Forms.GroupBox();
            this.DRWtargetmp = new MB.exProgressBar.exProgressBar();
            this.DRWtargethp = new MB.exProgressBar.exProgressBar();
            this.label49 = new MetroFramework.Controls.MetroLabel();
            this.label50 = new MetroFramework.Controls.MetroLabel();
            this.GBplayer = new System.Windows.Forms.GroupBox();
            this.DRWfly = new MB.exProgressBar.exProgressBar();
            this.DRWexp = new MB.exProgressBar.exProgressBar();
            this.DRWdp = new MB.exProgressBar.exProgressBar();
            this.DRWmp = new MB.exProgressBar.exProgressBar();
            this.DRWhp = new MB.exProgressBar.exProgressBar();
            this.label54 = new MetroFramework.Controls.MetroLabel();
            this.label53 = new MetroFramework.Controls.MetroLabel();
            this.label31 = new MetroFramework.Controls.MetroLabel();
            this.PNLdplevel = new System.Windows.Forms.Panel();
            this.label32 = new MetroFramework.Controls.MetroLabel();
            this.label33 = new MetroFramework.Controls.MetroLabel();
            this.PNLclass = new System.Windows.Forms.Panel();
            this.tabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.TCplayer = new MetroFramework.Controls.MetroTabPage();
            this.LBflytype = new MetroFramework.Controls.MetroLabel();
            this.LBloggin = new MetroFramework.Controls.MetroLabel();
            this.TCtarget = new MetroFramework.Controls.MetroTabPage();
            this.TBPWaypoint = new MetroFramework.Controls.MetroTabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TBdeathway = new MetroFramework.Controls.MetroTextBox();
            this.BTNdeathbrowse = new MetroFramework.Controls.MetroButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TBactionway = new MetroFramework.Controls.MetroTextBox();
            this.BTNwaybrowse = new MetroFramework.Controls.MetroButton();
            this.TBPPotion = new MetroFramework.Controls.MetroTabPage();
            this.DGVpotion = new MetroFramework.Controls.MetroGrid();
            this.PotionSkillbar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionCooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionCastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionLastUsed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionEffect = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DGVskillcontext = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TBPSkill = new MetroFramework.Controls.MetroTabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DGVskill = new MetroFramework.Controls.MetroGrid();
            this.AttackSkillbar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackCooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackCastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackLastUsed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackChain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackChainWait = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DGVbuff = new MetroFramework.Controls.MetroGrid();
            this.BuffSkillbar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffCooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffCastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffLastUsed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffChainLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffChainWait = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffOnCombat = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TBPScript = new MetroFramework.Controls.MetroTabPage();
            this.ListScript = new System.Windows.Forms.ListBox();
            this.BTNscriptload = new MetroFramework.Controls.MetroButton();
            this.BTNscriptsave = new MetroFramework.Controls.MetroButton();
            this.NRscriptstep = new System.Windows.Forms.NumericUpDown();
            this.label38 = new MetroFramework.Controls.MetroLabel();
            this.BTNrecord = new MetroFramework.Controls.MetroButton();
            this.BTNscriptclear = new MetroFramework.Controls.MetroButton();
            this.TBPAethertapping = new MetroFramework.Controls.MetroTabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.CBaetherReturn = new MetroFramework.Controls.MetroCheckBox();
            this.BTNAddAetherSafeSpot = new MetroFramework.Controls.MetroButton();
            this.ListAetherSafeSpot = new System.Windows.Forms.ListBox();
            this.CMSAetherSaveSpot = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TBPCombat = new MetroFramework.Controls.MetroTabPage();
            this.CBHuntingGetLoot = new MetroFramework.Controls.MetroCheckBox();
            this.TBPKey = new MetroFramework.Controls.MetroTabPage();
            this.CBland = new MB.KeyButton.KeyButton();
            this.CBfly = new MB.KeyButton.KeyButton();
            this.label1 = new MetroFramework.Controls.MetroLabel();
            this.label2 = new MetroFramework.Controls.MetroLabel();
            this.CBselectyourself = new MB.KeyButton.KeyButton();
            this.CBrest = new MB.KeyButton.KeyButton();
            this.CBloot = new MB.KeyButton.KeyButton();
            this.CBselect = new MB.KeyButton.KeyButton();
            this.CBforward = new MB.KeyButton.KeyButton();
            this.label43 = new MetroFramework.Controls.MetroLabel();
            this.label6 = new MetroFramework.Controls.MetroLabel();
            this.label4 = new MetroFramework.Controls.MetroLabel();
            this.label40 = new MetroFramework.Controls.MetroLabel();
            this.label39 = new MetroFramework.Controls.MetroLabel();
            this.TBPLog = new MetroFramework.Controls.MetroTabPage();
            this.BTNlogclear = new MetroFramework.Controls.MetroButton();
            this.RTBlog = new System.Windows.Forms.RichTextBox();
            this.label37 = new MetroFramework.Controls.MetroLabel();
            this.label36 = new MetroFramework.Controls.MetroLabel();
            this.label35 = new MetroFramework.Controls.MetroLabel();
            this.label34 = new MetroFramework.Controls.MetroLabel();
            this.LogUse = new System.Windows.Forms.PictureBox();
            this.LogGather = new System.Windows.Forms.PictureBox();
            this.LogHunting = new System.Windows.Forms.PictureBox();
            this.LogSystem = new System.Windows.Forms.PictureBox();
            this.ToolTipStat = new MetroFramework.Components.MetroToolTip();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TimerScript = new System.Windows.Forms.Timer(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.CMSScriptCreate = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.goToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.landToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.TBCMainWindow.SuspendLayout();
            this.TBPInformation.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.GBtarget.SuspendLayout();
            this.GBplayer.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TCplayer.SuspendLayout();
            this.TBPWaypoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TBPPotion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpotion)).BeginInit();
            this.DGVskillcontext.SuspendLayout();
            this.TBPSkill.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVskill)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVbuff)).BeginInit();
            this.TBPScript.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NRscriptstep)).BeginInit();
            this.TBPAethertapping.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.CMSAetherSaveSpot.SuspendLayout();
            this.TBPCombat.SuspendLayout();
            this.TBPKey.SuspendLayout();
            this.TBPLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogHunting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogSystem)).BeginInit();
            this.CMSScriptCreate.SuspendLayout();
            this.SuspendLayout();
            // 
            // BTNStartBot
            // 
            this.BTNStartBot.BackColor = System.Drawing.SystemColors.Control;
            this.BTNStartBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BTNStartBot.Location = new System.Drawing.Point(20, 597);
            this.BTNStartBot.Name = "BTNStartBot";
            this.BTNStartBot.Size = new System.Drawing.Size(787, 23);
            this.BTNStartBot.TabIndex = 10;
            this.BTNStartBot.Text = "Start Bot";
            this.BTNStartBot.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNStartBot.UseSelectable = true;
            this.BTNStartBot.Click += new System.EventHandler(this.BTNStartBot_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bOTToolStripMenuItem1,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(787, 24);
            this.menuStrip1.TabIndex = 47;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bOTToolStripMenuItem1
            // 
            this.bOTToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.saveSettingToolStripMenuItem,
            this.loadSettingToolStripMenuItem});
            this.bOTToolStripMenuItem1.Name = "bOTToolStripMenuItem1";
            this.bOTToolStripMenuItem1.Size = new System.Drawing.Size(41, 20);
            this.bOTToolStripMenuItem1.Text = "BOT";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.MIexit_Click);
            // 
            // saveSettingToolStripMenuItem
            // 
            this.saveSettingToolStripMenuItem.Name = "saveSettingToolStripMenuItem";
            this.saveSettingToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.saveSettingToolStripMenuItem.Text = "Save Setting";
            this.saveSettingToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadSettingToolStripMenuItem
            // 
            this.loadSettingToolStripMenuItem.Name = "loadSettingToolStripMenuItem";
            this.loadSettingToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadSettingToolStripMenuItem.Text = "Load Setting";
            this.loadSettingToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profiToolStripMenuItem,
            this.targetInfoToolStripMenuItem,
            this.alwaysOnTopToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // profiToolStripMenuItem
            // 
            this.profiToolStripMenuItem.Name = "profiToolStripMenuItem";
            this.profiToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.profiToolStripMenuItem.Text = "Profile";
            this.profiToolStripMenuItem.Click += new System.EventHandler(this.MIprofile_Click);
            // 
            // targetInfoToolStripMenuItem
            // 
            this.targetInfoToolStripMenuItem.Name = "targetInfoToolStripMenuItem";
            this.targetInfoToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.targetInfoToolStripMenuItem.Text = "Target info";
            this.targetInfoToolStripMenuItem.Click += new System.EventHandler(this.MItargetInfo_Click);
            // 
            // alwaysOnTopToolStripMenuItem
            // 
            this.alwaysOnTopToolStripMenuItem.Name = "alwaysOnTopToolStripMenuItem";
            this.alwaysOnTopToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.alwaysOnTopToolStripMenuItem.Text = "Always on top";
            this.alwaysOnTopToolStripMenuItem.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
            // 
            // TBCMainWindow
            // 
            this.TBCMainWindow.Controls.Add(this.TBPInformation);
            this.TBCMainWindow.Controls.Add(this.TBPWaypoint);
            this.TBCMainWindow.Controls.Add(this.TBPPotion);
            this.TBCMainWindow.Controls.Add(this.TBPSkill);
            this.TBCMainWindow.Controls.Add(this.TBPScript);
            this.TBCMainWindow.Controls.Add(this.TBPAethertapping);
            this.TBCMainWindow.Controls.Add(this.TBPCombat);
            this.TBCMainWindow.Controls.Add(this.TBPKey);
            this.TBCMainWindow.Controls.Add(this.TBPLog);
            this.TBCMainWindow.HotTrack = true;
            this.TBCMainWindow.Location = new System.Drawing.Point(23, 87);
            this.TBCMainWindow.Name = "TBCMainWindow";
            this.TBCMainWindow.SelectedIndex = 6;
            this.TBCMainWindow.Size = new System.Drawing.Size(787, 502);
            this.TBCMainWindow.TabIndex = 49;
            this.TBCMainWindow.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBCMainWindow.UseSelectable = true;
            // 
            // TBPInformation
            // 
            this.TBPInformation.BackColor = System.Drawing.SystemColors.Control;
            this.TBPInformation.Controls.Add(this.groupBox7);
            this.TBPInformation.Controls.Add(this.GBtarget);
            this.TBPInformation.Controls.Add(this.GBplayer);
            this.TBPInformation.Controls.Add(this.tabControl1);
            this.TBPInformation.HorizontalScrollbarBarColor = true;
            this.TBPInformation.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPInformation.HorizontalScrollbarSize = 10;
            this.TBPInformation.Location = new System.Drawing.Point(4, 38);
            this.TBPInformation.Name = "TBPInformation";
            this.TBPInformation.Padding = new System.Windows.Forms.Padding(3);
            this.TBPInformation.Size = new System.Drawing.Size(779, 460);
            this.TBPInformation.TabIndex = 0;
            this.TBPInformation.Text = "Information";
            this.TBPInformation.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPInformation.VerticalScrollbarBarColor = true;
            this.TBPInformation.VerticalScrollbarHighlightOnWheel = false;
            this.TBPInformation.VerticalScrollbarSize = 10;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.BTNHide);
            this.groupBox7.Controls.Add(this.BTNfullscreen);
            this.groupBox7.Controls.Add(this.TBwindowx);
            this.groupBox7.Controls.Add(this.TBwindowy);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.BTNresize);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.TBwindowheight);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.TBwindowwidth);
            this.groupBox7.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox7.Location = new System.Drawing.Point(3, 156);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(413, 152);
            this.groupBox7.TabIndex = 72;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Window Manager";
            // 
            // BTNHide
            // 
            this.BTNHide.Location = new System.Drawing.Point(6, 48);
            this.BTNHide.Name = "BTNHide";
            this.BTNHide.Size = new System.Drawing.Size(75, 23);
            this.BTNHide.TabIndex = 73;
            this.BTNHide.Text = "Hide";
            this.BTNHide.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNHide.UseSelectable = true;
            this.BTNHide.Click += new System.EventHandler(this.BTNHide_CheckedChanged);
            // 
            // BTNfullscreen
            // 
            this.BTNfullscreen.Location = new System.Drawing.Point(6, 19);
            this.BTNfullscreen.Name = "BTNfullscreen";
            this.BTNfullscreen.Size = new System.Drawing.Size(75, 23);
            this.BTNfullscreen.TabIndex = 59;
            this.BTNfullscreen.Text = "Full Screen";
            this.BTNfullscreen.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNfullscreen.UseSelectable = true;
            this.BTNfullscreen.Click += new System.EventHandler(this.BTNfullscreen_Click);
            // 
            // TBwindowx
            // 
            // 
            // 
            // 
            this.TBwindowx.CustomButton.Image = null;
            this.TBwindowx.CustomButton.Location = new System.Drawing.Point(57, 2);
            this.TBwindowx.CustomButton.Name = "";
            this.TBwindowx.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.TBwindowx.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TBwindowx.CustomButton.TabIndex = 1;
            this.TBwindowx.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TBwindowx.CustomButton.UseSelectable = true;
            this.TBwindowx.CustomButton.Visible = false;
            this.TBwindowx.Lines = new string[] {
        "0"};
            this.TBwindowx.Location = new System.Drawing.Point(162, 80);
            this.TBwindowx.MaxLength = 32767;
            this.TBwindowx.Name = "TBwindowx";
            this.TBwindowx.PasswordChar = '\0';
            this.TBwindowx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TBwindowx.SelectedText = "";
            this.TBwindowx.SelectionLength = 0;
            this.TBwindowx.SelectionStart = 0;
            this.TBwindowx.Size = new System.Drawing.Size(75, 20);
            this.TBwindowx.TabIndex = 60;
            this.TBwindowx.Text = "0";
            this.TBwindowx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBwindowx.UseSelectable = true;
            this.TBwindowx.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TBwindowx.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // TBwindowy
            // 
            // 
            // 
            // 
            this.TBwindowy.CustomButton.Image = null;
            this.TBwindowy.CustomButton.Location = new System.Drawing.Point(57, 2);
            this.TBwindowy.CustomButton.Name = "";
            this.TBwindowy.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.TBwindowy.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TBwindowy.CustomButton.TabIndex = 1;
            this.TBwindowy.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TBwindowy.CustomButton.UseSelectable = true;
            this.TBwindowy.CustomButton.Visible = false;
            this.TBwindowy.Lines = new string[] {
        "0"};
            this.TBwindowy.Location = new System.Drawing.Point(162, 105);
            this.TBwindowy.MaxLength = 32767;
            this.TBwindowy.Name = "TBwindowy";
            this.TBwindowy.PasswordChar = '\0';
            this.TBwindowy.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TBwindowy.SelectedText = "";
            this.TBwindowy.SelectionLength = 0;
            this.TBwindowy.SelectionStart = 0;
            this.TBwindowy.Size = new System.Drawing.Size(75, 20);
            this.TBwindowy.TabIndex = 61;
            this.TBwindowy.Text = "0";
            this.TBwindowy.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBwindowy.UseSelectable = true;
            this.TBwindowy.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TBwindowy.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(243, 77);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 19);
            this.label23.TabIndex = 68;
            this.label23.Text = "Width:";
            this.label23.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // BTNresize
            // 
            this.BTNresize.Location = new System.Drawing.Point(6, 77);
            this.BTNresize.Name = "BTNresize";
            this.BTNresize.Size = new System.Drawing.Size(75, 23);
            this.BTNresize.TabIndex = 62;
            this.BTNresize.Text = "Resize";
            this.BTNresize.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNresize.UseSelectable = true;
            this.BTNresize.Click += new System.EventHandler(this.BTNresize_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(240, 105);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 19);
            this.label24.TabIndex = 67;
            this.label24.Text = "Height:";
            this.label24.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(87, 80);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 19);
            this.label21.TabIndex = 63;
            this.label21.Text = "Position X:";
            this.label21.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // TBwindowheight
            // 
            // 
            // 
            // 
            this.TBwindowheight.CustomButton.Image = null;
            this.TBwindowheight.CustomButton.Location = new System.Drawing.Point(57, 2);
            this.TBwindowheight.CustomButton.Name = "";
            this.TBwindowheight.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.TBwindowheight.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TBwindowheight.CustomButton.TabIndex = 1;
            this.TBwindowheight.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TBwindowheight.CustomButton.UseSelectable = true;
            this.TBwindowheight.CustomButton.Visible = false;
            this.TBwindowheight.Lines = new string[] {
        "600"};
            this.TBwindowheight.Location = new System.Drawing.Point(296, 105);
            this.TBwindowheight.MaxLength = 32767;
            this.TBwindowheight.Name = "TBwindowheight";
            this.TBwindowheight.PasswordChar = '\0';
            this.TBwindowheight.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TBwindowheight.SelectedText = "";
            this.TBwindowheight.SelectionLength = 0;
            this.TBwindowheight.SelectionStart = 0;
            this.TBwindowheight.Size = new System.Drawing.Size(75, 20);
            this.TBwindowheight.TabIndex = 66;
            this.TBwindowheight.Text = "600";
            this.TBwindowheight.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBwindowheight.UseSelectable = true;
            this.TBwindowheight.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TBwindowheight.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(87, 106);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 19);
            this.label22.TabIndex = 64;
            this.label22.Text = "Position Y:";
            this.label22.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // TBwindowwidth
            // 
            // 
            // 
            // 
            this.TBwindowwidth.CustomButton.Image = null;
            this.TBwindowwidth.CustomButton.Location = new System.Drawing.Point(57, 2);
            this.TBwindowwidth.CustomButton.Name = "";
            this.TBwindowwidth.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.TBwindowwidth.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TBwindowwidth.CustomButton.TabIndex = 1;
            this.TBwindowwidth.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TBwindowwidth.CustomButton.UseSelectable = true;
            this.TBwindowwidth.CustomButton.Visible = false;
            this.TBwindowwidth.Lines = new string[] {
        "600"};
            this.TBwindowwidth.Location = new System.Drawing.Point(296, 77);
            this.TBwindowwidth.MaxLength = 32767;
            this.TBwindowwidth.Name = "TBwindowwidth";
            this.TBwindowwidth.PasswordChar = '\0';
            this.TBwindowwidth.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TBwindowwidth.SelectedText = "";
            this.TBwindowwidth.SelectionLength = 0;
            this.TBwindowwidth.SelectionStart = 0;
            this.TBwindowwidth.Size = new System.Drawing.Size(75, 20);
            this.TBwindowwidth.TabIndex = 65;
            this.TBwindowwidth.Text = "600";
            this.TBwindowwidth.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBwindowwidth.UseSelectable = true;
            this.TBwindowwidth.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TBwindowwidth.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // GBtarget
            // 
            this.GBtarget.BackColor = System.Drawing.Color.Transparent;
            this.GBtarget.Controls.Add(this.DRWtargetmp);
            this.GBtarget.Controls.Add(this.DRWtargethp);
            this.GBtarget.Controls.Add(this.label49);
            this.GBtarget.Controls.Add(this.label50);
            this.GBtarget.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.GBtarget.Location = new System.Drawing.Point(316, 6);
            this.GBtarget.Name = "GBtarget";
            this.GBtarget.Size = new System.Drawing.Size(250, 144);
            this.GBtarget.TabIndex = 52;
            this.GBtarget.TabStop = false;
            this.GBtarget.Text = "Target";
            // 
            // DRWtargetmp
            // 
            this.DRWtargetmp.BackColor = System.Drawing.Color.Gray;
            this.DRWtargetmp.Color1 = System.Drawing.Color.DarkBlue;
            this.DRWtargetmp.Color2 = System.Drawing.Color.Blue;
            this.DRWtargetmp.Location = new System.Drawing.Point(37, 41);
            this.DRWtargetmp.Maximum = 100F;
            this.DRWtargetmp.Minimum = 0F;
            this.DRWtargetmp.Name = "DRWtargetmp";
            this.DRWtargetmp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargetmp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargetmp.RoundedEdge = 0;
            this.DRWtargetmp.Size = new System.Drawing.Size(182, 17);
            this.DRWtargetmp.TabIndex = 75;
            this.DRWtargetmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWtargetmp.TextColor = System.Drawing.Color.White;
            this.DRWtargetmp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWtargetmp.Txt = null;
            this.DRWtargetmp.Value = 50F;
            // 
            // DRWtargethp
            // 
            this.DRWtargethp.BackColor = System.Drawing.Color.Gray;
            this.DRWtargethp.Color1 = System.Drawing.Color.DarkRed;
            this.DRWtargethp.Color2 = System.Drawing.Color.Red;
            this.DRWtargethp.Location = new System.Drawing.Point(37, 19);
            this.DRWtargethp.Maximum = 100F;
            this.DRWtargethp.Minimum = 0F;
            this.DRWtargethp.Name = "DRWtargethp";
            this.DRWtargethp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargethp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargethp.RoundedEdge = 0;
            this.DRWtargethp.Size = new System.Drawing.Size(182, 17);
            this.DRWtargethp.TabIndex = 75;
            this.DRWtargethp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWtargethp.TextColor = System.Drawing.Color.White;
            this.DRWtargethp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWtargethp.Txt = null;
            this.DRWtargethp.Value = 50F;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(7, 22);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(26, 19);
            this.label49.TabIndex = 41;
            this.label49.Text = "HP";
            this.label49.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(6, 41);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(29, 19);
            this.label50.TabIndex = 42;
            this.label50.Text = "MP";
            this.label50.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // GBplayer
            // 
            this.GBplayer.BackColor = System.Drawing.Color.Transparent;
            this.GBplayer.Controls.Add(this.DRWfly);
            this.GBplayer.Controls.Add(this.DRWexp);
            this.GBplayer.Controls.Add(this.DRWdp);
            this.GBplayer.Controls.Add(this.DRWmp);
            this.GBplayer.Controls.Add(this.DRWhp);
            this.GBplayer.Controls.Add(this.label54);
            this.GBplayer.Controls.Add(this.label53);
            this.GBplayer.Controls.Add(this.label31);
            this.GBplayer.Controls.Add(this.PNLdplevel);
            this.GBplayer.Controls.Add(this.label32);
            this.GBplayer.Controls.Add(this.label33);
            this.GBplayer.Controls.Add(this.PNLclass);
            this.GBplayer.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.GBplayer.Location = new System.Drawing.Point(6, 6);
            this.GBplayer.Name = "GBplayer";
            this.GBplayer.Size = new System.Drawing.Size(304, 144);
            this.GBplayer.TabIndex = 51;
            this.GBplayer.TabStop = false;
            this.GBplayer.Text = "Player";
            // 
            // DRWfly
            // 
            this.DRWfly.BackColor = System.Drawing.Color.Gray;
            this.DRWfly.Color1 = System.Drawing.Color.Green;
            this.DRWfly.Color2 = System.Drawing.Color.LightGreen;
            this.DRWfly.Location = new System.Drawing.Point(37, 112);
            this.DRWfly.Maximum = 100F;
            this.DRWfly.Minimum = 0F;
            this.DRWfly.Name = "DRWfly";
            this.DRWfly.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWfly.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWfly.RoundedEdge = 0;
            this.DRWfly.Size = new System.Drawing.Size(182, 17);
            this.DRWfly.TabIndex = 78;
            this.DRWfly.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWfly.TextColor = System.Drawing.Color.White;
            this.DRWfly.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWfly.Txt = null;
            this.DRWfly.Value = 50F;
            // 
            // DRWexp
            // 
            this.DRWexp.BackColor = System.Drawing.Color.Gray;
            this.DRWexp.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.DRWexp.Color2 = System.Drawing.Color.Cyan;
            this.DRWexp.Location = new System.Drawing.Point(37, 89);
            this.DRWexp.Maximum = 100F;
            this.DRWexp.Minimum = 0F;
            this.DRWexp.Name = "DRWexp";
            this.DRWexp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWexp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWexp.RoundedEdge = 0;
            this.DRWexp.Size = new System.Drawing.Size(182, 17);
            this.DRWexp.TabIndex = 77;
            this.DRWexp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWexp.TextColor = System.Drawing.Color.White;
            this.DRWexp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWexp.Txt = null;
            this.DRWexp.Value = 50F;
            // 
            // DRWdp
            // 
            this.DRWdp.BackColor = System.Drawing.Color.Gray;
            this.DRWdp.Color1 = System.Drawing.Color.YellowGreen;
            this.DRWdp.Color2 = System.Drawing.Color.Yellow;
            this.DRWdp.Location = new System.Drawing.Point(37, 65);
            this.DRWdp.Maximum = 100F;
            this.DRWdp.Minimum = 0F;
            this.DRWdp.Name = "DRWdp";
            this.DRWdp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWdp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWdp.RoundedEdge = 0;
            this.DRWdp.Size = new System.Drawing.Size(182, 17);
            this.DRWdp.TabIndex = 76;
            this.DRWdp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWdp.TextColor = System.Drawing.Color.White;
            this.DRWdp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWdp.Txt = null;
            this.DRWdp.Value = 50F;
            // 
            // DRWmp
            // 
            this.DRWmp.BackColor = System.Drawing.Color.Gray;
            this.DRWmp.Color1 = System.Drawing.Color.DarkBlue;
            this.DRWmp.Color2 = System.Drawing.Color.Blue;
            this.DRWmp.Location = new System.Drawing.Point(37, 42);
            this.DRWmp.Maximum = 100F;
            this.DRWmp.Minimum = 0F;
            this.DRWmp.Name = "DRWmp";
            this.DRWmp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWmp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWmp.RoundedEdge = 0;
            this.DRWmp.Size = new System.Drawing.Size(182, 17);
            this.DRWmp.TabIndex = 75;
            this.DRWmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWmp.TextColor = System.Drawing.Color.White;
            this.DRWmp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWmp.Txt = null;
            this.DRWmp.Value = 50F;
            // 
            // DRWhp
            // 
            this.DRWhp.BackColor = System.Drawing.Color.Gray;
            this.DRWhp.Color1 = System.Drawing.Color.DarkRed;
            this.DRWhp.Color2 = System.Drawing.Color.Red;
            this.DRWhp.Location = new System.Drawing.Point(37, 19);
            this.DRWhp.Maximum = 100F;
            this.DRWhp.Minimum = 0F;
            this.DRWhp.Name = "DRWhp";
            this.DRWhp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWhp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWhp.RoundedEdge = 0;
            this.DRWhp.Size = new System.Drawing.Size(182, 17);
            this.DRWhp.TabIndex = 74;
            this.DRWhp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWhp.TextColor = System.Drawing.Color.White;
            this.DRWhp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWhp.Txt = "";
            this.DRWhp.Value = 50F;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(8, 93);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(25, 19);
            this.label54.TabIndex = 52;
            this.label54.Text = "XP";
            this.label54.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(8, 116);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(25, 19);
            this.label53.TabIndex = 51;
            this.label53.Text = "Fly";
            this.label53.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(7, 71);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 19);
            this.label31.TabIndex = 48;
            this.label31.Text = "DP";
            this.label31.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // PNLdplevel
            // 
            this.PNLdplevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PNLdplevel.Location = new System.Drawing.Point(225, 65);
            this.PNLdplevel.Name = "PNLdplevel";
            this.PNLdplevel.Size = new System.Drawing.Size(19, 19);
            this.PNLdplevel.TabIndex = 43;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(7, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 19);
            this.label32.TabIndex = 49;
            this.label32.Text = "HP";
            this.label32.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(6, 46);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 19);
            this.label33.TabIndex = 50;
            this.label33.Text = "MP";
            this.label33.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // PNLclass
            // 
            this.PNLclass.BackColor = System.Drawing.Color.Transparent;
            this.PNLclass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PNLclass.Location = new System.Drawing.Point(225, 14);
            this.PNLclass.Name = "PNLclass";
            this.PNLclass.Size = new System.Drawing.Size(45, 45);
            this.PNLclass.TabIndex = 39;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TCplayer);
            this.tabControl1.Controls.Add(this.TCtarget);
            this.tabControl1.Location = new System.Drawing.Point(455, 148);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(238, 177);
            this.tabControl1.TabIndex = 27;
            this.tabControl1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tabControl1.UseSelectable = true;
            // 
            // TCplayer
            // 
            this.TCplayer.BackColor = System.Drawing.SystemColors.Control;
            this.TCplayer.Controls.Add(this.LBflytype);
            this.TCplayer.Controls.Add(this.LBloggin);
            this.TCplayer.HorizontalScrollbarBarColor = true;
            this.TCplayer.HorizontalScrollbarHighlightOnWheel = false;
            this.TCplayer.HorizontalScrollbarSize = 10;
            this.TCplayer.Location = new System.Drawing.Point(4, 38);
            this.TCplayer.Name = "TCplayer";
            this.TCplayer.Padding = new System.Windows.Forms.Padding(3);
            this.TCplayer.Size = new System.Drawing.Size(230, 135);
            this.TCplayer.TabIndex = 0;
            this.TCplayer.Text = "Player";
            this.TCplayer.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TCplayer.VerticalScrollbarBarColor = true;
            this.TCplayer.VerticalScrollbarHighlightOnWheel = false;
            this.TCplayer.VerticalScrollbarSize = 10;
            // 
            // LBflytype
            // 
            this.LBflytype.AutoSize = true;
            this.LBflytype.Location = new System.Drawing.Point(6, 67);
            this.LBflytype.Name = "LBflytype";
            this.LBflytype.Size = new System.Drawing.Size(59, 19);
            this.LBflytype.TabIndex = 32;
            this.LBflytype.Text = "Fly Type:";
            this.LBflytype.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // LBloggin
            // 
            this.LBloggin.AutoSize = true;
            this.LBloggin.Location = new System.Drawing.Point(6, 42);
            this.LBloggin.Name = "LBloggin";
            this.LBloggin.Size = new System.Drawing.Size(52, 19);
            this.LBloggin.TabIndex = 27;
            this.LBloggin.Text = "Loggin:";
            this.LBloggin.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // TCtarget
            // 
            this.TCtarget.BackColor = System.Drawing.SystemColors.Control;
            this.TCtarget.HorizontalScrollbarBarColor = true;
            this.TCtarget.HorizontalScrollbarHighlightOnWheel = false;
            this.TCtarget.HorizontalScrollbarSize = 10;
            this.TCtarget.Location = new System.Drawing.Point(4, 38);
            this.TCtarget.Name = "TCtarget";
            this.TCtarget.Padding = new System.Windows.Forms.Padding(3);
            this.TCtarget.Size = new System.Drawing.Size(230, 135);
            this.TCtarget.TabIndex = 1;
            this.TCtarget.Text = "Target";
            this.TCtarget.VerticalScrollbarBarColor = true;
            this.TCtarget.VerticalScrollbarHighlightOnWheel = false;
            this.TCtarget.VerticalScrollbarSize = 10;
            // 
            // TBPWaypoint
            // 
            this.TBPWaypoint.BackColor = System.Drawing.SystemColors.Control;
            this.TBPWaypoint.Controls.Add(this.button4);
            this.TBPWaypoint.Controls.Add(this.dataGridView1);
            this.TBPWaypoint.Controls.Add(this.groupBox2);
            this.TBPWaypoint.Controls.Add(this.groupBox1);
            this.TBPWaypoint.HorizontalScrollbarBarColor = true;
            this.TBPWaypoint.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPWaypoint.HorizontalScrollbarSize = 10;
            this.TBPWaypoint.Location = new System.Drawing.Point(4, 38);
            this.TBPWaypoint.Name = "TBPWaypoint";
            this.TBPWaypoint.Size = new System.Drawing.Size(779, 460);
            this.TBPWaypoint.TabIndex = 2;
            this.TBPWaypoint.Text = "Waypoint";
            this.TBPWaypoint.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPWaypoint.VerticalScrollbarBarColor = true;
            this.TBPWaypoint.VerticalScrollbarHighlightOnWheel = false;
            this.TBPWaypoint.VerticalScrollbarSize = 10;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(20, 159);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(113, 119);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(560, 204);
            this.dataGridView1.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.TBdeathway);
            this.groupBox2.Controls.Add(this.BTNdeathbrowse);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(327, 58);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Death Waypoint";
            // 
            // TBdeathway
            // 
            // 
            // 
            // 
            this.TBdeathway.CustomButton.Image = null;
            this.TBdeathway.CustomButton.Location = new System.Drawing.Point(212, 2);
            this.TBdeathway.CustomButton.Name = "";
            this.TBdeathway.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.TBdeathway.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TBdeathway.CustomButton.TabIndex = 1;
            this.TBdeathway.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TBdeathway.CustomButton.UseSelectable = true;
            this.TBdeathway.CustomButton.Visible = false;
            this.TBdeathway.Lines = new string[0];
            this.TBdeathway.Location = new System.Drawing.Point(6, 19);
            this.TBdeathway.MaxLength = 32767;
            this.TBdeathway.Name = "TBdeathway";
            this.TBdeathway.PasswordChar = '\0';
            this.TBdeathway.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TBdeathway.SelectedText = "";
            this.TBdeathway.SelectionLength = 0;
            this.TBdeathway.SelectionStart = 0;
            this.TBdeathway.Size = new System.Drawing.Size(230, 20);
            this.TBdeathway.TabIndex = 0;
            this.TBdeathway.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBdeathway.UseSelectable = true;
            this.TBdeathway.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TBdeathway.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // BTNdeathbrowse
            // 
            this.BTNdeathbrowse.Location = new System.Drawing.Point(242, 19);
            this.BTNdeathbrowse.Name = "BTNdeathbrowse";
            this.BTNdeathbrowse.Size = new System.Drawing.Size(75, 23);
            this.BTNdeathbrowse.TabIndex = 1;
            this.BTNdeathbrowse.Text = "Browse";
            this.BTNdeathbrowse.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNdeathbrowse.UseSelectable = true;
            this.BTNdeathbrowse.Click += new System.EventHandler(this.BTNdeathbrowse_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.TBactionway);
            this.groupBox1.Controls.Add(this.BTNwaybrowse);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox1.Location = new System.Drawing.Point(336, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 58);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Action Waypoint";
            // 
            // TBactionway
            // 
            // 
            // 
            // 
            this.TBactionway.CustomButton.Image = null;
            this.TBactionway.CustomButton.Location = new System.Drawing.Point(212, 2);
            this.TBactionway.CustomButton.Name = "";
            this.TBactionway.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.TBactionway.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TBactionway.CustomButton.TabIndex = 1;
            this.TBactionway.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TBactionway.CustomButton.UseSelectable = true;
            this.TBactionway.CustomButton.Visible = false;
            this.TBactionway.Lines = new string[0];
            this.TBactionway.Location = new System.Drawing.Point(6, 19);
            this.TBactionway.MaxLength = 32767;
            this.TBactionway.Name = "TBactionway";
            this.TBactionway.PasswordChar = '\0';
            this.TBactionway.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TBactionway.SelectedText = "";
            this.TBactionway.SelectionLength = 0;
            this.TBactionway.SelectionStart = 0;
            this.TBactionway.Size = new System.Drawing.Size(230, 20);
            this.TBactionway.TabIndex = 0;
            this.TBactionway.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBactionway.UseSelectable = true;
            this.TBactionway.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TBactionway.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // BTNwaybrowse
            // 
            this.BTNwaybrowse.Location = new System.Drawing.Point(242, 19);
            this.BTNwaybrowse.Name = "BTNwaybrowse";
            this.BTNwaybrowse.Size = new System.Drawing.Size(75, 23);
            this.BTNwaybrowse.TabIndex = 1;
            this.BTNwaybrowse.Text = "Browse";
            this.BTNwaybrowse.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNwaybrowse.UseSelectable = true;
            this.BTNwaybrowse.Click += new System.EventHandler(this.BTNwaybrowse_Click);
            // 
            // TBPPotion
            // 
            this.TBPPotion.BackColor = System.Drawing.SystemColors.Control;
            this.TBPPotion.Controls.Add(this.DGVpotion);
            this.TBPPotion.HorizontalScrollbarBarColor = true;
            this.TBPPotion.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPPotion.HorizontalScrollbarSize = 10;
            this.TBPPotion.Location = new System.Drawing.Point(4, 38);
            this.TBPPotion.Name = "TBPPotion";
            this.TBPPotion.Size = new System.Drawing.Size(779, 460);
            this.TBPPotion.TabIndex = 3;
            this.TBPPotion.Text = "Potion";
            this.TBPPotion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPPotion.VerticalScrollbarBarColor = true;
            this.TBPPotion.VerticalScrollbarHighlightOnWheel = false;
            this.TBPPotion.VerticalScrollbarSize = 10;
            // 
            // DGVpotion
            // 
            this.DGVpotion.AllowUserToAddRows = false;
            this.DGVpotion.AllowUserToResizeColumns = false;
            this.DGVpotion.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DGVpotion.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVpotion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVpotion.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.DGVpotion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVpotion.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGVpotion.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVpotion.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVpotion.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGVpotion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVpotion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PotionSkillbar,
            this.PotionKey,
            this.PotionCooldown,
            this.PotionCastTime,
            this.PotionLastUsed,
            this.PotionEffect,
            this.PotionPercent});
            this.DGVpotion.ContextMenuStrip = this.DGVskillcontext;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVpotion.DefaultCellStyle = dataGridViewCellStyle8;
            this.DGVpotion.EnableHeadersVisualStyles = false;
            this.DGVpotion.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.DGVpotion.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.DGVpotion.Location = new System.Drawing.Point(3, 3);
            this.DGVpotion.MultiSelect = false;
            this.DGVpotion.Name = "DGVpotion";
            this.DGVpotion.ReadOnly = true;
            this.DGVpotion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVpotion.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVpotion.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DGVpotion.RowHeadersVisible = false;
            this.DGVpotion.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGVpotion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVpotion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVpotion.Size = new System.Drawing.Size(679, 146);
            this.DGVpotion.TabIndex = 13;
            this.DGVpotion.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.DGVpotion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGVpotion_KeyDown);
            this.DGVpotion.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DGVpotion_MouseClick);
            // 
            // PotionSkillbar
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PotionSkillbar.DefaultCellStyle = dataGridViewCellStyle3;
            this.PotionSkillbar.HeaderText = "Skillbar";
            this.PotionSkillbar.Name = "PotionSkillbar";
            this.PotionSkillbar.ReadOnly = true;
            this.PotionSkillbar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PotionSkillbar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionKey
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PotionKey.DefaultCellStyle = dataGridViewCellStyle4;
            this.PotionKey.HeaderText = "Key";
            this.PotionKey.Name = "PotionKey";
            this.PotionKey.ReadOnly = true;
            this.PotionKey.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PotionKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionCooldown
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PotionCooldown.DefaultCellStyle = dataGridViewCellStyle5;
            this.PotionCooldown.HeaderText = "Cooldown";
            this.PotionCooldown.MaxInputLength = 3;
            this.PotionCooldown.Name = "PotionCooldown";
            this.PotionCooldown.ReadOnly = true;
            this.PotionCooldown.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionCastTime
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PotionCastTime.DefaultCellStyle = dataGridViewCellStyle6;
            this.PotionCastTime.HeaderText = "CastTime";
            this.PotionCastTime.MaxInputLength = 3;
            this.PotionCastTime.Name = "PotionCastTime";
            this.PotionCastTime.ReadOnly = true;
            this.PotionCastTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionLastUsed
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PotionLastUsed.DefaultCellStyle = dataGridViewCellStyle7;
            this.PotionLastUsed.HeaderText = "Last Used";
            this.PotionLastUsed.MaxInputLength = 50;
            this.PotionLastUsed.Name = "PotionLastUsed";
            this.PotionLastUsed.ReadOnly = true;
            this.PotionLastUsed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PotionLastUsed.Visible = false;
            // 
            // PotionEffect
            // 
            this.PotionEffect.HeaderText = "Effect";
            this.PotionEffect.Name = "PotionEffect";
            this.PotionEffect.ReadOnly = true;
            // 
            // PotionPercent
            // 
            this.PotionPercent.HeaderText = "Percent";
            this.PotionPercent.Name = "PotionPercent";
            this.PotionPercent.ReadOnly = true;
            // 
            // DGVskillcontext
            // 
            this.DGVskillcontext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.upToolStripMenuItem,
            this.downToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.DGVskillcontext.Name = "DGVskillcontext";
            this.DGVskillcontext.Size = new System.Drawing.Size(118, 92);
            this.DGVskillcontext.Opening += new System.ComponentModel.CancelEventHandler(this.DGVskillcontext_Opening);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // upToolStripMenuItem
            // 
            this.upToolStripMenuItem.Name = "upToolStripMenuItem";
            this.upToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.upToolStripMenuItem.Text = "Up";
            this.upToolStripMenuItem.Click += new System.EventHandler(this.upToolStripMenuItem_Click);
            // 
            // downToolStripMenuItem
            // 
            this.downToolStripMenuItem.Name = "downToolStripMenuItem";
            this.downToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.downToolStripMenuItem.Text = "Down";
            this.downToolStripMenuItem.Click += new System.EventHandler(this.downToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // TBPSkill
            // 
            this.TBPSkill.BackColor = System.Drawing.SystemColors.Control;
            this.TBPSkill.Controls.Add(this.button1);
            this.TBPSkill.Controls.Add(this.groupBox6);
            this.TBPSkill.Controls.Add(this.groupBox3);
            this.TBPSkill.HorizontalScrollbarBarColor = true;
            this.TBPSkill.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPSkill.HorizontalScrollbarSize = 10;
            this.TBPSkill.Location = new System.Drawing.Point(4, 38);
            this.TBPSkill.Name = "TBPSkill";
            this.TBPSkill.Size = new System.Drawing.Size(779, 460);
            this.TBPSkill.TabIndex = 4;
            this.TBPSkill.Text = "Skill";
            this.TBPSkill.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPSkill.VerticalScrollbarBarColor = true;
            this.TBPSkill.VerticalScrollbarHighlightOnWheel = false;
            this.TBPSkill.VerticalScrollbarSize = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(56, 381);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 94;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.DGVskill);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(695, 177);
            this.groupBox6.TabIndex = 93;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Attack Skill";
            // 
            // DGVskill
            // 
            this.DGVskill.AllowUserToAddRows = false;
            this.DGVskill.AllowUserToResizeColumns = false;
            this.DGVskill.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DGVskill.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.DGVskill.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVskill.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.DGVskill.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVskill.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGVskill.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVskill.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVskill.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.DGVskill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVskill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AttackSkillbar,
            this.AttackKey,
            this.AttackCooldown,
            this.AttackCastTime,
            this.AttackLastUsed,
            this.AttackChain,
            this.AttackChainWait});
            this.DGVskill.ContextMenuStrip = this.DGVskillcontext;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVskill.DefaultCellStyle = dataGridViewCellStyle19;
            this.DGVskill.EnableHeadersVisualStyles = false;
            this.DGVskill.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.DGVskill.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.DGVskill.Location = new System.Drawing.Point(7, 25);
            this.DGVskill.MultiSelect = false;
            this.DGVskill.Name = "DGVskill";
            this.DGVskill.ReadOnly = true;
            this.DGVskill.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVskill.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVskill.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.DGVskill.RowHeadersVisible = false;
            this.DGVskill.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGVskill.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVskill.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVskill.Size = new System.Drawing.Size(679, 146);
            this.DGVskill.TabIndex = 13;
            this.DGVskill.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // AttackSkillbar
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackSkillbar.DefaultCellStyle = dataGridViewCellStyle12;
            this.AttackSkillbar.HeaderText = "Skillbar";
            this.AttackSkillbar.MaxInputLength = 10;
            this.AttackSkillbar.Name = "AttackSkillbar";
            this.AttackSkillbar.ReadOnly = true;
            this.AttackSkillbar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackKey
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackKey.DefaultCellStyle = dataGridViewCellStyle13;
            this.AttackKey.HeaderText = "Key";
            this.AttackKey.MaxInputLength = 5;
            this.AttackKey.Name = "AttackKey";
            this.AttackKey.ReadOnly = true;
            this.AttackKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackCooldown
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackCooldown.DefaultCellStyle = dataGridViewCellStyle14;
            this.AttackCooldown.HeaderText = "Cooldown";
            this.AttackCooldown.MaxInputLength = 3;
            this.AttackCooldown.Name = "AttackCooldown";
            this.AttackCooldown.ReadOnly = true;
            this.AttackCooldown.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackCastTime
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackCastTime.DefaultCellStyle = dataGridViewCellStyle15;
            this.AttackCastTime.HeaderText = "CastTime";
            this.AttackCastTime.MaxInputLength = 3;
            this.AttackCastTime.Name = "AttackCastTime";
            this.AttackCastTime.ReadOnly = true;
            this.AttackCastTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackLastUsed
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackLastUsed.DefaultCellStyle = dataGridViewCellStyle16;
            this.AttackLastUsed.HeaderText = "Last Used";
            this.AttackLastUsed.MaxInputLength = 50;
            this.AttackLastUsed.Name = "AttackLastUsed";
            this.AttackLastUsed.ReadOnly = true;
            this.AttackLastUsed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AttackLastUsed.Visible = false;
            // 
            // AttackChain
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackChain.DefaultCellStyle = dataGridViewCellStyle17;
            this.AttackChain.HeaderText = "Chain Level";
            this.AttackChain.MaxInputLength = 1;
            this.AttackChain.Name = "AttackChain";
            this.AttackChain.ReadOnly = true;
            this.AttackChain.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AttackChain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AttackChain.Visible = false;
            // 
            // AttackChainWait
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AttackChainWait.DefaultCellStyle = dataGridViewCellStyle18;
            this.AttackChainWait.HeaderText = "Chain Wait";
            this.AttackChainWait.MaxInputLength = 2;
            this.AttackChainWait.Name = "AttackChainWait";
            this.AttackChainWait.ReadOnly = true;
            this.AttackChainWait.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AttackChainWait.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.DGVbuff);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox3.Location = new System.Drawing.Point(3, 186);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(695, 177);
            this.groupBox3.TabIndex = 92;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Buff";
            // 
            // DGVbuff
            // 
            this.DGVbuff.AllowUserToAddRows = false;
            this.DGVbuff.AllowUserToResizeColumns = false;
            this.DGVbuff.AllowUserToResizeRows = false;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DGVbuff.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.DGVbuff.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVbuff.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.DGVbuff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVbuff.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGVbuff.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVbuff.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVbuff.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.DGVbuff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVbuff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuffSkillbar,
            this.BuffKey,
            this.BuffCooldown,
            this.BuffCastTime,
            this.BuffLastUsed,
            this.BuffChainLevel,
            this.BuffChainWait,
            this.BuffOnCombat});
            this.DGVbuff.ContextMenuStrip = this.DGVskillcontext;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVbuff.DefaultCellStyle = dataGridViewCellStyle30;
            this.DGVbuff.EnableHeadersVisualStyles = false;
            this.DGVbuff.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.DGVbuff.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.DGVbuff.Location = new System.Drawing.Point(6, 20);
            this.DGVbuff.MultiSelect = false;
            this.DGVbuff.Name = "DGVbuff";
            this.DGVbuff.ReadOnly = true;
            this.DGVbuff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVbuff.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVbuff.RowHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.DGVbuff.RowHeadersVisible = false;
            this.DGVbuff.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGVbuff.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVbuff.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVbuff.Size = new System.Drawing.Size(680, 151);
            this.DGVbuff.TabIndex = 91;
            this.DGVbuff.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.DGVbuff.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGVbuff_KeyDown);
            // 
            // BuffSkillbar
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffSkillbar.DefaultCellStyle = dataGridViewCellStyle23;
            this.BuffSkillbar.HeaderText = "Skillbar";
            this.BuffSkillbar.MaxInputLength = 10;
            this.BuffSkillbar.Name = "BuffSkillbar";
            this.BuffSkillbar.ReadOnly = true;
            this.BuffSkillbar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffKey
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffKey.DefaultCellStyle = dataGridViewCellStyle24;
            this.BuffKey.HeaderText = "Key";
            this.BuffKey.MaxInputLength = 5;
            this.BuffKey.Name = "BuffKey";
            this.BuffKey.ReadOnly = true;
            this.BuffKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffCooldown
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffCooldown.DefaultCellStyle = dataGridViewCellStyle25;
            this.BuffCooldown.HeaderText = "Cooldown";
            this.BuffCooldown.MaxInputLength = 3;
            this.BuffCooldown.Name = "BuffCooldown";
            this.BuffCooldown.ReadOnly = true;
            this.BuffCooldown.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffCastTime
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffCastTime.DefaultCellStyle = dataGridViewCellStyle26;
            this.BuffCastTime.HeaderText = "CastTime";
            this.BuffCastTime.MaxInputLength = 3;
            this.BuffCastTime.Name = "BuffCastTime";
            this.BuffCastTime.ReadOnly = true;
            this.BuffCastTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffLastUsed
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffLastUsed.DefaultCellStyle = dataGridViewCellStyle27;
            this.BuffLastUsed.HeaderText = "Last Used";
            this.BuffLastUsed.MaxInputLength = 50;
            this.BuffLastUsed.Name = "BuffLastUsed";
            this.BuffLastUsed.ReadOnly = true;
            this.BuffLastUsed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BuffLastUsed.Visible = false;
            // 
            // BuffChainLevel
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffChainLevel.DefaultCellStyle = dataGridViewCellStyle28;
            this.BuffChainLevel.HeaderText = "Chain Level";
            this.BuffChainLevel.MaxInputLength = 1;
            this.BuffChainLevel.Name = "BuffChainLevel";
            this.BuffChainLevel.ReadOnly = true;
            this.BuffChainLevel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BuffChainLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BuffChainLevel.Visible = false;
            // 
            // BuffChainWait
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.BuffChainWait.DefaultCellStyle = dataGridViewCellStyle29;
            this.BuffChainWait.HeaderText = "Chain Wait";
            this.BuffChainWait.MaxInputLength = 2;
            this.BuffChainWait.Name = "BuffChainWait";
            this.BuffChainWait.ReadOnly = true;
            this.BuffChainWait.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BuffChainWait.Visible = false;
            // 
            // BuffOnCombat
            // 
            this.BuffOnCombat.HeaderText = "OnCombat";
            this.BuffOnCombat.Name = "BuffOnCombat";
            this.BuffOnCombat.ReadOnly = true;
            this.BuffOnCombat.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BuffOnCombat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // TBPScript
            // 
            this.TBPScript.Controls.Add(this.ListScript);
            this.TBPScript.Controls.Add(this.BTNscriptload);
            this.TBPScript.Controls.Add(this.BTNscriptsave);
            this.TBPScript.Controls.Add(this.NRscriptstep);
            this.TBPScript.Controls.Add(this.label38);
            this.TBPScript.Controls.Add(this.BTNrecord);
            this.TBPScript.Controls.Add(this.BTNscriptclear);
            this.TBPScript.HorizontalScrollbarBarColor = true;
            this.TBPScript.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPScript.HorizontalScrollbarSize = 10;
            this.TBPScript.Location = new System.Drawing.Point(4, 38);
            this.TBPScript.Name = "TBPScript";
            this.TBPScript.Size = new System.Drawing.Size(779, 460);
            this.TBPScript.TabIndex = 8;
            this.TBPScript.Text = "Script";
            this.TBPScript.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPScript.UseVisualStyleBackColor = true;
            this.TBPScript.VerticalScrollbarBarColor = true;
            this.TBPScript.VerticalScrollbarHighlightOnWheel = false;
            this.TBPScript.VerticalScrollbarSize = 10;
            // 
            // ListScript
            // 
            this.ListScript.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.ListScript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListScript.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.ListScript.FormattingEnabled = true;
            this.ListScript.Location = new System.Drawing.Point(11, 14);
            this.ListScript.Name = "ListScript";
            this.ListScript.Size = new System.Drawing.Size(266, 379);
            this.ListScript.TabIndex = 11;
            // 
            // BTNscriptload
            // 
            this.BTNscriptload.Location = new System.Drawing.Point(283, 103);
            this.BTNscriptload.Name = "BTNscriptload";
            this.BTNscriptload.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptload.TabIndex = 7;
            this.BTNscriptload.Text = "Load";
            this.BTNscriptload.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNscriptload.UseSelectable = true;
            this.BTNscriptload.Click += new System.EventHandler(this.BTNscriptload_Click);
            // 
            // BTNscriptsave
            // 
            this.BTNscriptsave.Location = new System.Drawing.Point(283, 73);
            this.BTNscriptsave.Name = "BTNscriptsave";
            this.BTNscriptsave.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptsave.TabIndex = 6;
            this.BTNscriptsave.Text = "Save";
            this.BTNscriptsave.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNscriptsave.UseSelectable = true;
            this.BTNscriptsave.Click += new System.EventHandler(this.BTNscriptsave_Click);
            // 
            // NRscriptstep
            // 
            this.NRscriptstep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.NRscriptstep.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.NRscriptstep.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NRscriptstep.Location = new System.Drawing.Point(443, 49);
            this.NRscriptstep.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.NRscriptstep.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NRscriptstep.Name = "NRscriptstep";
            this.NRscriptstep.Size = new System.Drawing.Size(39, 20);
            this.NRscriptstep.TabIndex = 5;
            this.NRscriptstep.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(364, 49);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(73, 19);
            this.label38.TabIndex = 4;
            this.label38.Text = "Every step ";
            this.label38.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // BTNrecord
            // 
            this.BTNrecord.Location = new System.Drawing.Point(283, 44);
            this.BTNrecord.Name = "BTNrecord";
            this.BTNrecord.Size = new System.Drawing.Size(75, 23);
            this.BTNrecord.TabIndex = 2;
            this.BTNrecord.Text = "Record";
            this.BTNrecord.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNrecord.UseSelectable = true;
            this.BTNrecord.Click += new System.EventHandler(this.BTNrecord_Click);
            // 
            // BTNscriptclear
            // 
            this.BTNscriptclear.Location = new System.Drawing.Point(283, 14);
            this.BTNscriptclear.Name = "BTNscriptclear";
            this.BTNscriptclear.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptclear.TabIndex = 1;
            this.BTNscriptclear.Text = "Clear";
            this.BTNscriptclear.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNscriptclear.UseSelectable = true;
            this.BTNscriptclear.Click += new System.EventHandler(this.BTNscriptclear_Click);
            // 
            // TBPAethertapping
            // 
            this.TBPAethertapping.Controls.Add(this.groupBox5);
            this.TBPAethertapping.HorizontalScrollbarBarColor = true;
            this.TBPAethertapping.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPAethertapping.HorizontalScrollbarSize = 10;
            this.TBPAethertapping.Location = new System.Drawing.Point(4, 38);
            this.TBPAethertapping.Name = "TBPAethertapping";
            this.TBPAethertapping.Size = new System.Drawing.Size(779, 460);
            this.TBPAethertapping.TabIndex = 10;
            this.TBPAethertapping.Text = "Aethertapping";
            this.TBPAethertapping.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPAethertapping.UseVisualStyleBackColor = true;
            this.TBPAethertapping.VerticalScrollbarBarColor = true;
            this.TBPAethertapping.VerticalScrollbarHighlightOnWheel = false;
            this.TBPAethertapping.VerticalScrollbarSize = 10;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.BTNAddAetherSafeSpot);
            this.groupBox5.Controls.Add(this.ListAetherSafeSpot);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(293, 174);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Aethertapping";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.CBaetherReturn);
            this.groupBox8.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox8.Location = new System.Drawing.Point(147, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(134, 47);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Option";
            // 
            // CBaetherReturn
            // 
            this.CBaetherReturn.AutoSize = true;
            this.CBaetherReturn.Location = new System.Drawing.Point(6, 19);
            this.CBaetherReturn.Name = "CBaetherReturn";
            this.CBaetherReturn.Size = new System.Drawing.Size(123, 15);
            this.CBaetherReturn.TabIndex = 10;
            this.CBaetherReturn.Text = "Return if script end";
            this.CBaetherReturn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CBaetherReturn.UseSelectable = true;
            // 
            // BTNAddAetherSafeSpot
            // 
            this.BTNAddAetherSafeSpot.Location = new System.Drawing.Point(6, 133);
            this.BTNAddAetherSafeSpot.Name = "BTNAddAetherSafeSpot";
            this.BTNAddAetherSafeSpot.Size = new System.Drawing.Size(135, 23);
            this.BTNAddAetherSafeSpot.TabIndex = 8;
            this.BTNAddAetherSafeSpot.Text = "Add Safe Spot";
            this.BTNAddAetherSafeSpot.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNAddAetherSafeSpot.UseSelectable = true;
            this.BTNAddAetherSafeSpot.Click += new System.EventHandler(this.BTNAddAtherSafeSpot_Click);
            // 
            // ListAetherSafeSpot
            // 
            this.ListAetherSafeSpot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.ListAetherSafeSpot.ContextMenuStrip = this.CMSAetherSaveSpot;
            this.ListAetherSafeSpot.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.ListAetherSafeSpot.FormattingEnabled = true;
            this.ListAetherSafeSpot.Location = new System.Drawing.Point(6, 19);
            this.ListAetherSafeSpot.Name = "ListAetherSafeSpot";
            this.ListAetherSafeSpot.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ListAetherSafeSpot.Size = new System.Drawing.Size(135, 108);
            this.ListAetherSafeSpot.TabIndex = 7;
            this.ListAetherSafeSpot.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListAetherSafeSpot_MouseDown);
            // 
            // CMSAetherSaveSpot
            // 
            this.CMSAetherSaveSpot.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.CMSAetherSaveSpot.Name = "CMSAetherSaveSpot";
            this.CMSAetherSaveSpot.Size = new System.Drawing.Size(108, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.TSMdelete_Click);
            // 
            // TBPCombat
            // 
            this.TBPCombat.BackColor = System.Drawing.SystemColors.Control;
            this.TBPCombat.Controls.Add(this.CBHuntingGetLoot);
            this.TBPCombat.HorizontalScrollbarBarColor = true;
            this.TBPCombat.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPCombat.HorizontalScrollbarSize = 10;
            this.TBPCombat.Location = new System.Drawing.Point(4, 38);
            this.TBPCombat.Name = "TBPCombat";
            this.TBPCombat.Size = new System.Drawing.Size(779, 460);
            this.TBPCombat.TabIndex = 5;
            this.TBPCombat.Text = "Combat";
            this.TBPCombat.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPCombat.VerticalScrollbarBarColor = true;
            this.TBPCombat.VerticalScrollbarHighlightOnWheel = false;
            this.TBPCombat.VerticalScrollbarSize = 10;
            // 
            // CBHuntingGetLoot
            // 
            this.CBHuntingGetLoot.AutoSize = true;
            this.CBHuntingGetLoot.Location = new System.Drawing.Point(35, 32);
            this.CBHuntingGetLoot.Name = "CBHuntingGetLoot";
            this.CBHuntingGetLoot.Size = new System.Drawing.Size(68, 15);
            this.CBHuntingGetLoot.TabIndex = 2;
            this.CBHuntingGetLoot.Text = "Get Loot";
            this.CBHuntingGetLoot.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CBHuntingGetLoot.UseSelectable = true;
            this.CBHuntingGetLoot.CheckedChanged += new System.EventHandler(this.CBHuntingGetLoot_CheckedChanged);
            // 
            // TBPKey
            // 
            this.TBPKey.Controls.Add(this.CBland);
            this.TBPKey.Controls.Add(this.CBfly);
            this.TBPKey.Controls.Add(this.label1);
            this.TBPKey.Controls.Add(this.label2);
            this.TBPKey.Controls.Add(this.CBselectyourself);
            this.TBPKey.Controls.Add(this.CBrest);
            this.TBPKey.Controls.Add(this.CBloot);
            this.TBPKey.Controls.Add(this.CBselect);
            this.TBPKey.Controls.Add(this.CBforward);
            this.TBPKey.Controls.Add(this.label43);
            this.TBPKey.Controls.Add(this.label6);
            this.TBPKey.Controls.Add(this.label4);
            this.TBPKey.Controls.Add(this.label40);
            this.TBPKey.Controls.Add(this.label39);
            this.TBPKey.HorizontalScrollbarBarColor = true;
            this.TBPKey.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPKey.HorizontalScrollbarSize = 10;
            this.TBPKey.Location = new System.Drawing.Point(4, 38);
            this.TBPKey.Name = "TBPKey";
            this.TBPKey.Size = new System.Drawing.Size(779, 460);
            this.TBPKey.TabIndex = 9;
            this.TBPKey.Text = "Key";
            this.TBPKey.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPKey.UseVisualStyleBackColor = true;
            this.TBPKey.VerticalScrollbarBarColor = true;
            this.TBPKey.VerticalScrollbarHighlightOnWheel = false;
            this.TBPKey.VerticalScrollbarSize = 10;
            // 
            // CBland
            // 
            this.CBland.Location = new System.Drawing.Point(353, 65);
            this.CBland.MultiKeys = false;
            this.CBland.Name = "CBland";
            this.CBland.SetText = "Wait on key down";
            this.CBland.Size = new System.Drawing.Size(121, 21);
            this.CBland.TabIndex = 22;
            this.CBland.ToolTip = true;
            this.CBland.UseVisualStyleBackColor = true;
            // 
            // CBfly
            // 
            this.CBfly.Location = new System.Drawing.Point(353, 37);
            this.CBfly.MultiKeys = false;
            this.CBfly.Name = "CBfly";
            this.CBfly.SetText = "Wait on key down";
            this.CBfly.Size = new System.Drawing.Size(121, 21);
            this.CBfly.TabIndex = 21;
            this.CBfly.ToolTip = true;
            this.CBfly.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(309, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 19);
            this.label1.TabIndex = 20;
            this.label1.Text = "Fly";
            this.label1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 19);
            this.label2.TabIndex = 19;
            this.label2.Text = "Land";
            this.label2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // CBselectyourself
            // 
            this.CBselectyourself.Location = new System.Drawing.Point(95, 120);
            this.CBselectyourself.MultiKeys = false;
            this.CBselectyourself.Name = "CBselectyourself";
            this.CBselectyourself.SetText = "Wait on key down";
            this.CBselectyourself.Size = new System.Drawing.Size(121, 21);
            this.CBselectyourself.TabIndex = 18;
            this.CBselectyourself.ToolTip = true;
            this.CBselectyourself.UseVisualStyleBackColor = true;
            // 
            // CBrest
            // 
            this.CBrest.Location = new System.Drawing.Point(95, 92);
            this.CBrest.MultiKeys = false;
            this.CBrest.Name = "CBrest";
            this.CBrest.SetText = "Wait on key down";
            this.CBrest.Size = new System.Drawing.Size(121, 21);
            this.CBrest.TabIndex = 17;
            this.CBrest.ToolTip = true;
            this.CBrest.UseVisualStyleBackColor = true;
            // 
            // CBloot
            // 
            this.CBloot.Location = new System.Drawing.Point(95, 66);
            this.CBloot.MultiKeys = false;
            this.CBloot.Name = "CBloot";
            this.CBloot.SetText = "Wait on key down";
            this.CBloot.Size = new System.Drawing.Size(121, 21);
            this.CBloot.TabIndex = 16;
            this.CBloot.ToolTip = true;
            this.CBloot.UseVisualStyleBackColor = true;
            // 
            // CBselect
            // 
            this.CBselect.Location = new System.Drawing.Point(95, 39);
            this.CBselect.MultiKeys = false;
            this.CBselect.Name = "CBselect";
            this.CBselect.SetText = "Wait on key down";
            this.CBselect.Size = new System.Drawing.Size(121, 21);
            this.CBselect.TabIndex = 15;
            this.CBselect.ToolTip = true;
            this.CBselect.UseVisualStyleBackColor = true;
            // 
            // CBforward
            // 
            this.CBforward.Location = new System.Drawing.Point(95, 12);
            this.CBforward.MultiKeys = false;
            this.CBforward.Name = "CBforward";
            this.CBforward.SetText = "Wait on key down";
            this.CBforward.Size = new System.Drawing.Size(121, 21);
            this.CBforward.TabIndex = 14;
            this.CBforward.ToolTip = true;
            this.CBforward.UseVisualStyleBackColor = true;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(4, 96);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(33, 19);
            this.label43.TabIndex = 13;
            this.label43.Text = "Rest";
            this.label43.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 19);
            this.label6.TabIndex = 8;
            this.label6.Text = "Select Yourself";
            this.label6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = "Loot";
            this.label4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(4, 43);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(83, 19);
            this.label40.TabIndex = 2;
            this.label40.Text = "Select Target";
            this.label40.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(91, 19);
            this.label39.TabIndex = 0;
            this.label39.Text = "Auto Forward";
            this.label39.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // TBPLog
            // 
            this.TBPLog.BackColor = System.Drawing.SystemColors.Control;
            this.TBPLog.Controls.Add(this.BTNlogclear);
            this.TBPLog.Controls.Add(this.RTBlog);
            this.TBPLog.Controls.Add(this.label37);
            this.TBPLog.Controls.Add(this.label36);
            this.TBPLog.Controls.Add(this.label35);
            this.TBPLog.Controls.Add(this.label34);
            this.TBPLog.Controls.Add(this.LogUse);
            this.TBPLog.Controls.Add(this.LogGather);
            this.TBPLog.Controls.Add(this.LogHunting);
            this.TBPLog.Controls.Add(this.LogSystem);
            this.TBPLog.HorizontalScrollbarBarColor = true;
            this.TBPLog.HorizontalScrollbarHighlightOnWheel = false;
            this.TBPLog.HorizontalScrollbarSize = 10;
            this.TBPLog.Location = new System.Drawing.Point(4, 38);
            this.TBPLog.Name = "TBPLog";
            this.TBPLog.Padding = new System.Windows.Forms.Padding(3);
            this.TBPLog.Size = new System.Drawing.Size(779, 460);
            this.TBPLog.TabIndex = 1;
            this.TBPLog.Text = "Log";
            this.TBPLog.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TBPLog.VerticalScrollbarBarColor = true;
            this.TBPLog.VerticalScrollbarHighlightOnWheel = false;
            this.TBPLog.VerticalScrollbarSize = 10;
            // 
            // BTNlogclear
            // 
            this.BTNlogclear.Location = new System.Drawing.Point(6, 378);
            this.BTNlogclear.Name = "BTNlogclear";
            this.BTNlogclear.Size = new System.Drawing.Size(581, 23);
            this.BTNlogclear.TabIndex = 11;
            this.BTNlogclear.Text = "Clear";
            this.BTNlogclear.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.BTNlogclear.UseSelectable = true;
            this.BTNlogclear.Click += new System.EventHandler(this.BTNlogclear_Click);
            // 
            // RTBlog
            // 
            this.RTBlog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.RTBlog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RTBlog.Cursor = System.Windows.Forms.Cursors.Default;
            this.RTBlog.DetectUrls = false;
            this.RTBlog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RTBlog.Location = new System.Drawing.Point(6, 6);
            this.RTBlog.Name = "RTBlog";
            this.RTBlog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RTBlog.Size = new System.Drawing.Size(581, 366);
            this.RTBlog.TabIndex = 10;
            this.RTBlog.Text = "";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(632, 104);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(30, 19);
            this.label37.TabIndex = 8;
            this.label37.Text = "Use";
            this.label37.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(611, 75);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(48, 19);
            this.label36.TabIndex = 6;
            this.label36.Text = "Gather";
            this.label36.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(605, 46);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 19);
            this.label35.TabIndex = 4;
            this.label35.Text = "Hunting";
            this.label35.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(608, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 19);
            this.label34.TabIndex = 2;
            this.label34.Text = "System";
            this.label34.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // LogUse
            // 
            this.LogUse.BackColor = System.Drawing.Color.Blue;
            this.LogUse.Location = new System.Drawing.Point(676, 104);
            this.LogUse.Name = "LogUse";
            this.LogUse.Size = new System.Drawing.Size(29, 20);
            this.LogUse.TabIndex = 9;
            this.LogUse.TabStop = false;
            this.LogUse.Click += new System.EventHandler(this.LogUse_Click);
            // 
            // LogGather
            // 
            this.LogGather.BackColor = System.Drawing.Color.Green;
            this.LogGather.Location = new System.Drawing.Point(676, 75);
            this.LogGather.Name = "LogGather";
            this.LogGather.Size = new System.Drawing.Size(29, 20);
            this.LogGather.TabIndex = 7;
            this.LogGather.TabStop = false;
            this.LogGather.Click += new System.EventHandler(this.LogGather_Click);
            // 
            // LogHunting
            // 
            this.LogHunting.BackColor = System.Drawing.Color.Red;
            this.LogHunting.Location = new System.Drawing.Point(676, 46);
            this.LogHunting.Name = "LogHunting";
            this.LogHunting.Size = new System.Drawing.Size(29, 20);
            this.LogHunting.TabIndex = 5;
            this.LogHunting.TabStop = false;
            this.LogHunting.Click += new System.EventHandler(this.LogHunting_Click);
            // 
            // LogSystem
            // 
            this.LogSystem.BackColor = System.Drawing.Color.Purple;
            this.LogSystem.Location = new System.Drawing.Point(676, 19);
            this.LogSystem.Name = "LogSystem";
            this.LogSystem.Size = new System.Drawing.Size(29, 20);
            this.LogSystem.TabIndex = 3;
            this.LogSystem.TabStop = false;
            this.LogSystem.Click += new System.EventHandler(this.LogSystem_Click);
            // 
            // ToolTipStat
            // 
            this.ToolTipStat.Style = MetroFramework.MetroColorStyle.Blue;
            this.ToolTipStat.StyleManager = null;
            this.ToolTipStat.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // TrayIcon
            // 
            this.TrayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.TrayIcon.BalloonTipText = "You have successfully minimized MyBot";
            this.TrayIcon.BalloonTipTitle = "Minimize to Tray App";
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "MyBot";
            this.TrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseDoubleClick);
            // 
            // TimerScript
            // 
            this.TimerScript.Tick += new System.EventHandler(this.TimerScript_Tick);
            // 
            // CMSScriptCreate
            // 
            this.CMSScriptCreate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem1,
            this.deleteToolStripMenuItem1,
            this.clearToolStripMenuItem});
            this.CMSScriptCreate.Name = "CMSScriptCreate";
            this.CMSScriptCreate.Size = new System.Drawing.Size(108, 70);
            // 
            // addToolStripMenuItem1
            // 
            this.addToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToolStripMenuItem,
            this.flyToolStripMenuItem,
            this.landToolStripMenuItem});
            this.addToolStripMenuItem1.Name = "addToolStripMenuItem1";
            this.addToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.addToolStripMenuItem1.Text = "Add";
            // 
            // goToolStripMenuItem
            // 
            this.goToolStripMenuItem.Name = "goToolStripMenuItem";
            this.goToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.goToolStripMenuItem.Text = "go";
            this.goToolStripMenuItem.Click += new System.EventHandler(this.goToolStripMenuItem_Click);
            // 
            // flyToolStripMenuItem
            // 
            this.flyToolStripMenuItem.Name = "flyToolStripMenuItem";
            this.flyToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.flyToolStripMenuItem.Text = "fly";
            this.flyToolStripMenuItem.Click += new System.EventHandler(this.flyToolStripMenuItem_Click);
            // 
            // landToolStripMenuItem
            // 
            this.landToolStripMenuItem.Name = "landToolStripMenuItem";
            this.landToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.landToolStripMenuItem.Text = "land";
            this.landToolStripMenuItem.Click += new System.EventHandler(this.landToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem1.Text = "Delete";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MetroMyBot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 640);
            this.Controls.Add(this.BTNStartBot);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.TBCMainWindow);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MetroMyBot";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.Text = "MyBot - Aion FreeToPlay";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MyBot_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MyBot_FormClosed);
            this.Load += new System.EventHandler(this.MyBot_Load);
            this.Resize += new System.EventHandler(this.TrayMinimizerForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.TBCMainWindow.ResumeLayout(false);
            this.TBPInformation.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.GBtarget.ResumeLayout(false);
            this.GBtarget.PerformLayout();
            this.GBplayer.ResumeLayout(false);
            this.GBplayer.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.TCplayer.ResumeLayout(false);
            this.TCplayer.PerformLayout();
            this.TBPWaypoint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.TBPPotion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVpotion)).EndInit();
            this.DGVskillcontext.ResumeLayout(false);
            this.TBPSkill.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVskill)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVbuff)).EndInit();
            this.TBPScript.ResumeLayout(false);
            this.TBPScript.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NRscriptstep)).EndInit();
            this.TBPAethertapping.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.CMSAetherSaveSpot.ResumeLayout(false);
            this.TBPCombat.ResumeLayout(false);
            this.TBPCombat.PerformLayout();
            this.TBPKey.ResumeLayout(false);
            this.TBPKey.PerformLayout();
            this.TBPLog.ResumeLayout(false);
            this.TBPLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogHunting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogSystem)).EndInit();
            this.CMSScriptCreate.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListBox ListAetherSafeSpot;
        private MetroFramework.Controls.MetroButton BTNAddAetherSafeSpot;
        public MetroFramework.Controls.MetroCheckBox CBaetherReturn;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox5;
        private MetroFramework.Controls.MetroTabPage TBPAethertapping;
        public System.Windows.Forms.PictureBox LogSystem;
        public System.Windows.Forms.PictureBox LogHunting;
        public System.Windows.Forms.PictureBox LogGather;
        public System.Windows.Forms.PictureBox LogUse;
        private MetroFramework.Controls.MetroLabel label34;
        private MetroFramework.Controls.MetroLabel label35;
        private MetroFramework.Controls.MetroLabel label36;
        private MetroFramework.Controls.MetroLabel label37;
        public System.Windows.Forms.RichTextBox RTBlog;
        private MetroFramework.Controls.MetroButton BTNlogclear;
        private MetroFramework.Controls.MetroTabPage TBPLog;
        private MetroFramework.Controls.MetroLabel label39;
        private MetroFramework.Controls.MetroLabel label40;
        private MetroFramework.Controls.MetroLabel label4;
        private MetroFramework.Controls.MetroLabel label6;
        private MetroFramework.Controls.MetroLabel label43;
        public MB.KeyButton.KeyButton CBforward;
        public MB.KeyButton.KeyButton CBselect;
        public MB.KeyButton.KeyButton CBloot;
        public MB.KeyButton.KeyButton CBrest;
        public MB.KeyButton.KeyButton CBselectyourself;
        private MetroFramework.Controls.MetroLabel label2;
        private MetroFramework.Controls.MetroLabel label1;
        public MB.KeyButton.KeyButton CBfly;
        public MB.KeyButton.KeyButton CBland;
        private MetroFramework.Controls.MetroTabPage TBPKey;
        private MetroFramework.Controls.MetroButton BTNscriptclear;
        private MetroFramework.Controls.MetroButton BTNrecord;
        private MetroFramework.Controls.MetroLabel label38;
        public System.Windows.Forms.NumericUpDown NRscriptstep;
        private MetroFramework.Controls.MetroButton BTNscriptsave;
        private MetroFramework.Controls.MetroButton BTNscriptload;
        private MetroFramework.Controls.MetroTabPage TBPScript;
        private MetroFramework.Controls.MetroTabPage TBPCombat;
        public MetroFramework.Controls.MetroGrid DGVbuff;
        private System.Windows.Forms.GroupBox groupBox3;
        public MetroFramework.Controls.MetroGrid DGVskill;
        private System.Windows.Forms.GroupBox groupBox6;
        private MetroFramework.Controls.MetroTabPage TBPSkill;
        public MetroFramework.Controls.MetroGrid DGVpotion;
        private MetroFramework.Controls.MetroTabPage TBPPotion;
        private MetroFramework.Controls.MetroButton BTNwaybrowse;
        public MetroFramework.Controls.MetroTextBox TBactionway;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroButton BTNdeathbrowse;
        public MetroFramework.Controls.MetroTextBox TBdeathway;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroTabPage TBPWaypoint;
        private MetroFramework.Controls.MetroTabPage TBPInformation;
        private MetroFramework.Controls.MetroTabControl TBCMainWindow;
        private System.Windows.Forms.ToolStripMenuItem alwaysOnTopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem targetInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bOTToolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        public MetroFramework.Controls.MetroButton BTNStartBot;
        private MetroFramework.Components.MetroToolTip ToolTipStat;
        private MetroFramework.Controls.MetroContextMenu DGVskillcontext;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private MetroFramework.Controls.MetroContextMenu CMSAetherSaveSpot;
        private System.Windows.Forms.Timer TimerScript;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private MetroFramework.Controls.MetroTabPage TCtarget;
        public MetroFramework.Controls.MetroLabel LBloggin;
        public MetroFramework.Controls.MetroLabel LBflytype;
        private MetroFramework.Controls.MetroTabPage TCplayer;
        private MetroFramework.Controls.MetroTabControl tabControl1;
        public System.Windows.Forms.Panel PNLclass;
        private MetroFramework.Controls.MetroLabel label33;
        private MetroFramework.Controls.MetroLabel label32;
        public System.Windows.Forms.Panel PNLdplevel;
        private MetroFramework.Controls.MetroLabel label31;
        private MetroFramework.Controls.MetroLabel label53;
        private MetroFramework.Controls.MetroLabel label54;
        public MB.exProgressBar.exProgressBar DRWhp;
        public MB.exProgressBar.exProgressBar DRWmp;
        public MB.exProgressBar.exProgressBar DRWdp;
        public MB.exProgressBar.exProgressBar DRWexp;
        public MB.exProgressBar.exProgressBar DRWfly;
        public System.Windows.Forms.GroupBox GBplayer;
        private MetroFramework.Controls.MetroLabel label50;
        private MetroFramework.Controls.MetroLabel label49;
        public MB.exProgressBar.exProgressBar DRWtargethp;
        public MB.exProgressBar.exProgressBar DRWtargetmp;
        public System.Windows.Forms.GroupBox GBtarget;
        private MetroFramework.Controls.MetroTextBox TBwindowwidth;
        private MetroFramework.Controls.MetroLabel label22;
        private MetroFramework.Controls.MetroTextBox TBwindowheight;
        private MetroFramework.Controls.MetroLabel label21;
        private MetroFramework.Controls.MetroLabel label24;
        private MetroFramework.Controls.MetroButton BTNresize;
        private MetroFramework.Controls.MetroLabel label23;
        private MetroFramework.Controls.MetroTextBox TBwindowy;
        private MetroFramework.Controls.MetroTextBox TBwindowx;
        private MetroFramework.Controls.MetroButton BTNfullscreen;
        private MetroFramework.Controls.MetroButton BTNHide;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ListBox ListScript;
        private System.Windows.Forms.ContextMenuStrip CMSScriptCreate;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem goToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem landToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BuffOnCombat;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffChainWait;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffChainLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffLastUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffCastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffCooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffSkillbar;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackChainWait;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackChain;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackLastUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackCastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackCooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackSkillbar;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionEffect;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionLastUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionCastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionCooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionSkillbar;
        private MetroFramework.Controls.MetroCheckBox CBHuntingGetLoot;
    }
}