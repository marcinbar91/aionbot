﻿namespace BOT
{
    partial class Launcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Launcher));
            this.BTNstart = new System.Windows.Forms.Button();
            this.BTNrefresh = new System.Windows.Forms.Button();
            this.CBclient = new System.Windows.Forms.ComboBox();
            this.BTNlaunchaion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BTNstart
            // 
            resources.ApplyResources(this.BTNstart, "BTNstart");
            this.BTNstart.Name = "BTNstart";
            this.BTNstart.UseVisualStyleBackColor = true;
            this.BTNstart.Click += new System.EventHandler(this.BTNstart_Click);
            // 
            // BTNrefresh
            // 
            resources.ApplyResources(this.BTNrefresh, "BTNrefresh");
            this.BTNrefresh.Name = "BTNrefresh";
            this.BTNrefresh.UseVisualStyleBackColor = true;
            this.BTNrefresh.Click += new System.EventHandler(this.BTNrefresh_Click);
            // 
            // CBclient
            // 
            this.CBclient.FormattingEnabled = true;
            resources.ApplyResources(this.CBclient, "CBclient");
            this.CBclient.Name = "CBclient";
            // 
            // BTNlaunchaion
            // 
            resources.ApplyResources(this.BTNlaunchaion, "BTNlaunchaion");
            this.BTNlaunchaion.Name = "BTNlaunchaion";
            this.BTNlaunchaion.UseVisualStyleBackColor = true;
            this.BTNlaunchaion.Click += new System.EventHandler(this.BTNlunchaion_Click);
            // 
            // Launcher
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BTNlaunchaion);
            this.Controls.Add(this.CBclient);
            this.Controls.Add(this.BTNrefresh);
            this.Controls.Add(this.BTNstart);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Launcher";
            this.Resizable = false;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Launcher_FormClosed);
            this.Load += new System.EventHandler(this.Launcher_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTNstart;
        private System.Windows.Forms.Button BTNrefresh;
        private System.Windows.Forms.ComboBox CBclient;
        private System.Windows.Forms.Button BTNlaunchaion;
    }
}