﻿namespace BOT
{
    partial class TargetInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TargetInfo));
            this.TargetHP = new System.Windows.Forms.Label();
            this.TargetMP = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opacityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.transparentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DRWtargetmp = new MB.exProgressBar.exProgressBar();
            this.DRWtargethp = new MB.exProgressBar.exProgressBar();
            this.TargetName = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TargetHP
            // 
            resources.ApplyResources(this.TargetHP, "TargetHP");
            this.TargetHP.BackColor = System.Drawing.Color.Transparent;
            this.TargetHP.ForeColor = System.Drawing.Color.Red;
            this.TargetHP.Name = "TargetHP";
            // 
            // TargetMP
            // 
            resources.ApplyResources(this.TargetMP, "TargetMP");
            this.TargetMP.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.TargetMP.Name = "TargetMP";
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveToolStripMenuItem,
            this.opacityToolStripMenuItem,
            this.transparentToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            // 
            // moveToolStripMenuItem
            // 
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            resources.ApplyResources(this.moveToolStripMenuItem, "moveToolStripMenuItem");
            this.moveToolStripMenuItem.Click += new System.EventHandler(this.moveToolStripMenuItem_Click);
            // 
            // opacityToolStripMenuItem
            // 
            this.opacityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.opacityToolStripMenuItem.Name = "opacityToolStripMenuItem";
            resources.ApplyResources(this.opacityToolStripMenuItem, "opacityToolStripMenuItem");
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // transparentToolStripMenuItem
            // 
            this.transparentToolStripMenuItem.Name = "transparentToolStripMenuItem";
            resources.ApplyResources(this.transparentToolStripMenuItem, "transparentToolStripMenuItem");
            this.transparentToolStripMenuItem.Click += new System.EventHandler(this.transparentToolStripMenuItem_Click);
            // 
            // DRWtargetmp
            // 
            this.DRWtargetmp.BackColor = System.Drawing.Color.Gray;
            this.DRWtargetmp.Color1 = System.Drawing.Color.DarkBlue;
            this.DRWtargetmp.Color2 = System.Drawing.Color.Blue;
            resources.ApplyResources(this.DRWtargetmp, "DRWtargetmp");
            this.DRWtargetmp.Maximum = 100F;
            this.DRWtargetmp.Minimum = 0F;
            this.DRWtargetmp.Name = "DRWtargetmp";
            this.DRWtargetmp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargetmp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargetmp.RoundedEdge = 80;
            this.DRWtargetmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWtargetmp.TextColor = System.Drawing.Color.White;
            this.DRWtargetmp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWtargetmp.Txt = null;
            this.DRWtargetmp.Value = 50F;
            // 
            // DRWtargethp
            // 
            this.DRWtargethp.BackColor = System.Drawing.Color.Gray;
            this.DRWtargethp.Color1 = System.Drawing.Color.DarkRed;
            this.DRWtargethp.Color2 = System.Drawing.Color.Red;
            resources.ApplyResources(this.DRWtargethp, "DRWtargethp");
            this.DRWtargethp.Maximum = 100F;
            this.DRWtargethp.Minimum = 0F;
            this.DRWtargethp.Name = "DRWtargethp";
            this.DRWtargethp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargethp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargethp.RoundedEdge = 80;
            this.DRWtargethp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWtargethp.TextColor = System.Drawing.Color.White;
            this.DRWtargethp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWtargethp.Txt = null;
            this.DRWtargethp.Value = 50F;
            // 
            // TargetName
            // 
            resources.ApplyResources(this.TargetName, "TargetName");
            this.TargetName.BackColor = System.Drawing.Color.Transparent;
            this.TargetName.ForeColor = System.Drawing.Color.Lime;
            this.TargetName.Name = "TargetName";
            // 
            // TargetInfo
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.ControlBox = false;
            this.Controls.Add(this.TargetName);
            this.Controls.Add(this.DRWtargetmp);
            this.Controls.Add(this.DRWtargethp);
            this.Controls.Add(this.TargetMP);
            this.Controls.Add(this.TargetHP);
            this.DisplayHeader = false;
            this.ForeColor = System.Drawing.Color.Transparent;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TargetInfo";
            this.Opacity = 0.7D;
            this.Resizable = false;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TopMost = true;
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TargetInfo_MouseMove);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TargetHP;
        private System.Windows.Forms.Label TargetMP;
        private System.Windows.Forms.Label TargetName;
        public System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opacityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem transparentToolStripMenuItem;
        private MB.exProgressBar.exProgressBar DRWtargethp;
        private MB.exProgressBar.exProgressBar DRWtargetmp;
    }
}