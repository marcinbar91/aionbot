﻿using MetroFramework.Forms;
using System;
using System.Drawing;


namespace BOT
{
    public partial class TargetInfo : MetroForm
    {
        private bool dragging;
        private System.Drawing.Point dragCursorPoint;
        private System.Drawing.Point dragFormPoint;
        public TargetInfo()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            TargetName.Text = Value.Target.Lvl + " - " + Value.Target.Name;
            //TargetHP.Text = "HP: " + Get.TargetCurHP + "/" + Get.TargetMaxHP;
            //TargetMP.Text = "MP: " + Get.TargetCurMP + "/" + Get.TargetMaxMP;
            DRWtargethp.Maximum = Value.Target.HP.Max;
            DRWtargethp.Value = Value.Target.HP.Cur;
            DRWtargetmp.Maximum = Value.Target.MP.Max;
            DRWtargetmp.Value = Value.Target.MP.Cur;

            DRWtargethp.Txt = Value.Target.HP.Cur.ToString("N0") + " / " + Value.Target.HP.Max.ToString("N0");
            DRWtargetmp.Txt = Value.Target.MP.Cur.ToString("N0") + " / " + Value.Target.MP.Max.ToString("N0");
        }


        private void moveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (moveToolStripMenuItem.Checked)
                moveToolStripMenuItem.Checked = false;
            else
                moveToolStripMenuItem.Checked = true;
        }



        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Opacity = .20;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.Opacity = .40;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            this.Opacity = .60;
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            this.Opacity = .80;
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            this.Opacity = 1;
        }

        private void transparentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (transparentToolStripMenuItem.Checked)
            {
                transparentToolStripMenuItem.Checked = false;
                this.TransparencyKey = Color.White;
            }
            else
            {
                transparentToolStripMenuItem.Checked = true;
                this.BackColor = Color.Black;
                this.TransparencyKey = Color.Black;
                
            }
        }

        private void TargetInfo_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = System.Windows.Forms.Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void TargetInfo_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            dragging = false;
        }
        private void TargetInfo_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (dragging && moveToolStripMenuItem.Checked)
            {
                System.Drawing.Point dif = System.Drawing.Point.Subtract(System.Windows.Forms.Cursor.Position, new Size(dragCursorPoint));
                this.Location = System.Drawing.Point.Add(dragFormPoint, new Size(dif));
            }
        }

     
    }
}
