﻿using MetroFramework.Forms;
using System;
using System.Windows.Forms;

namespace BOT
{
    public partial class PlayerInfo : MetroForm
    {
        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }
        public PlayerInfo()
        {
            InitializeComponent();
        }

         public void RefreshForm()
         {
                 TBhp.Text = Convert.ToString(Value.Player.HP.Max);
                 TBmp.Text = Convert.ToString(Value.Player.MP.Max);
                 TBpower.Text =  Convert.ToString(Value.Stat.Power);
                 TBhealth.Text = Convert.ToString(Value.Stat.Health);
                 TBagility.Text =  Convert.ToString(Value.Stat.Agility);
                 TBaccuracy.Text = Convert.ToString(Value.Stat.Accuracy);
                 TBknowledge.Text = Convert.ToString(Value.Stat.Knowledge);
                 TBwill.Text =  Convert.ToString(Value.Stat.Will);
                 LBcube.Text = "Cube: " + Value.Player.Cube.Cur + "/" + Value.Player.Cube.Max;
                 LBlegion.Text = "Legion: " + Value.Player.Legion.Name;
                 LBlegionAP.Text = "AP: " + Value.Player.Legion.AP;
                 LBpositionx.Text = "X:" + Value.Player.Position.X;
                 LBpositiony.Text = "Y:" + Value.Player.Position.Y;
                 LBpositionz.Text = "Z:" + Value.Player.Position.Z;
                 LBcamerax.Text = "X: " + Value.Player.Camera.X;
                 LBcameray.Text = "Y: " + Value.Player.Camera.Y;
                 //Stat
                 //1
                 TBattackmain.Text = Convert.ToString(Value.Stat.AttackMain);
                 TBattackoff.Text = Convert.ToString(Value.Stat.AttackOff);
                 TBaccmain.Text = Convert.ToString(Value.Stat.AccMain);
                 TBaccoff.Text = Convert.ToString(Value.Stat.AccOff);
                 TBcritmain.Text = Convert.ToString(Value.Stat.CritStrikeMain);
                 TBcritoff.Text = Convert.ToString(Value.Stat.CritStrikeOff);
                 TBatkspeed.Text = Convert.ToString(Value.Stat.AttackSpeed);
                 TBspeed.Text = Convert.ToString(Value.Stat.Speed);
                 //2
                 TBphydef.Text = Convert.ToString(Value.Stat.PhyDef);
                 TBblock.Text = Convert.ToString(Value.Stat.Block);
                 TBparry.Text = Convert.ToString(Value.Stat.Parry);
                 TBevansion.Text = Convert.ToString(Value.Stat.Evansion);
                 TBcritstrikeres.Text = Convert.ToString(Value.Stat.CritStrikeResist);
                 TBcritstrikefort.Text = Convert.ToString(Value.Stat.CritStrikeFort);
                 //3
                 TBmagicboost.Text = Convert.ToString(Value.Stat.MagBoost);
                 TBmagicacc.Text = Convert.ToString(Value.Stat.MagAcc);
                 TBcritspell.Text = Convert.ToString(Value.Stat.CritSpell);
                 TBcastspeed.Text = Convert.ToString(Value.Stat.CastSpeed);
                 TBhealingboost.Text = Convert.ToString(Value.Stat.HealingBoost);
                 //4
                 TBmagicdef.Text = Convert.ToString(Value.Stat.MagicDef);
                 TBmagicsupp.Text = Convert.ToString(Value.Stat.MagicSupp);
                 TBmagicres.Text = Convert.ToString(Value.Stat.MagicResist);
                 TBcritspellres.Text = Convert.ToString(Value.Stat.CritSpellResist);
                 TBspellfort.Text = Convert.ToString(Value.Stat.SpellFortitude);
                 TBfireres.Text = Convert.ToString(Value.Stat.FireResist);
                 TBwindres.Text = Convert.ToString(Value.Stat.WindResist);
                 TBwaterres.Text = Convert.ToString(Value.Stat.WaterResist);
                 TBearthres.Text = Convert.ToString(Value.Stat.EarthResist);

                 TBrank.Text = Convert.ToString(Value.Stat.RankNumber);
                 TBcurrentrank.Text = Rank(Value.Stat.Rank);
                 TBtodayap.Text = Convert.ToString(Value.Stat.AbyssPoint.Today);
                 TBtodayely.Text = Convert.ToString(Value.Stat.Kill.Today);
                 TBtodayhp.Text = Convert.ToString(Value.Stat.HonorPoint.Today);
                 TBtotalap.Text = Convert.ToString(Value.Stat.AbyssPoint.Total);
                 TBtotalely.Text = Convert.ToString(Value.Stat.Kill.Total);
                 TBtotalhp.Text = Convert.ToString(Value.Stat.HonorPoint.Total);
                 TBthisweekap.Text = Convert.ToString(Value.Stat.AbyssPoint.ThisWeek);
                 TBthisweekely.Text = Convert.ToString(Value.Stat.Kill.ThisWeek);
                 TBthisweekhp.Text = Convert.ToString(Value.Stat.HonorPoint.ThisWeek);
                 TBlastweekap.Text = Convert.ToString(Value.Stat.AbyssPoint.LastWeek);
                 TBlastweekely.Text = Convert.ToString(Value.Stat.Kill.LastWeek);
                 TBlastweekhp.Text = Convert.ToString(Value.Stat.HonorPoint.LastWeek);
                
        }

         private void timer1_Tick(object sender, EventArgs e)
         {
             RefreshForm();
         }

         public void RUN(bool r)
         {
             if (r)
                 timer1.Start();
             else
                 timer1.Stop();
         }

         public string Rank(int r)
         {
             switch (r)
             {
                 case 1:
                     return "Soldier, Rank 9";
                 case 2:
                     return "Soldier, Rank 8";
                 case 3:
                     return "Soldier, Rank 7";
                 case 4:
                     return "Soldier, Rank 6";
                 case 5:
                     return "Soldier, Rank 5";
                 case 6:
                     return "Soldier, Rank 4";
                 case 7:
                     return "Soldier, Rank 3";
                 case 8:
                     return "Soldier, Rank 2";
                 case 9:
                     return "Soldier, Rank 1";
                 case 10:
                     return "Army 1-Star Officer";
                 case 11:
                     return "Army 2-Star Officer";
                 case 12:
                     return "Army 3-Star Officer";
                 case 13:
                     return "Army 4-Star Officer";
                 case 14:
                     return "Army 5-Star Officer";
                 case 15:
                     return "General";
                 case 16:
                     return "Great General";
                 case 17:
                     return "Commander";
                 case 18:
                     return "Governor";
             }

             return "No Rank";
         }

        
    }
}
