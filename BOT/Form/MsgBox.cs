﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BOT
{
    public partial class MsgBox : MetroForm
    {

        public enum Display
        {
            Potion,
            Skill,
            Buff,
        }

        public MsgBox(Display a)
        {
            this.TopMost = true;
            InitializeComponent();
            Dis = a;
        }

        private Display Dis { get; set; }

        public string Skillbar { get; set; }
        public string Key { get; set; }
        public string Effect { get; set; }
        public string Percent { get; set; }
        public string Cooldown { get; set; }
        public string CastTime { get; set; }
        public string ChainLVL { get; set; }
        public string ChainWait { get; set; }
        public bool OnCombat { get; set; }

        private void BTNok_Click(object sender, EventArgs e)
        {
            Skillbar = ADDskillbar.Text;
            Key = AddKey.Text;
            Effect = AddPotionEffect.Text;
            Percent = AddPotionPercent.Text;
            Cooldown = AddCooldown.Text;
            CastTime = AddCastTime.Text;
            ChainLVL = AddChainLevel.Text;
            ChainWait = AddChainWait.Text;
            OnCombat = ADDOnCombat.Checked;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BTNcancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void MsgBox_Load(object sender, EventArgs e)
        {
            int i = 0;
            tableLayoutPanel1.Controls.Add(ADDskillbar, 0, i);
            tableLayoutPanel1.Controls.Add(LBLskillbar, 0, i++);

            tableLayoutPanel1.Controls.Add(AddKey, 0, i);
            tableLayoutPanel1.Controls.Add(LBLkey, 0, i++);

            tableLayoutPanel1.Controls.Add(AddCastTime, 0, i);
            tableLayoutPanel1.Controls.Add(LBLcasttime, 0, i++);

            tableLayoutPanel1.Controls.Add(AddCooldown, 0, i);
            tableLayoutPanel1.Controls.Add(LBLCooldown, 0, i++);

            if (Dis == Display.Skill || Dis == Display.Buff)
            {
                tableLayoutPanel1.Controls.Add(AddChainLevel, 0, i);
                tableLayoutPanel1.Controls.Add(LBLchainlvl, 0, i++);

                tableLayoutPanel1.Controls.Add(AddChainWait, 0, i);
                tableLayoutPanel1.Controls.Add(LBLchainwait, 0, i++);

                if (Dis == Display.Buff)
                {
                    tableLayoutPanel1.Controls.Add(ADDOnCombat, 0, i);
                    tableLayoutPanel1.Controls.Add(lblOnCombat, 0, i++);
                }
            }
            else
            {
                tableLayoutPanel1.Controls.Add(AddPotionEffect, 0, i);
                tableLayoutPanel1.Controls.Add(LBLeffect, 0, i++);

                tableLayoutPanel1.Controls.Add(AddPotionPercent, 0, i);
                tableLayoutPanel1.Controls.Add(LBLpercent, 0, i++);
            }
            this.Height = tableLayoutPanel1.Height + 100 + BTNok.Height;
        }

    }
}
