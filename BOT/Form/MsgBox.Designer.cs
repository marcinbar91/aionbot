﻿namespace BOT
{
    partial class MsgBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTNok = new MetroFramework.Controls.MetroButton();
            this.BTNcancel = new MetroFramework.Controls.MetroButton();
            this.AddChainWait = new System.Windows.Forms.NumericUpDown();
            this.AddChainLevel = new System.Windows.Forms.NumericUpDown();
            this.AddCastTime = new System.Windows.Forms.NumericUpDown();
            this.LBLpercent = new MetroFramework.Controls.MetroLabel();
            this.LBLskillbar = new MetroFramework.Controls.MetroLabel();
            this.LBLcasttime = new MetroFramework.Controls.MetroLabel();
            this.LBLCooldown = new MetroFramework.Controls.MetroLabel();
            this.LBLkey = new MetroFramework.Controls.MetroLabel();
            this.LBLeffect = new MetroFramework.Controls.MetroLabel();
            this.AddPotionPercent = new System.Windows.Forms.NumericUpDown();
            this.ADDskillbar = new MetroFramework.Controls.MetroComboBox();
            this.AddCooldown = new System.Windows.Forms.NumericUpDown();
            this.AddPotionEffect = new MetroFramework.Controls.MetroComboBox();
            this.AddKey = new MB.KeyButton.KeyButton();
            this.ADDOnCombat = new MetroFramework.Controls.MetroCheckBox();
            this.LBLchainwait = new MetroFramework.Controls.MetroLabel();
            this.LBLchainlvl = new MetroFramework.Controls.MetroLabel();
            this.lblOnCombat = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCastTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPotionPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCooldown)).BeginInit();
            this.SuspendLayout();
            // 
            // BTNok
            // 
            this.BTNok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNok.AutoSize = true;
            this.BTNok.Location = new System.Drawing.Point(141, 388);
            this.BTNok.Name = "BTNok";
            this.BTNok.Size = new System.Drawing.Size(75, 23);
            this.BTNok.TabIndex = 0;
            this.BTNok.Text = "OK";
            this.BTNok.UseSelectable = true;
            this.BTNok.Click += new System.EventHandler(this.BTNok_Click);
            // 
            // BTNcancel
            // 
            this.BTNcancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNcancel.AutoSize = true;
            this.BTNcancel.Location = new System.Drawing.Point(222, 388);
            this.BTNcancel.Name = "BTNcancel";
            this.BTNcancel.Size = new System.Drawing.Size(75, 23);
            this.BTNcancel.TabIndex = 1;
            this.BTNcancel.Text = "Cancel";
            this.BTNcancel.UseSelectable = true;
            this.BTNcancel.Click += new System.EventHandler(this.BTNcancel_Click);
            // 
            // AddChainWait
            // 
            this.AddChainWait.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddChainWait.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddChainWait.Location = new System.Drawing.Point(141, 241);
            this.AddChainWait.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AddChainWait.Name = "AddChainWait";
            this.AddChainWait.Size = new System.Drawing.Size(132, 29);
            this.AddChainWait.TabIndex = 116;
            this.AddChainWait.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // AddChainLevel
            // 
            this.AddChainLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddChainLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddChainLevel.Location = new System.Drawing.Point(141, 207);
            this.AddChainLevel.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AddChainLevel.Name = "AddChainLevel";
            this.AddChainLevel.Size = new System.Drawing.Size(132, 29);
            this.AddChainLevel.TabIndex = 117;
            // 
            // AddCastTime
            // 
            this.AddCastTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddCastTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddCastTime.Location = new System.Drawing.Point(141, 71);
            this.AddCastTime.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.AddCastTime.Name = "AddCastTime";
            this.AddCastTime.Size = new System.Drawing.Size(132, 29);
            this.AddCastTime.TabIndex = 132;
            this.AddCastTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // LBLpercent
            // 
            this.LBLpercent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLpercent.Location = new System.Drawing.Point(3, 170);
            this.LBLpercent.Name = "LBLpercent";
            this.LBLpercent.Size = new System.Drawing.Size(132, 34);
            this.LBLpercent.TabIndex = 149;
            this.LBLpercent.Text = "Percent";
            this.LBLpercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBLskillbar
            // 
            this.LBLskillbar.BackColor = System.Drawing.Color.Transparent;
            this.LBLskillbar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLskillbar.ForeColor = System.Drawing.Color.White;
            this.LBLskillbar.Location = new System.Drawing.Point(3, 0);
            this.LBLskillbar.Name = "LBLskillbar";
            this.LBLskillbar.Size = new System.Drawing.Size(132, 34);
            this.LBLskillbar.TabIndex = 154;
            this.LBLskillbar.Text = "Skillbar";
            this.LBLskillbar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBLcasttime
            // 
            this.LBLcasttime.BackColor = System.Drawing.Color.Transparent;
            this.LBLcasttime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLcasttime.ForeColor = System.Drawing.Color.White;
            this.LBLcasttime.Location = new System.Drawing.Point(3, 68);
            this.LBLcasttime.Name = "LBLcasttime";
            this.LBLcasttime.Size = new System.Drawing.Size(132, 34);
            this.LBLcasttime.TabIndex = 152;
            this.LBLcasttime.Text = "Cast Time";
            this.LBLcasttime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBLCooldown
            // 
            this.LBLCooldown.BackColor = System.Drawing.Color.Transparent;
            this.LBLCooldown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLCooldown.ForeColor = System.Drawing.Color.White;
            this.LBLCooldown.Location = new System.Drawing.Point(3, 102);
            this.LBLCooldown.Name = "LBLCooldown";
            this.LBLCooldown.Size = new System.Drawing.Size(132, 34);
            this.LBLCooldown.TabIndex = 150;
            this.LBLCooldown.Text = "Cooldown";
            this.LBLCooldown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBLkey
            // 
            this.LBLkey.BackColor = System.Drawing.Color.Transparent;
            this.LBLkey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLkey.ForeColor = System.Drawing.Color.White;
            this.LBLkey.Location = new System.Drawing.Point(3, 34);
            this.LBLkey.Name = "LBLkey";
            this.LBLkey.Size = new System.Drawing.Size(132, 34);
            this.LBLkey.TabIndex = 151;
            this.LBLkey.Text = "Key";
            this.LBLkey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBLeffect
            // 
            this.LBLeffect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLeffect.Location = new System.Drawing.Point(3, 136);
            this.LBLeffect.Name = "LBLeffect";
            this.LBLeffect.Size = new System.Drawing.Size(132, 34);
            this.LBLeffect.TabIndex = 153;
            this.LBLeffect.Text = "Effect";
            this.LBLeffect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddPotionPercent
            // 
            this.AddPotionPercent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddPotionPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddPotionPercent.Location = new System.Drawing.Point(141, 173);
            this.AddPotionPercent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AddPotionPercent.Name = "AddPotionPercent";
            this.AddPotionPercent.Size = new System.Drawing.Size(132, 29);
            this.AddPotionPercent.TabIndex = 140;
            this.AddPotionPercent.Value = new decimal(new int[] {
            99,
            0,
            0,
            0});
            // 
            // ADDskillbar
            // 
            this.ADDskillbar.FormattingEnabled = true;
            this.ADDskillbar.ItemHeight = 23;
            this.ADDskillbar.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.ADDskillbar.Location = new System.Drawing.Point(141, 3);
            this.ADDskillbar.Name = "ADDskillbar";
            this.ADDskillbar.Size = new System.Drawing.Size(132, 29);
            this.ADDskillbar.TabIndex = 146;
            this.ADDskillbar.UseSelectable = true;
            // 
            // AddCooldown
            // 
            this.AddCooldown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddCooldown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddCooldown.Location = new System.Drawing.Point(141, 105);
            this.AddCooldown.Maximum = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            this.AddCooldown.Name = "AddCooldown";
            this.AddCooldown.Size = new System.Drawing.Size(132, 29);
            this.AddCooldown.TabIndex = 131;
            this.AddCooldown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // AddPotionEffect
            // 
            this.AddPotionEffect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddPotionEffect.FormattingEnabled = true;
            this.AddPotionEffect.ItemHeight = 23;
            this.AddPotionEffect.Items.AddRange(new object[] {
            "HP",
            "MP",
            "Vigor",
            "Flight"});
            this.AddPotionEffect.Location = new System.Drawing.Point(141, 139);
            this.AddPotionEffect.Name = "AddPotionEffect";
            this.AddPotionEffect.Size = new System.Drawing.Size(132, 29);
            this.AddPotionEffect.TabIndex = 139;
            this.AddPotionEffect.UseSelectable = true;
            // 
            // AddKey
            // 
            this.AddKey.BackColor = System.Drawing.SystemColors.Control;
            this.AddKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddKey.Location = new System.Drawing.Point(141, 37);
            this.AddKey.MultiKeys = false;
            this.AddKey.Name = "AddKey";
            this.AddKey.SetText = "Wait on key down";
            this.AddKey.Size = new System.Drawing.Size(132, 28);
            this.AddKey.TabIndex = 137;
            this.AddKey.ToolTip = true;
            this.AddKey.UseVisualStyleBackColor = false;
            // 
            // ADDOnCombat
            // 
            this.ADDOnCombat.BackColor = System.Drawing.Color.Transparent;
            this.ADDOnCombat.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ADDOnCombat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ADDOnCombat.Location = new System.Drawing.Point(141, 275);
            this.ADDOnCombat.Name = "ADDOnCombat";
            this.ADDOnCombat.Size = new System.Drawing.Size(132, 37);
            this.ADDOnCombat.TabIndex = 159;
            this.ADDOnCombat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ADDOnCombat.UseSelectable = true;
            // 
            // LBLchainwait
            // 
            this.LBLchainwait.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLchainwait.Location = new System.Drawing.Point(3, 238);
            this.LBLchainwait.Name = "LBLchainwait";
            this.LBLchainwait.Size = new System.Drawing.Size(132, 34);
            this.LBLchainwait.TabIndex = 158;
            this.LBLchainwait.Text = "Chain Wait";
            this.LBLchainwait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBLchainlvl
            // 
            this.LBLchainlvl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBLchainlvl.Location = new System.Drawing.Point(3, 204);
            this.LBLchainlvl.Name = "LBLchainlvl";
            this.LBLchainlvl.Size = new System.Drawing.Size(132, 34);
            this.LBLchainlvl.TabIndex = 157;
            this.LBLchainlvl.Text = "Chain Level";
            this.LBLchainlvl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOnCombat
            // 
            this.lblOnCombat.AutoSize = true;
            this.lblOnCombat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOnCombat.Location = new System.Drawing.Point(3, 272);
            this.lblOnCombat.Name = "lblOnCombat";
            this.lblOnCombat.Size = new System.Drawing.Size(132, 43);
            this.lblOnCombat.TabIndex = 160;
            this.lblOnCombat.Text = "OnCombat";
            this.lblOnCombat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(23, 63);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel1.TabIndex = 161;
            // 
            // MsgBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(324, 434);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.BTNcancel);
            this.Controls.Add(this.BTNok);
            this.Name = "MsgBox";
            this.Resizable = false;
            this.Text = "MsgBox";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.MsgBox_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AddChainWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCastTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPotionPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCooldown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton BTNok;
        private MetroFramework.Controls.MetroButton BTNcancel;
        private System.Windows.Forms.NumericUpDown AddCastTime;
        private System.Windows.Forms.NumericUpDown AddChainLevel;
        private System.Windows.Forms.NumericUpDown AddChainWait;
        private MB.KeyButton.KeyButton AddKey;
        public MetroFramework.Controls.MetroComboBox AddPotionEffect;
        private System.Windows.Forms.NumericUpDown AddCooldown;
        public MetroFramework.Controls.MetroComboBox ADDskillbar;
        private System.Windows.Forms.NumericUpDown AddPotionPercent;
        private MetroFramework.Controls.MetroLabel LBLeffect;
        private MetroFramework.Controls.MetroLabel LBLkey;
        private MetroFramework.Controls.MetroLabel LBLCooldown;
        private MetroFramework.Controls.MetroLabel LBLcasttime;
        private MetroFramework.Controls.MetroLabel LBLskillbar;
        private MetroFramework.Controls.MetroLabel LBLpercent;
        private MetroFramework.Controls.MetroCheckBox ADDOnCombat;
        private MetroFramework.Controls.MetroLabel LBLchainwait;
        private MetroFramework.Controls.MetroLabel LBLchainlvl;
        private MetroFramework.Controls.MetroLabel lblOnCombat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}