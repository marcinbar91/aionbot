﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using MB.ProcessControl;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using MetroFramework.Forms;

namespace BOT
{
    public partial class MyBot : Form
    {
        float LastPosX = 0, LastPosY = 0;

        Setting setting = new Setting();
        TargetInfo target = new TargetInfo();
        PlayerInfo player = new PlayerInfo();
        public static Bot botcontrol = new Bot();
        Thread threadStart;
        static bool stopBot = false;
        Window window = new Window(Launcher.processHandlekey);

        Potion pot = new Potion();

        public MyBot()
        {
            this.TopMost = true;
            InitializeComponent();
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            //ScriptItem asd2 = asd.CreatePathToSafeSpot(ListAetherSafeSpot);
            //foreach (PointXYZ p in asd2)
            //{
            //    Console.WriteLine(p.X);
            //}


            // Key.GoForward();

            //await Hunting.Kill();

            //Script.Start(TBaethertappingway.Text);
            //Buff.Use();
            //Script.GetValue();
            //threadStart.Abort();
            //Key.GoForward2();
            //timer1.Start();

        }


        private void button3_Click(object sender, EventArgs e)
        {
            //Waypoint1 wa = new Waypoint1();
            //wa.GatheringWaypoint();
            //timer1.Stop(); 
            //Tools.CaptureApplication();
        }

        private void MyBot_FormClosed(object sender, FormClosedEventArgs e)
        {
            stopBot = true;
            threadStart.Join();
            //Program.launcher.Show();
        }

        private void MyBot_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "MyBot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            {
                e.Cancel = true;
                Program.bot = null;
            }
            Program.launcher.Visible = true;

        }



        private static void RefreshValueAndForm()
        {
            while (true && !stopBot)
            {
                RefreshForm.Do();
                Thread.Sleep(200);
            }
        }



        #region Log
        private void LogSystem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }

        private void LogHunting_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }

        private void LogGather_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }

        private void LogUse_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }
        private void BTNlogclear_Click(object sender, EventArgs e)
        {
            RTBlog.Clear();
        }

        #endregion

        #region Script
        private void BTNscriptclear_Click(object sender, EventArgs e)
        {
            RTBscript.ResetText();
        }

        private void BTNrecord_Click(object sender, EventArgs e)
        {
            if (BTNrecord.Text == "Record")
            {
                BTNrecord.Text = "Stop";
                TimerScript.Start();
            }
            else if (BTNrecord.Text == "Stop")
            {
                BTNrecord.Text = "Record";
                TimerScript.Stop();
            }

        }

        private void TimerScript_Tick(object sender, EventArgs e)
        {
            if (RTBscript.TextLength == 0)
            {
                RTBscript.AppendText("go(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
                LastPosX = Value.Player.Position.X;
                LastPosY = Value.Player.Position.Y;
            }
            if ((int)Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, LastPosX, LastPosY) > NRscriptstep.Value)
            {
                LastPosX = Value.Player.Position.X;
                LastPosY = Value.Player.Position.Y;
                RTBscript.AppendText(Environment.NewLine + "go(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
            }
        }

        private void BTNscriptsave_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Waypoint (*.way)|*.way";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(save.FileName);
                if (RTBscript.Text != "")
                    writer.WriteLine(RTBscript.Text);
                writer.Close();
            }
            save.Dispose();
        }

        private void BTNscriptload_Click(object sender, EventArgs e)
        {
            RTBscript.ResetText();
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(open.FileName);
                string text = sr.ReadLine();
                RTBscript.AppendText(text);
                while (!sr.EndOfStream)
                {
                    text = sr.ReadLine();
                    if (text != "")
                        RTBscript.AppendText(Environment.NewLine + text);
                }
                sr.Close();
            }
            open.Dispose();
        }
        private void BTNscriptadd_Click(object sender, EventArgs e)
        {
            if (RTBscript.TextLength == 0)
                RTBscript.AppendText("go(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z);
            else
                RTBscript.AppendText(Environment.NewLine + "go(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
        }
        private void CBscriptadd_MouseHover(object sender, EventArgs e)
        {
            CBscriptadd.DroppedDown = true;
        }

        private void CBscriptadd_MouseLeave(object sender, EventArgs e)
        {
            CBscriptadd.DroppedDown = false;
        }

        private void CBscriptadd_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (RTBscript.TextLength == 0)
            {
                if (CBscriptadd.SelectedIndex == 0)
                    RTBscript.AppendText("go(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
                if (CBscriptadd.SelectedIndex == 1)
                    RTBscript.AppendText("fly");
                if (CBscriptadd.SelectedIndex == 2)
                    RTBscript.AppendText("land");
            }
            else
            {
                if (CBscriptadd.SelectedIndex == 0)
                    RTBscript.AppendText(Environment.NewLine + "go(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
                if (CBscriptadd.SelectedIndex == 1)
                    RTBscript.AppendText(Environment.NewLine + "fly");
                if (CBscriptadd.SelectedIndex == 2)
                    RTBscript.AppendText(Environment.NewLine + "land");
            }
        }

        private void BTNwaybrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == DialogResult.OK)
            {
                if (Script.CheckScript(new Script(open.FileName)))
                    TBactionway.Text = open.FileName;
                else
                    MessageBox.Show("The script is incorrect");
            }
            open.Dispose();
        }
        private void BTNdeathbrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == DialogResult.OK)
            {
                if (Script.CheckScript(new Script(open.FileName)))
                    TBdeathway.Text = open.FileName;
                else
                    MessageBox.Show("The script is incorrect");
            }
            open.Dispose();
        }


        public void BTNStartBot_Click(object sender, EventArgs e)
        {
            if (BTNStartBot.Text == "Start Bot")
            {
                Bot.StartBot();
            }
            else
            {
                Bot.Stop();
            }
        }
        #region Aethertapping
        private void BTNAddAtherSafeSpot_Click(object sender, EventArgs e)
        {
            ListAetherSafeSpot.Items.Add("(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
        }
        private void TSMdelete_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(ListAetherSafeSpot);
            selectedItems = ListAetherSafeSpot.SelectedItems;

            if (ListAetherSafeSpot.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    ListAetherSafeSpot.Items.Remove(selectedItems[i]);
            }
            else
                MessageBox.Show("Select any item.");
        }
        #endregion Aethertapping
        #endregion Script

        #region Child Form
        private void MIprofile_Click(object sender, EventArgs e)
        {
            if (MIprofile.Checked)
            {
                player.RUN(false);
                MIprofile.Checked = false;
                player.Hide();
            }
            else
            {
                MIprofile.Checked = true;
                player.Show();
                player.RUN(true);
            }
        }

        private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (alwaysOnTopToolStripMenuItem.Checked)
            {
                this.TopMost = false;
                alwaysOnTopToolStripMenuItem.Checked = false;
            }
            else
            {
                this.TopMost = true;
                alwaysOnTopToolStripMenuItem.Checked = true;
            }
        }

        private void MItargetInfo_Click(object sender, EventArgs e)
        {
            if (MItargetInfo.Checked)
            {
                target.Hide();
                target.timer1.Enabled = false;
                MItargetInfo.Checked = false;

            }
            else
            {
                target.timer1.Enabled = true;
                MItargetInfo.Checked = true;
                target.Show();

            }
        }

        private void MIexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Setting
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want save as default setting?", "MyBot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Setting (*.ini)|*.ini";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(save.FileName))
                        File.Delete(save.FileName);
                    setting.SaveSettingIni(save.FileName);
                }
                save.Dispose();
            }
            else
            {
                if (File.Exists(Setting.defaultpath))
                    File.Delete(Setting.defaultpath);
                setting.SaveSettingIni(Setting.defaultpath);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Setting (*.ini)|*.ini";
            if (open.ShowDialog() == DialogResult.OK)
            {
                setting.LoadSettingIni(open.FileName);
            }
            open.Dispose();
        }
        #endregion Setting

        private void BTNskilladd_Click(object sender, EventArgs e)
        {

            Button btnSender = (Button)sender;
            System.Drawing.Point ptLowerLeft = new System.Drawing.Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);
            CMSskill.Show(ptLowerLeft);

            /*
            int n = dataGridView1.Rows.Add();

            dataGridView1.Rows[n].Cells[0].Value = title;
            dataGridView1.Rows[n].Cells[1].Value = dateTimeNow;

            //
            // The second cell is a date cell, use typeof(DateTime).
            //
            dataGridView1.Rows[n].Cells[1].ValueType = typeof(DateTime);
            dataGridView1.Rows[n].Cells[2].Value = wordCount;
             */
        }

        private void attackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DGVskill.AllowUserToAddRows = true;
            DataGridViewRow row = (DataGridViewRow)DGVskill.Rows[0].Clone();
            row.Cells[0].Value = AddSkillbar.Text;
            row.Cells[1].Value = AddKey.Text;
            row.Cells[2].Value = AddCooldown.Text;
            row.Cells[3].Value = AddCastTime.Text;
            row.Cells[4].Value = AddChainLevel.Text;
            row.Cells[5].Value = AddChainWait.Text;
            row.Cells[6].Value = new DateTime(2014, 1, 1, 0, 0, 0);
            DGVskill.Rows.Add(row);
            DGVskill.AllowUserToAddRows = false;
        }

        private void buffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DGVbuff.AllowUserToAddRows = true;
            DataGridViewRow row = (DataGridViewRow)DGVbuff.Rows[0].Clone();
            row.Cells[0].Value = AddSkillbar.Text;
            row.Cells[1].Value = AddKey.Text;
            row.Cells[2].Value = AddCooldown.Text;
            row.Cells[3].Value = AddCastTime.Text;
            row.Cells[4].Value = AddChainLevel.Text;
            row.Cells[5].Value = AddChainWait.Text;
            row.Cells[6].Value = CBOnCombat.CheckState;
            row.Cells[7].Value = new DateTime(2014, 1, 1, 0, 0, 0);
            DGVbuff.Rows.Add(row);
            DGVbuff.AllowUserToAddRows = false;
        }

        private void DGVskill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
            {
                moveUp();
            }
            if (e.KeyCode.Equals(Keys.Down))
            {
                moveDown();
            }
            e.Handled = true;
        }

        private void moveUp()
        {
            Control source = ((ContextMenuStrip)DGVskillcontext).SourceControl;
            DataGridView dg = null;
            switch (source.Name)
            {
                case "DGVskill":
                    dg = DGVskill;
                    break;
                case "DGVbuff":
                    dg = DGVbuff;
                    break;
                case "DGVpotion":
                    dg = DGVpotion;
                    break;
            }
            if (dg.RowCount > 0)
            {
                if (dg.SelectedRows.Count > 0)
                {
                    int rowCount = dg.Rows.Count;
                    int index = dg.SelectedCells[0].OwningRow.Index;

                    if (index == 0)
                    {
                        return;
                    }
                    DataGridViewRowCollection rows = dg.Rows;

                    // remove the previous row and add it behind the selected row.
                    DataGridViewRow prevRow = rows[index - 1];
                    rows.Remove(prevRow);
                    prevRow.Frozen = false;
                    rows.Insert(index, prevRow);
                    dg.ClearSelection();
                    dg.Rows[index - 1].Selected = true;
                }
            }
        }

        private void moveDown()
        {
            Control source = ((ContextMenuStrip)DGVskillcontext).SourceControl;
            DataGridView dg = null;
            switch (source.Name)
            {
                case "DGVskill":
                    dg = DGVskill;
                    break;
                case "DGVbuff":
                    dg = DGVbuff;
                    break;
                case "DGVpotion":
                    dg = DGVpotion;
                    break;
            }
            if (dg.RowCount > 0)
            {
                if (dg.SelectedRows.Count > 0)
                {
                    int rowCount = dg.Rows.Count;
                    int index = dg.SelectedCells[0].OwningRow.Index;

                    if (index == (rowCount - 1))
                    {
                        return;
                    }
                    DataGridViewRowCollection rows = dg.Rows;

                    // remove the next row and add it in front of the selected row.
                    DataGridViewRow nextRow = rows[index + 1];
                    rows.Remove(nextRow);
                    nextRow.Frozen = false;
                    rows.Insert(index, nextRow);
                    dg.ClearSelection();
                    dg.Rows[index + 1].Selected = true;
                }
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Control source = ((ContextMenuStrip)DGVskillcontext).SourceControl;
            DataGridView dg = null;
            switch (source.Name)
            {
                case "DGVskill":
                    dg = DGVskill;
                    break;
                case "DGVbuff":
                    dg = DGVbuff;
                    break;
                case "DGVpotion":
                    dg = DGVpotion;
                    break;
            }
            foreach (DataGridViewRow item in dg.SelectedRows)
            {
                dg.Rows.RemoveAt(item.Index);

            }
        }
        private void upToolStripMenuItem_Click(object sender, EventArgs e)
        {
            moveUp();
        }

        private void downToolStripMenuItem_Click(object sender, EventArgs e)
        {
            moveDown();
        }

        #region  Window Manager
        private void TrayMinimizerForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                TrayIcon.Visible = true;
                TrayIcon.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                TrayIcon.Visible = false;
            }
        }

        private void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }


        private void BTNfullscreen_Click(object sender, EventArgs e)
        {

            window.FullScreen();
        }

        private void BTNHide_CheckedChanged(object sender, EventArgs e)
        {
            if (BTNHide.Checked)
            {
                BTNHide.Text = "Show";
                window.Hide();
            }
            else
            {
                BTNHide.Text = "Hide";
                window.Show();
            }
        }

        private void BTNresize_Click(object sender, EventArgs e)
        {
            window.Resize(Convert.ToInt32(TBwindowx.Text), Convert.ToInt32(TBwindowy.Text), Convert.ToInt32(TBwindowwidth.Text), Convert.ToInt32(TBwindowheight.Text));
        }
        #endregion  Window Manager

        #region Potion

        private void DGVpotion_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DGVskillcontext.Show();
            }
        }

        private void BTNpotionadd_Click(object sender, EventArgs e)
        {
            DGVpotion.AllowUserToAddRows = true;
            DataGridViewRow row = (DataGridViewRow)DGVpotion.Rows[0].Clone();
            row.Cells[0].Value = AddPotionSkillbar.Text;
            row.Cells[1].Value = AddPotionKey.Text;
            row.Cells[2].Value = AddPotionEffect.Text;
            row.Cells[3].Value = AddPotionPercent.Value;
            row.Cells[4].Value = AddPotionCooldown.Value;
            row.Cells[5].Value = 1;
            row.Cells[6].Value = new DateTime(2014, 1, 1, 0, 0, 0);
            DGVpotion.Rows.Add(row);
            DGVpotion.AllowUserToAddRows = false;
        }
        #endregion

        private void MyBot_Load(object sender, EventArgs e)
        {
            Setting.LoadDefaultSetting();
            if (File.Exists(Setting.defaultpath))
                setting.LoadSettingIni(Setting.defaultpath);
            threadStart = new Thread(RefreshValueAndForm);
            threadStart.Start();

        }

        private void metroUiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetroMyBot form = new MetroMyBot();
            form.Show();

        }







    }
}
