﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections.Generic;
using MetroFramework.Forms;
using MB.ProcessControl;

namespace BOT
{
    public partial class Launcher : MetroForm
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(Enums.ProcessAccessFlags dwDesiredAccess, bool bInheritHandle, int dwProcessId);


        public static IntPtr baseAddress;
        public static IntPtr processHandle;
        public static IntPtr processHandlekey;
        public static Process botProcess;
        public static Process aionProcess;
        public static Process[] ProcessList;


        public Launcher()
        {
            InitializeComponent();
        }

        private void BTNrefresh_Click(object sender, EventArgs e)
        {
            BTNRefresh();
        }

        private void BTNRefresh()
        {

            Memory mem;
            ProcessList = Process.GetProcessesByName("aion.bin");
            CBclient.Items.Clear();
            if (ProcessList.Length > 0)
                for (int i = 0; i < ProcessList.Length; i++)
                {
                    mem = new Memory(ProcessList[i], "Game.dll");
                    if (!string.IsNullOrEmpty(mem.Read((int)Offsets.Player.Name).ConvertToString()))
                    {
                        CBclient.Items.Add(mem.Read((int)Offsets.Player.Name).ConvertToString() + " - " + ProcessList[i].Id);
                        CBclient.SelectedIndex = 0;
                    }
                }
            if (CBclient.Items.Count != 0)
                BTNstart.Enabled = true;
            else
                BTNstart.Enabled = false;
            mem = null;
        }

        private void BTNstart_Click(object sender, EventArgs e)
        {
            Start();
        }


        private void Start()
        {
            string AionID = CBclient.Text;
            int AionIDPosition = AionID.IndexOf(" - ");
            AionID = AionID.Remove(0, AionIDPosition + 3);
            int currentPID = Convert.ToInt32(AionID);
            botProcess = Process.GetCurrentProcess();
            processHandle = OpenProcess(Enums.ProcessAccessFlags.All, false, currentPID);
            processHandlekey = Process.GetProcessById(currentPID).MainWindowHandle;
            baseAddress = GetModuleBaseAddress("Game.dll", currentPID);
            aionProcess = Process.GetProcessById(currentPID);
            Program.bot = new MetroMyBot();
            Program.bot.Show();

                Program.launcher.Visible = false;

        }



        public IntPtr GetModuleBaseAddress(string ModuleName, int currentPID)
        {
            IntPtr BaseAddress = IntPtr.Zero;
            ProcessModule myProcessModule = null;
            ProcessModuleCollection myProcessModuleCollection;

            try
            {
                myProcessModuleCollection = Process.GetProcessById(currentPID).Modules;
            }
            catch { return IntPtr.Zero; }

            for (int i = 0; i < myProcessModuleCollection.Count; i++)
            {
                myProcessModule = myProcessModuleCollection[i];
                if (myProcessModule.ModuleName.Contains(ModuleName))
                {
                    BaseAddress = myProcessModule.BaseAddress;
                    break;
                }
            }

            return BaseAddress;
        }

        private void BTNlunchaion_Click(object sender, EventArgs e)
        {
            //if (Environment.Is64BitOperatingSystem)
            //{
                string InstallPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Gameforge\AION-LIVE", "BaseDir", Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Gameforge\AION-LIVE", "BaseDir", null));
                if (InstallPath != null)
                {
                    InstallPath += "bin32\\aion.bin";
                    InstallPath = InstallPath.Replace(" ", "\" \"");
                    RunAion(InstallPath);
                }
                else
                    MessageBox.Show("Error", " Aion Not Found");
            //}

            // "@start D:\\Gry\\Gameforge\\AION\" \"Free-To-Play\\bin32\\aion.bin -ip:64.25.35.103 -port:2106 -cc:1 -noauthgg -noweb -lang:enu -charnamemenu -lbox -f2p -loginex -nosatab -pwd16");

        }



        static void RunAion(string command)
        {
            command += " -ip:79.110.83.80 -port:2106 -noauthgg -noweb -lang:eng";
            ProcessStartInfo processInfo = new ProcessStartInfo("cmd.exe", "@/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            Process process = Process.Start(processInfo);

            process.Close();
        }

        private void Launcher_Load(object sender, EventArgs e)
        {
            BTNRefresh();
        }

        private void Launcher_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
