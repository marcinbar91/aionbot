﻿namespace BOT
{
    partial class PlayerInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LBlegionAP = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LBlegion = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LBcube = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LBpositionx = new System.Windows.Forms.Label();
            this.LBpositiony = new System.Windows.Forms.Label();
            this.LBpositionz = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LBcamerax = new System.Windows.Forms.Label();
            this.LBcameray = new System.Windows.Forms.Label();
            this.TBwill = new System.Windows.Forms.TextBox();
            this.TBknowledge = new System.Windows.Forms.TextBox();
            this.TBaccuracy = new System.Windows.Forms.TextBox();
            this.TBagility = new System.Windows.Forms.TextBox();
            this.TBhealth = new System.Windows.Forms.TextBox();
            this.TBpower = new System.Windows.Forms.TextBox();
            this.TBmp = new System.Windows.Forms.TextBox();
            this.TBhp = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TCstat = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TBspeed = new System.Windows.Forms.TextBox();
            this.TBcritoff = new System.Windows.Forms.TextBox();
            this.TBatkspeed = new System.Windows.Forms.TextBox();
            this.TBaccoff = new System.Windows.Forms.TextBox();
            this.TBattackoff = new System.Windows.Forms.TextBox();
            this.TBcritmain = new System.Windows.Forms.TextBox();
            this.TBaccmain = new System.Windows.Forms.TextBox();
            this.TBattackmain = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TBcritstrikefort = new System.Windows.Forms.TextBox();
            this.TBcritstrikeres = new System.Windows.Forms.TextBox();
            this.TBevansion = new System.Windows.Forms.TextBox();
            this.TBparry = new System.Windows.Forms.TextBox();
            this.TBblock = new System.Windows.Forms.TextBox();
            this.TBphydef = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TBcastspeed = new System.Windows.Forms.TextBox();
            this.TBhealingboost = new System.Windows.Forms.TextBox();
            this.TBcritspell = new System.Windows.Forms.TextBox();
            this.TBmagicacc = new System.Windows.Forms.TextBox();
            this.TBmagicboost = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.TBearthres = new System.Windows.Forms.TextBox();
            this.TBwaterres = new System.Windows.Forms.TextBox();
            this.TBwindres = new System.Windows.Forms.TextBox();
            this.TBfireres = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.TBspellfort = new System.Windows.Forms.TextBox();
            this.TBcritspellres = new System.Windows.Forms.TextBox();
            this.TBmagicres = new System.Windows.Forms.TextBox();
            this.TBmagicsupp = new System.Windows.Forms.TextBox();
            this.TBmagicdef = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.TBtotalap = new System.Windows.Forms.TextBox();
            this.TBtodayap = new System.Windows.Forms.TextBox();
            this.TBlastweekap = new System.Windows.Forms.TextBox();
            this.TBthisweekap = new System.Windows.Forms.TextBox();
            this.TBtotalely = new System.Windows.Forms.TextBox();
            this.TBtodayely = new System.Windows.Forms.TextBox();
            this.TBlastweekely = new System.Windows.Forms.TextBox();
            this.TBthisweekely = new System.Windows.Forms.TextBox();
            this.TBtotalhp = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.TBtodayhp = new System.Windows.Forms.TextBox();
            this.TBlastweekhp = new System.Windows.Forms.TextBox();
            this.TBthisweekhp = new System.Windows.Forms.TextBox();
            this.TBrank = new System.Windows.Forms.TextBox();
            this.TBcurrentrank = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TCstat.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel3.Controls.Add(this.LBlegionAP);
            this.panel3.Location = new System.Drawing.Point(153, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(131, 38);
            this.panel3.TabIndex = 47;
            // 
            // LBlegionAP
            // 
            this.LBlegionAP.AutoSize = true;
            this.LBlegionAP.Location = new System.Drawing.Point(7, 15);
            this.LBlegionAP.Name = "LBlegionAP";
            this.LBlegionAP.Size = new System.Drawing.Size(24, 13);
            this.LBlegionAP.TabIndex = 18;
            this.LBlegionAP.Text = "AP:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.LBlegion);
            this.panel2.Location = new System.Drawing.Point(9, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(134, 38);
            this.panel2.TabIndex = 46;
            // 
            // LBlegion
            // 
            this.LBlegion.AutoSize = true;
            this.LBlegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LBlegion.Location = new System.Drawing.Point(6, 15);
            this.LBlegion.Name = "LBlegion";
            this.LBlegion.Size = new System.Drawing.Size(42, 13);
            this.LBlegion.TabIndex = 16;
            this.LBlegion.Text = "Legion:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.LBcube);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.TBwill);
            this.panel1.Controls.Add(this.TBknowledge);
            this.panel1.Controls.Add(this.TBaccuracy);
            this.panel1.Controls.Add(this.TBagility);
            this.panel1.Controls.Add(this.TBhealth);
            this.panel1.Controls.Add(this.TBpower);
            this.panel1.Controls.Add(this.TBmp);
            this.panel1.Controls.Add(this.TBhp);
            this.panel1.Controls.Add(this.label48);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.label44);
            this.panel1.Controls.Add(this.label43);
            this.panel1.Controls.Add(this.label42);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(9, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(275, 183);
            this.panel1.TabIndex = 44;
            // 
            // LBcube
            // 
            this.LBcube.AutoSize = true;
            this.LBcube.Location = new System.Drawing.Point(138, 143);
            this.LBcube.Name = "LBcube";
            this.LBcube.Size = new System.Drawing.Size(35, 13);
            this.LBcube.TabIndex = 35;
            this.LBcube.Text = "Cube:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LBpositionx);
            this.groupBox2.Controls.Add(this.LBpositiony);
            this.groupBox2.Controls.Add(this.LBpositionz);
            this.groupBox2.Location = new System.Drawing.Point(141, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(104, 66);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Position";
            // 
            // LBpositionx
            // 
            this.LBpositionx.AutoSize = true;
            this.LBpositionx.Location = new System.Drawing.Point(7, 16);
            this.LBpositionx.Name = "LBpositionx";
            this.LBpositionx.Size = new System.Drawing.Size(17, 13);
            this.LBpositionx.TabIndex = 22;
            this.LBpositionx.Text = "X:";
            // 
            // LBpositiony
            // 
            this.LBpositiony.AutoSize = true;
            this.LBpositiony.Location = new System.Drawing.Point(7, 32);
            this.LBpositiony.Name = "LBpositiony";
            this.LBpositiony.Size = new System.Drawing.Size(17, 13);
            this.LBpositiony.TabIndex = 23;
            this.LBpositiony.Text = "Y:";
            // 
            // LBpositionz
            // 
            this.LBpositionz.AutoSize = true;
            this.LBpositionz.Location = new System.Drawing.Point(7, 47);
            this.LBpositionz.Name = "LBpositionz";
            this.LBpositionz.Size = new System.Drawing.Size(17, 13);
            this.LBpositionz.TabIndex = 24;
            this.LBpositionz.Text = "Z:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LBcamerax);
            this.groupBox1.Controls.Add(this.LBcameray);
            this.groupBox1.Location = new System.Drawing.Point(141, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(104, 51);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Camera";
            // 
            // LBcamerax
            // 
            this.LBcamerax.AutoSize = true;
            this.LBcamerax.Location = new System.Drawing.Point(7, 16);
            this.LBcamerax.Name = "LBcamerax";
            this.LBcamerax.Size = new System.Drawing.Size(17, 13);
            this.LBcamerax.TabIndex = 25;
            this.LBcamerax.Text = "X:";
            // 
            // LBcameray
            // 
            this.LBcameray.AutoSize = true;
            this.LBcameray.Location = new System.Drawing.Point(7, 31);
            this.LBcameray.Name = "LBcameray";
            this.LBcameray.Size = new System.Drawing.Size(17, 13);
            this.LBcameray.TabIndex = 26;
            this.LBcameray.Text = "Y:";
            // 
            // TBwill
            // 
            this.TBwill.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBwill.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBwill.Location = new System.Drawing.Point(95, 147);
            this.TBwill.MaxLength = 6;
            this.TBwill.Name = "TBwill";
            this.TBwill.ReadOnly = true;
            this.TBwill.Size = new System.Drawing.Size(37, 13);
            this.TBwill.TabIndex = 42;
            this.TBwill.Text = "N/A";
            this.TBwill.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBknowledge
            // 
            this.TBknowledge.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBknowledge.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBknowledge.Location = new System.Drawing.Point(95, 128);
            this.TBknowledge.MaxLength = 6;
            this.TBknowledge.Name = "TBknowledge";
            this.TBknowledge.ReadOnly = true;
            this.TBknowledge.Size = new System.Drawing.Size(37, 13);
            this.TBknowledge.TabIndex = 41;
            this.TBknowledge.Text = "N/A";
            this.TBknowledge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBaccuracy
            // 
            this.TBaccuracy.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBaccuracy.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBaccuracy.Location = new System.Drawing.Point(95, 109);
            this.TBaccuracy.MaxLength = 6;
            this.TBaccuracy.Name = "TBaccuracy";
            this.TBaccuracy.ReadOnly = true;
            this.TBaccuracy.Size = new System.Drawing.Size(37, 13);
            this.TBaccuracy.TabIndex = 40;
            this.TBaccuracy.Text = "N/A";
            this.TBaccuracy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBagility
            // 
            this.TBagility.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBagility.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBagility.Location = new System.Drawing.Point(95, 90);
            this.TBagility.MaxLength = 6;
            this.TBagility.Name = "TBagility";
            this.TBagility.ReadOnly = true;
            this.TBagility.Size = new System.Drawing.Size(37, 13);
            this.TBagility.TabIndex = 39;
            this.TBagility.Text = "N/A";
            this.TBagility.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBhealth
            // 
            this.TBhealth.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBhealth.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBhealth.Location = new System.Drawing.Point(95, 71);
            this.TBhealth.MaxLength = 6;
            this.TBhealth.Name = "TBhealth";
            this.TBhealth.ReadOnly = true;
            this.TBhealth.Size = new System.Drawing.Size(37, 13);
            this.TBhealth.TabIndex = 38;
            this.TBhealth.Text = "N/A";
            this.TBhealth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBpower
            // 
            this.TBpower.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBpower.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBpower.Location = new System.Drawing.Point(95, 52);
            this.TBpower.MaxLength = 6;
            this.TBpower.Name = "TBpower";
            this.TBpower.ReadOnly = true;
            this.TBpower.Size = new System.Drawing.Size(37, 13);
            this.TBpower.TabIndex = 37;
            this.TBpower.Text = "N/A";
            this.TBpower.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBmp
            // 
            this.TBmp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBmp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBmp.Location = new System.Drawing.Point(95, 33);
            this.TBmp.MaxLength = 6;
            this.TBmp.Name = "TBmp";
            this.TBmp.ReadOnly = true;
            this.TBmp.Size = new System.Drawing.Size(37, 13);
            this.TBmp.TabIndex = 36;
            this.TBmp.Text = "N/A";
            this.TBmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBhp
            // 
            this.TBhp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBhp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBhp.Location = new System.Drawing.Point(95, 14);
            this.TBhp.MaxLength = 6;
            this.TBhp.Name = "TBhp";
            this.TBhp.ReadOnly = true;
            this.TBhp.Size = new System.Drawing.Size(37, 13);
            this.TBhp.TabIndex = 35;
            this.TBhp.Text = "N/A";
            this.TBhp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.SystemColors.Info;
            this.label48.Location = new System.Drawing.Point(10, 127);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(60, 13);
            this.label48.TabIndex = 34;
            this.label48.Text = "Knowledge";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.SystemColors.Info;
            this.label47.Location = new System.Drawing.Point(10, 146);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(24, 13);
            this.label47.TabIndex = 33;
            this.label47.Text = "Will";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.SystemColors.Info;
            this.label46.Location = new System.Drawing.Point(10, 108);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(52, 13);
            this.label46.TabIndex = 32;
            this.label46.Text = "Accuracy";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.SystemColors.Info;
            this.label45.Location = new System.Drawing.Point(10, 89);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(34, 13);
            this.label45.TabIndex = 31;
            this.label45.Text = "Agility";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.SystemColors.Info;
            this.label44.Location = new System.Drawing.Point(10, 70);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(38, 13);
            this.label44.TabIndex = 30;
            this.label44.Text = "Health";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.SystemColors.Info;
            this.label43.Location = new System.Drawing.Point(9, 51);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(37, 13);
            this.label43.TabIndex = 29;
            this.label43.Text = "Power";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.SystemColors.Info;
            this.label42.Location = new System.Drawing.Point(9, 32);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(23, 13);
            this.label42.TabIndex = 28;
            this.label42.Text = "MP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "HP";
            // 
            // TCstat
            // 
            this.TCstat.Controls.Add(this.tabPage1);
            this.TCstat.Controls.Add(this.tabPage2);
            this.TCstat.Controls.Add(this.tabPage3);
            this.TCstat.Controls.Add(this.tabPage4);
            this.TCstat.Controls.Add(this.tabPage5);
            this.TCstat.Location = new System.Drawing.Point(9, 296);
            this.TCstat.Name = "TCstat";
            this.TCstat.SelectedIndex = 0;
            this.TCstat.Size = new System.Drawing.Size(275, 195);
            this.TCstat.TabIndex = 45;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tabPage1.Controls.Add(this.TBspeed);
            this.tabPage1.Controls.Add(this.TBcritoff);
            this.tabPage1.Controls.Add(this.TBatkspeed);
            this.tabPage1.Controls.Add(this.TBaccoff);
            this.tabPage1.Controls.Add(this.TBattackoff);
            this.tabPage1.Controls.Add(this.TBcritmain);
            this.tabPage1.Controls.Add(this.TBaccmain);
            this.tabPage1.Controls.Add(this.TBattackmain);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(267, 169);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "1";
            // 
            // TBspeed
            // 
            this.TBspeed.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBspeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBspeed.Location = new System.Drawing.Point(188, 122);
            this.TBspeed.MaxLength = 6;
            this.TBspeed.Name = "TBspeed";
            this.TBspeed.ReadOnly = true;
            this.TBspeed.Size = new System.Drawing.Size(37, 13);
            this.TBspeed.TabIndex = 26;
            this.TBspeed.Text = "N/A";
            this.TBspeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBcritoff
            // 
            this.TBcritoff.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcritoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcritoff.Location = new System.Drawing.Point(190, 78);
            this.TBcritoff.MaxLength = 6;
            this.TBcritoff.Name = "TBcritoff";
            this.TBcritoff.ReadOnly = true;
            this.TBcritoff.Size = new System.Drawing.Size(37, 13);
            this.TBcritoff.TabIndex = 25;
            this.TBcritoff.Text = "N/A";
            this.TBcritoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBatkspeed
            // 
            this.TBatkspeed.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBatkspeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBatkspeed.Location = new System.Drawing.Point(73, 119);
            this.TBatkspeed.MaxLength = 6;
            this.TBatkspeed.Name = "TBatkspeed";
            this.TBatkspeed.ReadOnly = true;
            this.TBatkspeed.Size = new System.Drawing.Size(37, 13);
            this.TBatkspeed.TabIndex = 24;
            this.TBatkspeed.Text = "N/A";
            this.TBatkspeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBaccoff
            // 
            this.TBaccoff.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBaccoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBaccoff.Location = new System.Drawing.Point(190, 52);
            this.TBaccoff.MaxLength = 6;
            this.TBaccoff.Name = "TBaccoff";
            this.TBaccoff.ReadOnly = true;
            this.TBaccoff.Size = new System.Drawing.Size(37, 13);
            this.TBaccoff.TabIndex = 23;
            this.TBaccoff.Text = "N/A";
            this.TBaccoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBattackoff
            // 
            this.TBattackoff.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBattackoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBattackoff.Location = new System.Drawing.Point(190, 26);
            this.TBattackoff.MaxLength = 6;
            this.TBattackoff.Name = "TBattackoff";
            this.TBattackoff.ReadOnly = true;
            this.TBattackoff.Size = new System.Drawing.Size(37, 13);
            this.TBattackoff.TabIndex = 22;
            this.TBattackoff.Text = "N/A";
            this.TBattackoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBcritmain
            // 
            this.TBcritmain.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcritmain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcritmain.Location = new System.Drawing.Point(71, 78);
            this.TBcritmain.MaxLength = 6;
            this.TBcritmain.Name = "TBcritmain";
            this.TBcritmain.ReadOnly = true;
            this.TBcritmain.Size = new System.Drawing.Size(37, 13);
            this.TBcritmain.TabIndex = 20;
            this.TBcritmain.Text = "N/A";
            this.TBcritmain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBaccmain
            // 
            this.TBaccmain.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBaccmain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBaccmain.Location = new System.Drawing.Point(71, 52);
            this.TBaccmain.MaxLength = 6;
            this.TBaccmain.Name = "TBaccmain";
            this.TBaccmain.ReadOnly = true;
            this.TBaccmain.Size = new System.Drawing.Size(37, 13);
            this.TBaccmain.TabIndex = 19;
            this.TBaccmain.Text = "N/A";
            this.TBaccmain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBattackmain
            // 
            this.TBattackmain.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBattackmain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBattackmain.Location = new System.Drawing.Point(71, 26);
            this.TBattackmain.MaxLength = 6;
            this.TBattackmain.Name = "TBattackmain";
            this.TBattackmain.ReadOnly = true;
            this.TBattackmain.Size = new System.Drawing.Size(37, 13);
            this.TBattackmain.TabIndex = 18;
            this.TBattackmain.Text = "N/A";
            this.TBattackmain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.Info;
            this.label9.Location = new System.Drawing.Point(129, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Crit. Strike";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.Info;
            this.label10.Location = new System.Drawing.Point(129, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Attack";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.Info;
            this.label11.Location = new System.Drawing.Point(129, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Accuracy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Info;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Main Hand";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Info;
            this.label8.Location = new System.Drawing.Point(129, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Speed";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(6, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Crit. Strike";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(129, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Off Hand";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Attack";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.Info;
            this.label6.Location = new System.Drawing.Point(6, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Atk. Speed";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.Info;
            this.label7.Location = new System.Drawing.Point(6, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Accuracy";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.TBcritstrikefort);
            this.tabPage2.Controls.Add(this.TBcritstrikeres);
            this.tabPage2.Controls.Add(this.TBevansion);
            this.tabPage2.Controls.Add(this.TBparry);
            this.tabPage2.Controls.Add(this.TBblock);
            this.tabPage2.Controls.Add(this.TBphydef);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(267, 169);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.Info;
            this.label16.Location = new System.Drawing.Point(6, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Crit. Strike Fort.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.Info;
            this.label15.Location = new System.Drawing.Point(6, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Crit. Strik Res.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.Info;
            this.label14.Location = new System.Drawing.Point(6, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "Evasion";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.Info;
            this.label13.Location = new System.Drawing.Point(6, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Parry";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.Info;
            this.label12.Location = new System.Drawing.Point(6, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Block";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.Info;
            this.label17.Location = new System.Drawing.Point(6, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "Physical Def.";
            // 
            // TBcritstrikefort
            // 
            this.TBcritstrikefort.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcritstrikefort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcritstrikefort.Location = new System.Drawing.Point(91, 130);
            this.TBcritstrikefort.MaxLength = 6;
            this.TBcritstrikefort.Name = "TBcritstrikefort";
            this.TBcritstrikefort.ReadOnly = true;
            this.TBcritstrikefort.Size = new System.Drawing.Size(37, 13);
            this.TBcritstrikefort.TabIndex = 30;
            this.TBcritstrikefort.Text = "N/A";
            this.TBcritstrikefort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBcritstrikeres
            // 
            this.TBcritstrikeres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcritstrikeres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcritstrikeres.Location = new System.Drawing.Point(91, 104);
            this.TBcritstrikeres.MaxLength = 6;
            this.TBcritstrikeres.Name = "TBcritstrikeres";
            this.TBcritstrikeres.ReadOnly = true;
            this.TBcritstrikeres.Size = new System.Drawing.Size(37, 13);
            this.TBcritstrikeres.TabIndex = 29;
            this.TBcritstrikeres.Text = "N/A";
            this.TBcritstrikeres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBevansion
            // 
            this.TBevansion.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBevansion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBevansion.Location = new System.Drawing.Point(91, 78);
            this.TBevansion.MaxLength = 6;
            this.TBevansion.Name = "TBevansion";
            this.TBevansion.ReadOnly = true;
            this.TBevansion.Size = new System.Drawing.Size(37, 13);
            this.TBevansion.TabIndex = 28;
            this.TBevansion.Text = "N/A";
            this.TBevansion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBparry
            // 
            this.TBparry.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBparry.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBparry.Location = new System.Drawing.Point(91, 52);
            this.TBparry.MaxLength = 6;
            this.TBparry.Name = "TBparry";
            this.TBparry.ReadOnly = true;
            this.TBparry.Size = new System.Drawing.Size(37, 13);
            this.TBparry.TabIndex = 27;
            this.TBparry.Text = "N/A";
            this.TBparry.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBblock
            // 
            this.TBblock.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBblock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBblock.Location = new System.Drawing.Point(91, 26);
            this.TBblock.MaxLength = 6;
            this.TBblock.Name = "TBblock";
            this.TBblock.ReadOnly = true;
            this.TBblock.Size = new System.Drawing.Size(37, 13);
            this.TBblock.TabIndex = 26;
            this.TBblock.Text = "N/A";
            this.TBblock.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBphydef
            // 
            this.TBphydef.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBphydef.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBphydef.Location = new System.Drawing.Point(91, 0);
            this.TBphydef.MaxLength = 6;
            this.TBphydef.Name = "TBphydef";
            this.TBphydef.ReadOnly = true;
            this.TBphydef.Size = new System.Drawing.Size(37, 13);
            this.TBphydef.TabIndex = 25;
            this.TBphydef.Text = "N/A";
            this.TBphydef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.TBcastspeed);
            this.tabPage3.Controls.Add(this.TBhealingboost);
            this.tabPage3.Controls.Add(this.TBcritspell);
            this.tabPage3.Controls.Add(this.TBmagicacc);
            this.tabPage3.Controls.Add(this.TBmagicboost);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(267, 169);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "3";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.Info;
            this.label18.Location = new System.Drawing.Point(6, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 47;
            this.label18.Text = "Cast. Speed";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.Info;
            this.label19.Location = new System.Drawing.Point(6, 78);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 46;
            this.label19.Text = "Healing Boost";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.Info;
            this.label20.Location = new System.Drawing.Point(6, 52);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Crit. Spell";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.Info;
            this.label21.Location = new System.Drawing.Point(6, 26);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 44;
            this.label21.Text = "Magic Acc.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.Info;
            this.label22.Location = new System.Drawing.Point(6, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 13);
            this.label22.TabIndex = 43;
            this.label22.Text = "Magic Boost";
            // 
            // TBcastspeed
            // 
            this.TBcastspeed.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcastspeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcastspeed.Location = new System.Drawing.Point(84, 104);
            this.TBcastspeed.MaxLength = 6;
            this.TBcastspeed.Name = "TBcastspeed";
            this.TBcastspeed.ReadOnly = true;
            this.TBcastspeed.Size = new System.Drawing.Size(37, 13);
            this.TBcastspeed.TabIndex = 41;
            this.TBcastspeed.Text = "N/A";
            this.TBcastspeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBhealingboost
            // 
            this.TBhealingboost.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBhealingboost.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBhealingboost.Location = new System.Drawing.Point(84, 78);
            this.TBhealingboost.MaxLength = 6;
            this.TBhealingboost.Name = "TBhealingboost";
            this.TBhealingboost.ReadOnly = true;
            this.TBhealingboost.Size = new System.Drawing.Size(37, 13);
            this.TBhealingboost.TabIndex = 40;
            this.TBhealingboost.Text = "N/A";
            this.TBhealingboost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBcritspell
            // 
            this.TBcritspell.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcritspell.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcritspell.Location = new System.Drawing.Point(84, 52);
            this.TBcritspell.MaxLength = 6;
            this.TBcritspell.Name = "TBcritspell";
            this.TBcritspell.ReadOnly = true;
            this.TBcritspell.Size = new System.Drawing.Size(37, 13);
            this.TBcritspell.TabIndex = 39;
            this.TBcritspell.Text = "N/A";
            this.TBcritspell.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBmagicacc
            // 
            this.TBmagicacc.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBmagicacc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBmagicacc.Location = new System.Drawing.Point(84, 26);
            this.TBmagicacc.MaxLength = 6;
            this.TBmagicacc.Name = "TBmagicacc";
            this.TBmagicacc.ReadOnly = true;
            this.TBmagicacc.Size = new System.Drawing.Size(37, 13);
            this.TBmagicacc.TabIndex = 38;
            this.TBmagicacc.Text = "N/A";
            this.TBmagicacc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBmagicboost
            // 
            this.TBmagicboost.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBmagicboost.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBmagicboost.Location = new System.Drawing.Point(84, 0);
            this.TBmagicboost.MaxLength = 6;
            this.TBmagicboost.Name = "TBmagicboost";
            this.TBmagicboost.ReadOnly = true;
            this.TBmagicboost.Size = new System.Drawing.Size(37, 13);
            this.TBmagicboost.TabIndex = 37;
            this.TBmagicboost.Text = "N/A";
            this.TBmagicboost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label30);
            this.tabPage4.Controls.Add(this.TBearthres);
            this.tabPage4.Controls.Add(this.TBwaterres);
            this.tabPage4.Controls.Add(this.TBwindres);
            this.tabPage4.Controls.Add(this.TBfireres);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label41);
            this.tabPage4.Controls.Add(this.TBspellfort);
            this.tabPage4.Controls.Add(this.TBcritspellres);
            this.tabPage4.Controls.Add(this.TBmagicres);
            this.tabPage4.Controls.Add(this.TBmagicsupp);
            this.tabPage4.Controls.Add(this.TBmagicdef);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(267, 169);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "4";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.Info;
            this.label27.Location = new System.Drawing.Point(152, 93);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 13);
            this.label27.TabIndex = 65;
            this.label27.Text = "Earth Resist";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.Info;
            this.label28.Location = new System.Drawing.Point(152, 65);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 13);
            this.label28.TabIndex = 64;
            this.label28.Text = "Watter Resist";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.Info;
            this.label29.Location = new System.Drawing.Point(152, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 63;
            this.label29.Text = "Wind Resist";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.Info;
            this.label30.Location = new System.Drawing.Point(152, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 62;
            this.label30.Text = "Fire Resist";
            // 
            // TBearthres
            // 
            this.TBearthres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBearthres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBearthres.Location = new System.Drawing.Point(224, 90);
            this.TBearthres.MaxLength = 6;
            this.TBearthres.Name = "TBearthres";
            this.TBearthres.ReadOnly = true;
            this.TBearthres.Size = new System.Drawing.Size(37, 13);
            this.TBearthres.TabIndex = 61;
            this.TBearthres.Text = "N/A";
            this.TBearthres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBwaterres
            // 
            this.TBwaterres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBwaterres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBwaterres.Location = new System.Drawing.Point(224, 64);
            this.TBwaterres.MaxLength = 6;
            this.TBwaterres.Name = "TBwaterres";
            this.TBwaterres.ReadOnly = true;
            this.TBwaterres.Size = new System.Drawing.Size(37, 13);
            this.TBwaterres.TabIndex = 60;
            this.TBwaterres.Text = "N/A";
            this.TBwaterres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBwindres
            // 
            this.TBwindres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBwindres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBwindres.Location = new System.Drawing.Point(224, 38);
            this.TBwindres.MaxLength = 6;
            this.TBwindres.Name = "TBwindres";
            this.TBwindres.ReadOnly = true;
            this.TBwindres.Size = new System.Drawing.Size(37, 13);
            this.TBwindres.TabIndex = 59;
            this.TBwindres.Text = "N/A";
            this.TBwindres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBfireres
            // 
            this.TBfireres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBfireres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBfireres.Location = new System.Drawing.Point(224, 12);
            this.TBfireres.MaxLength = 6;
            this.TBfireres.Name = "TBfireres";
            this.TBfireres.ReadOnly = true;
            this.TBfireres.Size = new System.Drawing.Size(37, 13);
            this.TBfireres.TabIndex = 58;
            this.TBfireres.Text = "N/A";
            this.TBfireres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.Info;
            this.label23.Location = new System.Drawing.Point(3, 116);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 57;
            this.label23.Text = "Spell Fortitude";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.Info;
            this.label24.Location = new System.Drawing.Point(3, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 13);
            this.label24.TabIndex = 56;
            this.label24.Text = "Crit. Spell Resist";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.Info;
            this.label25.Location = new System.Drawing.Point(3, 62);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 13);
            this.label25.TabIndex = 55;
            this.label25.Text = "Magic Resist";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.Info;
            this.label26.Location = new System.Drawing.Point(3, 38);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(97, 13);
            this.label26.TabIndex = 54;
            this.label26.Text = "Magic Suppression";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.SystemColors.Info;
            this.label41.Location = new System.Drawing.Point(3, 12);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(62, 13);
            this.label41.TabIndex = 53;
            this.label41.Text = "Magic. Def.";
            // 
            // TBspellfort
            // 
            this.TBspellfort.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBspellfort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBspellfort.Location = new System.Drawing.Point(109, 116);
            this.TBspellfort.MaxLength = 6;
            this.TBspellfort.Name = "TBspellfort";
            this.TBspellfort.ReadOnly = true;
            this.TBspellfort.Size = new System.Drawing.Size(37, 13);
            this.TBspellfort.TabIndex = 52;
            this.TBspellfort.Text = "N/A";
            this.TBspellfort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBcritspellres
            // 
            this.TBcritspellres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcritspellres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcritspellres.Location = new System.Drawing.Point(109, 90);
            this.TBcritspellres.MaxLength = 6;
            this.TBcritspellres.Name = "TBcritspellres";
            this.TBcritspellres.ReadOnly = true;
            this.TBcritspellres.Size = new System.Drawing.Size(37, 13);
            this.TBcritspellres.TabIndex = 51;
            this.TBcritspellres.Text = "N/A";
            this.TBcritspellres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBmagicres
            // 
            this.TBmagicres.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBmagicres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBmagicres.Location = new System.Drawing.Point(109, 64);
            this.TBmagicres.MaxLength = 6;
            this.TBmagicres.Name = "TBmagicres";
            this.TBmagicres.ReadOnly = true;
            this.TBmagicres.Size = new System.Drawing.Size(37, 13);
            this.TBmagicres.TabIndex = 50;
            this.TBmagicres.Text = "N/A";
            this.TBmagicres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBmagicsupp
            // 
            this.TBmagicsupp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBmagicsupp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBmagicsupp.Location = new System.Drawing.Point(109, 38);
            this.TBmagicsupp.MaxLength = 6;
            this.TBmagicsupp.Name = "TBmagicsupp";
            this.TBmagicsupp.ReadOnly = true;
            this.TBmagicsupp.Size = new System.Drawing.Size(37, 13);
            this.TBmagicsupp.TabIndex = 49;
            this.TBmagicsupp.Text = "N/A";
            this.TBmagicsupp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBmagicdef
            // 
            this.TBmagicdef.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBmagicdef.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBmagicdef.Location = new System.Drawing.Point(109, 12);
            this.TBmagicdef.MaxLength = 6;
            this.TBmagicdef.Name = "TBmagicdef";
            this.TBmagicdef.ReadOnly = true;
            this.TBmagicdef.Size = new System.Drawing.Size(37, 13);
            this.TBmagicdef.TabIndex = 48;
            this.TBmagicdef.Text = "N/A";
            this.TBmagicdef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage5.Controls.Add(this.TBtotalap);
            this.tabPage5.Controls.Add(this.TBtodayap);
            this.tabPage5.Controls.Add(this.TBlastweekap);
            this.tabPage5.Controls.Add(this.TBthisweekap);
            this.tabPage5.Controls.Add(this.TBtotalely);
            this.tabPage5.Controls.Add(this.TBtodayely);
            this.tabPage5.Controls.Add(this.TBlastweekely);
            this.tabPage5.Controls.Add(this.TBthisweekely);
            this.tabPage5.Controls.Add(this.TBtotalhp);
            this.tabPage5.Controls.Add(this.label36);
            this.tabPage5.Controls.Add(this.label31);
            this.tabPage5.Controls.Add(this.label32);
            this.tabPage5.Controls.Add(this.label33);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.label35);
            this.tabPage5.Controls.Add(this.TBtodayhp);
            this.tabPage5.Controls.Add(this.TBlastweekhp);
            this.tabPage5.Controls.Add(this.TBthisweekhp);
            this.tabPage5.Controls.Add(this.TBrank);
            this.tabPage5.Controls.Add(this.TBcurrentrank);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(267, 169);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Abyss/Honor Point";
            // 
            // TBtotalap
            // 
            this.TBtotalap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBtotalap.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBtotalap.Location = new System.Drawing.Point(204, 144);
            this.TBtotalap.MaxLength = 6;
            this.TBtotalap.Name = "TBtotalap";
            this.TBtotalap.ReadOnly = true;
            this.TBtotalap.Size = new System.Drawing.Size(37, 13);
            this.TBtotalap.TabIndex = 77;
            this.TBtotalap.Text = "N/A";
            this.TBtotalap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBtodayap
            // 
            this.TBtodayap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBtodayap.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBtodayap.Location = new System.Drawing.Point(204, 51);
            this.TBtodayap.MaxLength = 6;
            this.TBtodayap.Name = "TBtodayap";
            this.TBtodayap.ReadOnly = true;
            this.TBtodayap.Size = new System.Drawing.Size(37, 13);
            this.TBtodayap.TabIndex = 76;
            this.TBtodayap.Text = "N/A";
            this.TBtodayap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBlastweekap
            // 
            this.TBlastweekap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBlastweekap.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBlastweekap.Location = new System.Drawing.Point(204, 104);
            this.TBlastweekap.MaxLength = 6;
            this.TBlastweekap.Name = "TBlastweekap";
            this.TBlastweekap.ReadOnly = true;
            this.TBlastweekap.Size = new System.Drawing.Size(37, 13);
            this.TBlastweekap.TabIndex = 75;
            this.TBlastweekap.Text = "N/A";
            this.TBlastweekap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBthisweekap
            // 
            this.TBthisweekap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBthisweekap.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBthisweekap.Location = new System.Drawing.Point(204, 77);
            this.TBthisweekap.MaxLength = 6;
            this.TBthisweekap.Name = "TBthisweekap";
            this.TBthisweekap.ReadOnly = true;
            this.TBthisweekap.Size = new System.Drawing.Size(37, 13);
            this.TBthisweekap.TabIndex = 74;
            this.TBthisweekap.Text = "N/A";
            this.TBthisweekap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBtotalely
            // 
            this.TBtotalely.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBtotalely.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBtotalely.Location = new System.Drawing.Point(137, 144);
            this.TBtotalely.MaxLength = 6;
            this.TBtotalely.Name = "TBtotalely";
            this.TBtotalely.ReadOnly = true;
            this.TBtotalely.Size = new System.Drawing.Size(37, 13);
            this.TBtotalely.TabIndex = 73;
            this.TBtotalely.Text = "N/A";
            this.TBtotalely.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBtodayely
            // 
            this.TBtodayely.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBtodayely.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBtodayely.Location = new System.Drawing.Point(137, 51);
            this.TBtodayely.MaxLength = 6;
            this.TBtodayely.Name = "TBtodayely";
            this.TBtodayely.ReadOnly = true;
            this.TBtodayely.Size = new System.Drawing.Size(37, 13);
            this.TBtodayely.TabIndex = 72;
            this.TBtodayely.Text = "N/A";
            this.TBtodayely.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBlastweekely
            // 
            this.TBlastweekely.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBlastweekely.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBlastweekely.Location = new System.Drawing.Point(137, 104);
            this.TBlastweekely.MaxLength = 6;
            this.TBlastweekely.Name = "TBlastweekely";
            this.TBlastweekely.ReadOnly = true;
            this.TBlastweekely.Size = new System.Drawing.Size(37, 13);
            this.TBlastweekely.TabIndex = 71;
            this.TBlastweekely.Text = "N/A";
            this.TBlastweekely.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBthisweekely
            // 
            this.TBthisweekely.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBthisweekely.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBthisweekely.Location = new System.Drawing.Point(137, 77);
            this.TBthisweekely.MaxLength = 6;
            this.TBthisweekely.Name = "TBthisweekely";
            this.TBthisweekely.ReadOnly = true;
            this.TBthisweekely.Size = new System.Drawing.Size(37, 13);
            this.TBthisweekely.TabIndex = 70;
            this.TBthisweekely.Text = "N/A";
            this.TBthisweekely.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBtotalhp
            // 
            this.TBtotalhp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBtotalhp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBtotalhp.Location = new System.Drawing.Point(76, 144);
            this.TBtotalhp.MaxLength = 6;
            this.TBtotalhp.Name = "TBtotalhp";
            this.TBtotalhp.ReadOnly = true;
            this.TBtotalhp.Size = new System.Drawing.Size(37, 13);
            this.TBtotalhp.TabIndex = 69;
            this.TBtotalhp.Text = "N/A";
            this.TBtotalhp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.SystemColors.Info;
            this.label36.Location = new System.Drawing.Point(8, 144);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(31, 13);
            this.label36.TabIndex = 68;
            this.label36.Text = "Total";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.Info;
            this.label31.Location = new System.Drawing.Point(8, 104);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(62, 13);
            this.label31.TabIndex = 67;
            this.label31.Text = "Last Week:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.SystemColors.Info;
            this.label32.Location = new System.Drawing.Point(6, 77);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(62, 13);
            this.label32.TabIndex = 66;
            this.label32.Text = "This Week:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.SystemColors.Info;
            this.label33.Location = new System.Drawing.Point(6, 51);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 13);
            this.label33.TabIndex = 65;
            this.label33.Text = "Today:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.Info;
            this.label34.Location = new System.Drawing.Point(162, 15);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(36, 13);
            this.label34.TabIndex = 64;
            this.label34.Text = "Rank:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.Info;
            this.label35.Location = new System.Drawing.Point(8, 15);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 13);
            this.label35.TabIndex = 63;
            this.label35.Text = "Current Rank:";
            // 
            // TBtodayhp
            // 
            this.TBtodayhp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBtodayhp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBtodayhp.Location = new System.Drawing.Point(76, 51);
            this.TBtodayhp.MaxLength = 6;
            this.TBtodayhp.Name = "TBtodayhp";
            this.TBtodayhp.ReadOnly = true;
            this.TBtodayhp.Size = new System.Drawing.Size(37, 13);
            this.TBtodayhp.TabIndex = 62;
            this.TBtodayhp.Text = "N/A";
            this.TBtodayhp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBlastweekhp
            // 
            this.TBlastweekhp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBlastweekhp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBlastweekhp.Location = new System.Drawing.Point(76, 104);
            this.TBlastweekhp.MaxLength = 6;
            this.TBlastweekhp.Name = "TBlastweekhp";
            this.TBlastweekhp.ReadOnly = true;
            this.TBlastweekhp.Size = new System.Drawing.Size(37, 13);
            this.TBlastweekhp.TabIndex = 61;
            this.TBlastweekhp.Text = "N/A";
            this.TBlastweekhp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBthisweekhp
            // 
            this.TBthisweekhp.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBthisweekhp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBthisweekhp.Location = new System.Drawing.Point(76, 77);
            this.TBthisweekhp.MaxLength = 6;
            this.TBthisweekhp.Name = "TBthisweekhp";
            this.TBthisweekhp.ReadOnly = true;
            this.TBthisweekhp.Size = new System.Drawing.Size(37, 13);
            this.TBthisweekhp.TabIndex = 60;
            this.TBthisweekhp.Text = "N/A";
            this.TBthisweekhp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBrank
            // 
            this.TBrank.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBrank.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBrank.Location = new System.Drawing.Point(204, 15);
            this.TBrank.MaxLength = 6;
            this.TBrank.Name = "TBrank";
            this.TBrank.ReadOnly = true;
            this.TBrank.Size = new System.Drawing.Size(37, 13);
            this.TBrank.TabIndex = 59;
            this.TBrank.Text = "N/A";
            this.TBrank.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBcurrentrank
            // 
            this.TBcurrentrank.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TBcurrentrank.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBcurrentrank.Location = new System.Drawing.Point(87, 15);
            this.TBcurrentrank.MaxLength = 6;
            this.TBcurrentrank.Name = "TBcurrentrank";
            this.TBcurrentrank.ReadOnly = true;
            this.TBcurrentrank.Size = new System.Drawing.Size(69, 13);
            this.TBcurrentrank.TabIndex = 58;
            this.TBcurrentrank.Text = "N/A";
            this.TBcurrentrank.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PlayerInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 504);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TCstat);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlayerInfo";
            this.Resizable = false;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "PlayerInfo";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.TopMost = true;
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TCstat.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label LBlegionAP;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Label LBlegion;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Label LBpositionx;
        public System.Windows.Forms.Label LBpositiony;
        public System.Windows.Forms.Label LBpositionz;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label LBcamerax;
        public System.Windows.Forms.Label LBcameray;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl TCstat;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label LBcube;
        public System.Windows.Forms.TextBox TBwill;
        public System.Windows.Forms.TextBox TBknowledge;
        public System.Windows.Forms.TextBox TBaccuracy;
        public System.Windows.Forms.TextBox TBagility;
        public System.Windows.Forms.TextBox TBhealth;
        public System.Windows.Forms.TextBox TBpower;
        public System.Windows.Forms.TextBox TBmp;
        public System.Windows.Forms.TextBox TBhp;
        public System.Windows.Forms.TextBox TBatkspeed;
        public System.Windows.Forms.TextBox TBcritmain;
        public System.Windows.Forms.TextBox TBaccmain;
        public System.Windows.Forms.TextBox TBattackmain;
        public System.Windows.Forms.TextBox TBcritstrikefort;
        public System.Windows.Forms.TextBox TBcritstrikeres;
        public System.Windows.Forms.TextBox TBevansion;
        public System.Windows.Forms.TextBox TBparry;
        public System.Windows.Forms.TextBox TBblock;
        public System.Windows.Forms.TextBox TBphydef;
        public System.Windows.Forms.TextBox TBcastspeed;
        public System.Windows.Forms.TextBox TBhealingboost;
        public System.Windows.Forms.TextBox TBcritspell;
        public System.Windows.Forms.TextBox TBmagicacc;
        public System.Windows.Forms.TextBox TBmagicboost;
        public System.Windows.Forms.TextBox TBearthres;
        public System.Windows.Forms.TextBox TBwaterres;
        public System.Windows.Forms.TextBox TBwindres;
        public System.Windows.Forms.TextBox TBfireres;
        public System.Windows.Forms.TextBox TBspellfort;
        public System.Windows.Forms.TextBox TBcritspellres;
        public System.Windows.Forms.TextBox TBmagicres;
        public System.Windows.Forms.TextBox TBmagicsupp;
        public System.Windows.Forms.TextBox TBmagicdef;
        public System.Windows.Forms.TextBox TBspeed;
        public System.Windows.Forms.TextBox TBcritoff;
        public System.Windows.Forms.TextBox TBaccoff;
        public System.Windows.Forms.TextBox TBattackoff;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabPage tabPage5;
        public System.Windows.Forms.TextBox TBtotalap;
        public System.Windows.Forms.TextBox TBtodayap;
        public System.Windows.Forms.TextBox TBlastweekap;
        public System.Windows.Forms.TextBox TBthisweekap;
        public System.Windows.Forms.TextBox TBtotalely;
        public System.Windows.Forms.TextBox TBtodayely;
        public System.Windows.Forms.TextBox TBlastweekely;
        public System.Windows.Forms.TextBox TBthisweekely;
        public System.Windows.Forms.TextBox TBtotalhp;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        public System.Windows.Forms.TextBox TBtodayhp;
        public System.Windows.Forms.TextBox TBlastweekhp;
        public System.Windows.Forms.TextBox TBthisweekhp;
        public System.Windows.Forms.TextBox TBrank;
        public System.Windows.Forms.TextBox TBcurrentrank;
    }
}