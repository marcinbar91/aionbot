﻿using MetroFramework.Forms;
using System;

using System.IO;
using System.Windows.Forms;
using System.Threading;
using MB.ProcessControl;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace BOT
{
    public partial class MetroMyBot : MetroForm
    {
        Setting setting = new Setting();
        TargetInfo target = new TargetInfo();
        PlayerInfo player = new PlayerInfo();
        public static Bot botcontrol = new Bot();
        Thread threadStart;
        static bool stopBot = false;
        Window window = new Window(Launcher.processHandlekey);

        Potion pot = new Potion();

        public MetroMyBot()
        {
            this.TopMost = true;
            InitializeComponent();
        }      

        private void MyBot_FormClosed(object sender, FormClosedEventArgs e)
        {
            stopBot = true;
            threadStart.Join();
            Application.Exit();
        }

        private void MyBot_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "MyBot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.OK)
            {
                e.Cancel = true;
            }
        }

        private static void RefreshValueAndForm()
        {
            while (true && !stopBot)
            {
                RefreshForm.Do();
                Thread.Sleep(200);
            }
        }



        #region Log
        private void LogSystem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }

        private void LogHunting_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }

        private void LogGather_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }

        private void LogUse_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                LogSystem.BackColor = colorDialog1.Color;
        }
        private void BTNlogclear_Click(object sender, EventArgs e)
        {
            RTBlog.Clear();
        }

        #endregion

        #region Script
        private void BTNscriptclear_Click(object sender, EventArgs e)
        {
            ListScript.Items.Clear();
        }

        private void BTNrecord_Click(object sender, EventArgs e)
        {
            if (BTNrecord.Text == "Record")
            {
                BTNrecord.Text = "Stop Record";
                Properties.LastScript.Default.Fly = Value.Player.Flight.Type == 1 ? true : false;
                TimerScript.Start();
            }
            else if (BTNrecord.Text == "Stop Record")
            {
                BTNrecord.Text = "Record";
                Properties.LastScript.Default.LastPosX = 0;
                Properties.LastScript.Default.LastPosY = 0;
                TimerScript.Stop();
            }

        }


        private void TimerScript_Tick(object sender, EventArgs e)
        {
            if ((int)Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, Properties.LastScript.Default.LastPosX, Properties.LastScript.Default.LastPosY) > NRscriptstep.Value)
            {
                ListScript.Items.Add(Properties.ScriptSetting.Default.go + "(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
                Properties.LastScript.Default.LastPosX = Value.Player.Position.X;
                Properties.LastScript.Default.LastPosY = Value.Player.Position.Y;
            }
            if ((Value.Player.Flight.Type == 1 || Value.Player.Flight.Type == 7) && Properties.LastScript.Default.Fly == false)
            {
                ListScript.Items.Add(Properties.ScriptSetting.Default.fly);
                Properties.LastScript.Default.Fly = true;
            }
            else if (!(Value.Player.Flight.Type == 1 || Value.Player.Flight.Type == 7) && Properties.LastScript.Default.Fly)
            {
                ListScript.Items.Add(Properties.ScriptSetting.Default.land);
                Properties.LastScript.Default.Fly = false;
            }
        }

        private void BTNscriptsave_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Waypoint (*.way)|*.way";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(save.FileName);
                foreach (string s in ListScript.Items)
                {
                    if (s != "")
                        writer.WriteLine(s);
                }
                writer.Close();
            }
            save.Dispose();
        }

        private void BTNscriptload_Click(object sender, EventArgs e)
        {
            ListScript.Items.Clear();
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(open.FileName);
                while (!sr.EndOfStream)
                {
                    string text = sr.ReadLine();
                    if (text != "")
                        ListScript.Items.Add(text);
                }
                sr.Close();
            }
            open.Dispose();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListScript.Items.Clear();
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(ListScript);
            selectedItems = ListScript.SelectedItems;

            if (ListScript.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    ListScript.Items.Remove(selectedItems[i]);
            }
        }

        private void goToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListScript.Items.Add(Properties.ScriptSetting.Default.go + "(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
        }

        private void flyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListScript.Items.Add(Properties.ScriptSetting.Default.fly);
        }

        private void landToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListScript.Items.Add(Properties.ScriptSetting.Default.land);
        }


        private void BTNwaybrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == DialogResult.OK)
            {
                if (Script.CheckScript(new Script(open.FileName)))
                    TBactionway.Text = open.FileName;
                else
                    MessageBox.Show("The script is incorrect!");
            }
            open.Dispose();
        }
        private void BTNdeathbrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == DialogResult.OK)
            {
                if (Script.CheckScript(new Script(open.FileName)))
                    TBdeathway.Text = open.FileName;
                else
                    MessageBox.Show("The script is incorrect");
            }
            open.Dispose();
        }


        public void BTNStartBot_Click(object sender, EventArgs e)
        {
         
            if (BTNStartBot.Text == "Start Bot")
            {
                Bot.StartBot();
            }
            else
            {
                Bot.Stop();
            }
        }
        #region Aethertapping
        private void BTNAddAtherSafeSpot_Click(object sender, EventArgs e)
        {
            ListAetherSafeSpot.Items.Add("(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
        }
        private void TSMdelete_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(ListAetherSafeSpot);
            selectedItems = ListAetherSafeSpot.SelectedItems;

            if (ListAetherSafeSpot.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    ListAetherSafeSpot.Items.Remove(selectedItems[i]);
            }
        }
        #endregion Aethertapping
        #endregion Script

        #region Child Form
        private void MIprofile_Click(object sender, EventArgs e)
        {
            if (profiToolStripMenuItem.Checked)
            {
                player.RUN(false);
                profiToolStripMenuItem.Checked = false;
                player.Hide();
            }
            else
            {
                profiToolStripMenuItem.Checked = true;
                player.Show();
                player.RUN(true);
            }
        }

        private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (alwaysOnTopToolStripMenuItem.Checked)
            {
                this.TopMost = false;
                alwaysOnTopToolStripMenuItem.Checked = false;
            }
            else
            {
                this.TopMost = true;
                alwaysOnTopToolStripMenuItem.Checked = true;
            }
        }

        private void MItargetInfo_Click(object sender, EventArgs e)
        {
            if (targetInfoToolStripMenuItem.Checked)
            {
                target.Hide();
                target.timer1.Enabled = false;
                targetInfoToolStripMenuItem.Checked = false;

            }
            else
            {
                target.timer1.Enabled = true;
                targetInfoToolStripMenuItem.Checked = true;
                target.Show();

            }
        }

        private void MIexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Setting
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want save as default setting?", "MyBot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Setting (*.ini)|*.ini";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(save.FileName))
                        File.Delete(save.FileName);
                    setting.SaveSettingIni(save.FileName);
                }
                save.Dispose();
            }
            else
            {
                if (File.Exists(Setting.defaultpath))
                    File.Delete(Setting.defaultpath);
                setting.SaveSettingIni(Setting.defaultpath);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Setting (*.ini)|*.ini";
            if (open.ShowDialog() == DialogResult.OK)
            {
                setting.LoadSettingIni(open.FileName);
            }
            open.Dispose();
        }
        #endregion Setting

        private void BTNskilladd_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button)sender;
            System.Drawing.Point ptLowerLeft = new System.Drawing.Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);
            DGVskillcontext.Show(ptLowerLeft);
        }

        private void DGVskill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
            {
                MoveUpDataGridView(DGVskill);
            }
            if (e.KeyCode.Equals(Keys.Down))
            {
                MoveDownDataGridView(DGVskill);
            }
            e.Handled = true;
        }
        private void DGVbuff_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
            {
                MoveUpDataGridView(DGVbuff);
            }
            if (e.KeyCode.Equals(Keys.Down))
            {
                MoveDownDataGridView(DGVbuff);
            }
            e.Handled = true;
        }

        private void DGVpotion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
            {
                MoveUpDataGridView(DGVpotion);
            }
            if (e.KeyCode.Equals(Keys.Down))
            {
                MoveDownDataGridView(DGVpotion);
            }
            e.Handled = true;
        }

        private void RemoveFromDataGridView(DataGridView dgv)
        {
            if (dgv != null)
            {
                foreach (DataGridViewRow item in dgv.SelectedRows)
                    dgv.Rows.RemoveAt(item.Index);
            }
        }
        private void MoveUpDataGridView(DataGridView dgv)
        {
            if (dgv != null)
            {
                if (dgv.RowCount > 0)
                {
                    if (dgv.SelectedRows.Count > 0)
                    {
                        int rowCount = dgv.Rows.Count;
                        int index = dgv.SelectedCells[0].OwningRow.Index;

                        if (index == 0)
                        {
                            return;
                        }
                        DataGridViewRowCollection rows = dgv.Rows;

                        // remove the previous row and add it behind the selected row.
                        DataGridViewRow prevRow = rows[index - 1];
                        rows.Remove(prevRow);
                        prevRow.Frozen = false;
                        rows.Insert(index, prevRow);
                        dgv.ClearSelection();
                        dgv.Rows[index - 1].Selected = true;
                    }
                }
            }
        }

        private void MoveDownDataGridView(DataGridView dgv)
        {
            if (dgv != null)
            {
                if (dgv.RowCount > 0)
                {
                    if (dgv.SelectedRows.Count > 0)
                    {
                        int rowCount = dgv.Rows.Count;
                        int index = dgv.SelectedCells[0].OwningRow.Index;

                        if (index == (rowCount - 1))
                        {
                            return;
                        }
                        DataGridViewRowCollection rows = dgv.Rows;

                        // remove the next row and add it in front of the selected row.
                        DataGridViewRow nextRow = rows[index + 1];
                        rows.Remove(nextRow);
                        nextRow.Frozen = false;
                        rows.Insert(index, nextRow);
                        dgv.ClearSelection();
                        dgv.Rows[index + 1].Selected = true;
                    }
                }
            }
        }
        private DataGridView GetDataGridView()
        {
            string source = ((ContextMenuStrip)DGVskillcontext).SourceControl.Name;
            if (source == DGVskill.Name)
            {
                return DGVskill;
            }
            else if (source == DGVbuff.Name)
            {
                return DGVbuff;
            }
            else if (source == DGVpotion.Name)
            {
                return DGVpotion;
            }
            return null;
        }

        private void AddToDataGridView(DataGridView dgv)
        {
            if (dgv != null)
            {
                MsgBox form;
                if (dgv == DGVskill)
                {
                    form = new MsgBox(MsgBox.Display.Skill);
                }
                else if (dgv == DGVbuff)
                {
                    form = new MsgBox(MsgBox.Display.Buff);
                }
                else
                {
                    form = new MsgBox(MsgBox.Display.Potion);
                }

                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {

                    dgv.AllowUserToAddRows = true;
                    DataGridViewRow row = (DataGridViewRow)dgv.Rows[0].Clone();
                    row.Cells[0].Value = form.Skillbar;
                    row.Cells[1].Value = form.Key;
                    row.Cells[2].Value = form.Cooldown;
                    row.Cells[3].Value = form.CastTime;
                    row.Cells[4].Value = new DateTime(2016, 1, 1, 0, 0, 0);
                    if (dgv == DGVskill)
                    {
                        row.Cells[5].Value = form.ChainLVL;
                        row.Cells[6].Value = form.ChainWait;
                    }
                    else if (dgv == DGVbuff)
                    {
                        row.Cells[5].Value = form.ChainLVL;
                        row.Cells[6].Value = form.ChainWait;
                        row.Cells[7].Value = form.OnCombat;
                    }
                    else if (dgv == DGVpotion)
                    {
                        row.Cells[5].Value = form.Effect;
                        row.Cells[6].Value = form.Percent;
                    }

                    dgv.Rows.Add(row);
                    dgv.AllowUserToAddRows = false;
                }


            }

        }


        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveFromDataGridView(GetDataGridView());
        }
        private void upToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MoveUpDataGridView(GetDataGridView());
        }

        private void downToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveDownDataGridView(GetDataGridView());
        }
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddToDataGridView(GetDataGridView());
        }

        #region  Window Manager
        private void TrayMinimizerForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                TrayIcon.Visible = true;
                TrayIcon.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                TrayIcon.Visible = false;
            }
        }

        private void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }


        private void BTNfullscreen_Click(object sender, EventArgs e)
        {
            window.FullScreen();
        }

        private void BTNHide_CheckedChanged(object sender, EventArgs e)
        {
            if (BTNHide.Text == "Hide")
            {
                BTNHide.Text = "Show";
                window.Hide();
            }
            else
            {
                BTNHide.Text = "Hide";
                window.Show();
            }
        }

        private void BTNresize_Click(object sender, EventArgs e)
        {
            window.Resize(Convert.ToInt32(TBwindowx.Text), Convert.ToInt32(TBwindowy.Text), Convert.ToInt32(TBwindowwidth.Text), Convert.ToInt32(TBwindowheight.Text));
        }
        #endregion  Window Manager

        #region Potion

        private void DGVpotion_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DGVskillcontext.Show();
            }
        }

        #endregion

        private void ListAetherSafeSpot_MouseDown(object sender, MouseEventArgs e)
        {
            CMSAetherSaveSpot.Show();
        }

        private void DGVskillcontext_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }



        private void MyBot_Load(object sender, EventArgs e)
        {
            Setting.LoadDefaultSetting();
            if (File.Exists(Setting.defaultpath))
                setting.LoadSettingIni(Setting.defaultpath);
            threadStart = new Thread(RefreshValueAndForm);
            threadStart.Start();

        }

        private async void button1_Click(object sender, EventArgs e)
        {
           
        }

            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
   

        private void button4_Click(object sender, EventArgs e)
        {




            //foreach (Keys foo in Enum.GetValues(typeof(Keys)))
            //    Console.WriteLine(foo);
            ////Console.WriteLine(Value.Skillbar.ID(1, 1, 1));

            //  timer1.Start();
            SkillbarIteam s = Value.Skillbar.ListID;
            Console.WriteLine();
            Console.Write("ID" + s.ID[0]);
            Console.Write(" Name" + s.Name[0]);
            Console.Write(" CastTime" + s.CastTime[0]);
            Console.Write(" Cooldown" + s.Cooldown[0]);
            Console.Write(" UsageCost" + s.UsageCost[0]);
            Console.Write(" UsageType" + s.UsageType[0]);




        }


        private void timer1_Tick(object sender, EventArgs e)
        {
           // Buff.Use();
        }

        private void CBHuntingGetLoot_CheckedChanged(object sender, EventArgs e)
        {
            if (CBHuntingGetLoot.Checked)
            {
                Properties.Settings.Default.GetLoot = true;
            }
            else
            {
                Properties.Settings.Default.GetLoot = false;
            }

        }
    }
}
