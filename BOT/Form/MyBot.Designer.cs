﻿namespace BOT
{
    partial class MyBot
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyBot));
            this.TBCMainWindow = new System.Windows.Forms.TabControl();
            this.TBPInformation = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.BTNHide = new System.Windows.Forms.CheckBox();
            this.BTNfullscreen = new System.Windows.Forms.Button();
            this.TBwindowx = new System.Windows.Forms.TextBox();
            this.TBwindowy = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.BTNresize = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TBwindowheight = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TBwindowwidth = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.GBtarget = new System.Windows.Forms.GroupBox();
            this.DRWtargetmp = new MB.exProgressBar.exProgressBar();
            this.DRWtargethp = new MB.exProgressBar.exProgressBar();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.GBplayer = new System.Windows.Forms.GroupBox();
            this.DRWfly = new MB.exProgressBar.exProgressBar();
            this.DRWexp = new MB.exProgressBar.exProgressBar();
            this.DRWdp = new MB.exProgressBar.exProgressBar();
            this.DRWmp = new MB.exProgressBar.exProgressBar();
            this.DRWhp = new MB.exProgressBar.exProgressBar();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.PNLdplevel = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.PNLclass = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TCplayer = new System.Windows.Forms.TabPage();
            this.LBflytype = new System.Windows.Forms.Label();
            this.LBloggin = new System.Windows.Forms.Label();
            this.TCtarget = new System.Windows.Forms.TabPage();
            this.TBPWaypoint = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TBdeathway = new System.Windows.Forms.TextBox();
            this.BTNdeathbrowse = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TBactionway = new System.Windows.Forms.TextBox();
            this.BTNwaybrowse = new System.Windows.Forms.Button();
            this.TBPPotion = new System.Windows.Forms.TabPage();
            this.AddPotionKey = new MB.KeyButton.KeyButton();
            this.label8 = new System.Windows.Forms.Label();
            this.AddPotionPercent = new System.Windows.Forms.NumericUpDown();
            this.AddPotionEffect = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.DGVpotion = new System.Windows.Forms.DataGridView();
            this.PotionSkillbar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionEffect = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionCooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionCastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PotionLastUsed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DGVskillcontext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.upToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.AddPotionCooldown = new System.Windows.Forms.NumericUpDown();
            this.BTNpotionadd = new System.Windows.Forms.Button();
            this.AddPotionSkillbar = new System.Windows.Forms.ComboBox();
            this.TBPSkill = new System.Windows.Forms.TabPage();
            this.AddKey = new MB.KeyButton.KeyButton();
            this.CBOnCombat = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DGVskill = new System.Windows.Forms.DataGridView();
            this.AttackSkillbar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackCooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackCastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackChain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackChainWait = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttackLastUsed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DGVbuff = new System.Windows.Forms.DataGridView();
            this.BuffSkillbar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffCooldown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffCastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffChainLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffChainWait = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuffOnCombat = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BuffLastUsed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label51 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.AddChainLevel = new System.Windows.Forms.NumericUpDown();
            this.AddChainWait = new System.Windows.Forms.NumericUpDown();
            this.AddCastTime = new System.Windows.Forms.NumericUpDown();
            this.AddCooldown = new System.Windows.Forms.NumericUpDown();
            this.BTNskilladd = new System.Windows.Forms.Button();
            this.AddSkillbar = new System.Windows.Forms.ComboBox();
            this.TBPHunting = new System.Windows.Forms.TabPage();
            this.TBPScript = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.CBscriptadd = new System.Windows.Forms.ComboBox();
            this.BTNscriptadd = new System.Windows.Forms.Button();
            this.BTNscriptload = new System.Windows.Forms.Button();
            this.BTNscriptsave = new System.Windows.Forms.Button();
            this.NRscriptstep = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.BTNrecord = new System.Windows.Forms.Button();
            this.BTNscriptclear = new System.Windows.Forms.Button();
            this.RTBscript = new System.Windows.Forms.RichTextBox();
            this.TBPKey = new System.Windows.Forms.TabPage();
            this.CBland = new MB.KeyButton.KeyButton();
            this.CBfly = new MB.KeyButton.KeyButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CBselectyourself = new MB.KeyButton.KeyButton();
            this.CBrest = new MB.KeyButton.KeyButton();
            this.CBloot = new MB.KeyButton.KeyButton();
            this.CBselect = new MB.KeyButton.KeyButton();
            this.CBforward = new MB.KeyButton.KeyButton();
            this.label43 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.TBPLog = new System.Windows.Forms.TabPage();
            this.BTNlogclear = new System.Windows.Forms.Button();
            this.RTBlog = new System.Windows.Forms.RichTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.LogUse = new System.Windows.Forms.PictureBox();
            this.LogGather = new System.Windows.Forms.PictureBox();
            this.LogHunting = new System.Windows.Forms.PictureBox();
            this.LogSystem = new System.Windows.Forms.PictureBox();
            this.TBPAethertapping = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.CBaetherReturn = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.BTNAddAetherSafeSpot = new System.Windows.Forms.Button();
            this.ListAetherSafeSpot = new System.Windows.Forms.ListBox();
            this.CMSAetherSaveSpot = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TSMdelete = new System.Windows.Forms.ToolStripMenuItem();
            this.BTNStartBot = new System.Windows.Forms.Button();
            this.ToolTipStat = new System.Windows.Forms.ToolTip(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.BWscript = new System.ComponentModel.BackgroundWorker();
            this.TimerScript = new System.Windows.Forms.Timer(this.components);
            this.bOTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MIexit = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MIprofile = new System.Windows.Forms.ToolStripMenuItem();
            this.MItargetInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.MIalwaysOnTop = new System.Windows.Forms.ToolStripMenuItem();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.CMSskill = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.attackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatCpu = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bOTToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.targetInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alwaysOnTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metroUiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TBCMainWindow.SuspendLayout();
            this.TBPInformation.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.GBtarget.SuspendLayout();
            this.GBplayer.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TCplayer.SuspendLayout();
            this.TBPWaypoint.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TBPPotion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddPotionPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpotion)).BeginInit();
            this.DGVskillcontext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddPotionCooldown)).BeginInit();
            this.TBPSkill.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVskill)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVbuff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCastTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCooldown)).BeginInit();
            this.TBPScript.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NRscriptstep)).BeginInit();
            this.TBPKey.SuspendLayout();
            this.TBPLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogHunting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogSystem)).BeginInit();
            this.TBPAethertapping.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.CMSAetherSaveSpot.SuspendLayout();
            this.CMSskill.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TBCMainWindow
            // 
            this.TBCMainWindow.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TBCMainWindow.Controls.Add(this.TBPInformation);
            this.TBCMainWindow.Controls.Add(this.TBPWaypoint);
            this.TBCMainWindow.Controls.Add(this.TBPPotion);
            this.TBCMainWindow.Controls.Add(this.TBPSkill);
            this.TBCMainWindow.Controls.Add(this.TBPHunting);
            this.TBCMainWindow.Controls.Add(this.TBPScript);
            this.TBCMainWindow.Controls.Add(this.TBPKey);
            this.TBCMainWindow.Controls.Add(this.TBPLog);
            this.TBCMainWindow.Controls.Add(this.TBPAethertapping);
            this.TBCMainWindow.HotTrack = true;
            this.TBCMainWindow.Location = new System.Drawing.Point(12, 27);
            this.TBCMainWindow.Name = "TBCMainWindow";
            this.TBCMainWindow.SelectedIndex = 0;
            this.TBCMainWindow.Size = new System.Drawing.Size(753, 502);
            this.TBCMainWindow.TabIndex = 39;
            // 
            // TBPInformation
            // 
            this.TBPInformation.BackColor = System.Drawing.SystemColors.Control;
            this.TBPInformation.Controls.Add(this.button2);
            this.TBPInformation.Controls.Add(this.groupBox7);
            this.TBPInformation.Controls.Add(this.button3);
            this.TBPInformation.Controls.Add(this.GBtarget);
            this.TBPInformation.Controls.Add(this.GBplayer);
            this.TBPInformation.Controls.Add(this.tabControl1);
            this.TBPInformation.Location = new System.Drawing.Point(4, 25);
            this.TBPInformation.Name = "TBPInformation";
            this.TBPInformation.Padding = new System.Windows.Forms.Padding(3);
            this.TBPInformation.Size = new System.Drawing.Size(745, 473);
            this.TBPInformation.TabIndex = 0;
            this.TBPInformation.Text = "Information";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(30, 362);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 73;
            this.button2.Text = "Start";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.BTNHide);
            this.groupBox7.Controls.Add(this.BTNfullscreen);
            this.groupBox7.Controls.Add(this.TBwindowx);
            this.groupBox7.Controls.Add(this.TBwindowy);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.BTNresize);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.TBwindowheight);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.TBwindowwidth);
            this.groupBox7.Location = new System.Drawing.Point(6, 148);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(413, 152);
            this.groupBox7.TabIndex = 72;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Window Manager";
            // 
            // BTNHide
            // 
            this.BTNHide.Appearance = System.Windows.Forms.Appearance.Button;
            this.BTNHide.Location = new System.Drawing.Point(6, 48);
            this.BTNHide.Name = "BTNHide";
            this.BTNHide.Size = new System.Drawing.Size(75, 23);
            this.BTNHide.TabIndex = 73;
            this.BTNHide.Text = "Hide";
            this.BTNHide.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BTNHide.UseVisualStyleBackColor = true;
            this.BTNHide.CheckedChanged += new System.EventHandler(this.BTNHide_CheckedChanged);
            // 
            // BTNfullscreen
            // 
            this.BTNfullscreen.Location = new System.Drawing.Point(6, 19);
            this.BTNfullscreen.Name = "BTNfullscreen";
            this.BTNfullscreen.Size = new System.Drawing.Size(75, 23);
            this.BTNfullscreen.TabIndex = 59;
            this.BTNfullscreen.Text = "Full Screen";
            this.BTNfullscreen.UseVisualStyleBackColor = true;
            this.BTNfullscreen.Click += new System.EventHandler(this.BTNfullscreen_Click);
            // 
            // TBwindowx
            // 
            this.TBwindowx.Location = new System.Drawing.Point(150, 77);
            this.TBwindowx.Name = "TBwindowx";
            this.TBwindowx.Size = new System.Drawing.Size(75, 20);
            this.TBwindowx.TabIndex = 60;
            this.TBwindowx.Text = "0";
            // 
            // TBwindowy
            // 
            this.TBwindowy.Location = new System.Drawing.Point(150, 103);
            this.TBwindowy.Name = "TBwindowy";
            this.TBwindowy.Size = new System.Drawing.Size(75, 20);
            this.TBwindowy.TabIndex = 61;
            this.TBwindowy.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(236, 80);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 13);
            this.label23.TabIndex = 68;
            this.label23.Text = "Width:";
            // 
            // BTNresize
            // 
            this.BTNresize.Location = new System.Drawing.Point(6, 77);
            this.BTNresize.Name = "BTNresize";
            this.BTNresize.Size = new System.Drawing.Size(75, 23);
            this.BTNresize.TabIndex = 62;
            this.BTNresize.Text = "Resize";
            this.BTNresize.UseVisualStyleBackColor = true;
            this.BTNresize.Click += new System.EventHandler(this.BTNresize_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(233, 106);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 67;
            this.label24.Text = "Height:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(87, 80);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 63;
            this.label21.Text = "Position X:";
            // 
            // TBwindowheight
            // 
            this.TBwindowheight.Location = new System.Drawing.Point(280, 103);
            this.TBwindowheight.Name = "TBwindowheight";
            this.TBwindowheight.Size = new System.Drawing.Size(75, 20);
            this.TBwindowheight.TabIndex = 66;
            this.TBwindowheight.Text = "600";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(87, 106);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 13);
            this.label22.TabIndex = 64;
            this.label22.Text = "Position Y:";
            // 
            // TBwindowwidth
            // 
            this.TBwindowwidth.Location = new System.Drawing.Point(280, 77);
            this.TBwindowwidth.Name = "TBwindowwidth";
            this.TBwindowwidth.Size = new System.Drawing.Size(75, 20);
            this.TBwindowwidth.TabIndex = 65;
            this.TBwindowwidth.Text = "600";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(157, 362);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 53;
            this.button3.Text = "Stop";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // GBtarget
            // 
            this.GBtarget.Controls.Add(this.DRWtargetmp);
            this.GBtarget.Controls.Add(this.DRWtargethp);
            this.GBtarget.Controls.Add(this.label49);
            this.GBtarget.Controls.Add(this.label50);
            this.GBtarget.Location = new System.Drawing.Point(316, 6);
            this.GBtarget.Name = "GBtarget";
            this.GBtarget.Size = new System.Drawing.Size(250, 136);
            this.GBtarget.TabIndex = 52;
            this.GBtarget.TabStop = false;
            this.GBtarget.Text = "Target";
            // 
            // DRWtargetmp
            // 
            this.DRWtargetmp.BackColor = System.Drawing.Color.Gray;
            this.DRWtargetmp.Color1 = System.Drawing.Color.DarkBlue;
            this.DRWtargetmp.Color2 = System.Drawing.Color.Blue;
            this.DRWtargetmp.Location = new System.Drawing.Point(37, 41);
            this.DRWtargetmp.Maximum = 100F;
            this.DRWtargetmp.Minimum = 0F;
            this.DRWtargetmp.Name = "DRWtargetmp";
            this.DRWtargetmp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargetmp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargetmp.RoundedEdge = 0;
            this.DRWtargetmp.Size = new System.Drawing.Size(182, 17);
            this.DRWtargetmp.TabIndex = 75;
            this.DRWtargetmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWtargetmp.TextColor = System.Drawing.Color.White;
            this.DRWtargetmp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWtargetmp.Txt = null;
            this.DRWtargetmp.Value = 50F;
            // 
            // DRWtargethp
            // 
            this.DRWtargethp.BackColor = System.Drawing.Color.Gray;
            this.DRWtargethp.Color1 = System.Drawing.Color.DarkRed;
            this.DRWtargethp.Color2 = System.Drawing.Color.Red;
            this.DRWtargethp.Location = new System.Drawing.Point(37, 19);
            this.DRWtargethp.Maximum = 100F;
            this.DRWtargethp.Minimum = 0F;
            this.DRWtargethp.Name = "DRWtargethp";
            this.DRWtargethp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargethp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWtargethp.RoundedEdge = 0;
            this.DRWtargethp.Size = new System.Drawing.Size(182, 17);
            this.DRWtargethp.TabIndex = 75;
            this.DRWtargethp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWtargethp.TextColor = System.Drawing.Color.White;
            this.DRWtargethp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWtargethp.Txt = null;
            this.DRWtargethp.Value = 50F;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(7, 22);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(24, 13);
            this.label49.TabIndex = 41;
            this.label49.Text = "HP";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(6, 41);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(25, 13);
            this.label50.TabIndex = 42;
            this.label50.Text = "MP";
            // 
            // GBplayer
            // 
            this.GBplayer.Controls.Add(this.DRWfly);
            this.GBplayer.Controls.Add(this.DRWexp);
            this.GBplayer.Controls.Add(this.DRWdp);
            this.GBplayer.Controls.Add(this.DRWmp);
            this.GBplayer.Controls.Add(this.DRWhp);
            this.GBplayer.Controls.Add(this.label54);
            this.GBplayer.Controls.Add(this.label53);
            this.GBplayer.Controls.Add(this.label31);
            this.GBplayer.Controls.Add(this.PNLdplevel);
            this.GBplayer.Controls.Add(this.label32);
            this.GBplayer.Controls.Add(this.label33);
            this.GBplayer.Controls.Add(this.PNLclass);
            this.GBplayer.Location = new System.Drawing.Point(6, 6);
            this.GBplayer.Name = "GBplayer";
            this.GBplayer.Size = new System.Drawing.Size(304, 136);
            this.GBplayer.TabIndex = 51;
            this.GBplayer.TabStop = false;
            this.GBplayer.Text = "Player";
            // 
            // DRWfly
            // 
            this.DRWfly.BackColor = System.Drawing.Color.Gray;
            this.DRWfly.Color1 = System.Drawing.Color.Green;
            this.DRWfly.Color2 = System.Drawing.Color.LightGreen;
            this.DRWfly.Location = new System.Drawing.Point(37, 112);
            this.DRWfly.Maximum = 100F;
            this.DRWfly.Minimum = 0F;
            this.DRWfly.Name = "DRWfly";
            this.DRWfly.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWfly.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWfly.RoundedEdge = 0;
            this.DRWfly.Size = new System.Drawing.Size(182, 17);
            this.DRWfly.TabIndex = 78;
            this.DRWfly.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWfly.TextColor = System.Drawing.Color.White;
            this.DRWfly.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWfly.Txt = null;
            this.DRWfly.Value = 50F;
            // 
            // DRWexp
            // 
            this.DRWexp.BackColor = System.Drawing.Color.Gray;
            this.DRWexp.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.DRWexp.Color2 = System.Drawing.Color.Cyan;
            this.DRWexp.Location = new System.Drawing.Point(37, 89);
            this.DRWexp.Maximum = 100F;
            this.DRWexp.Minimum = 0F;
            this.DRWexp.Name = "DRWexp";
            this.DRWexp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWexp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWexp.RoundedEdge = 0;
            this.DRWexp.Size = new System.Drawing.Size(182, 17);
            this.DRWexp.TabIndex = 77;
            this.DRWexp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWexp.TextColor = System.Drawing.Color.White;
            this.DRWexp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWexp.Txt = null;
            this.DRWexp.Value = 50F;
            // 
            // DRWdp
            // 
            this.DRWdp.BackColor = System.Drawing.Color.Gray;
            this.DRWdp.Color1 = System.Drawing.Color.YellowGreen;
            this.DRWdp.Color2 = System.Drawing.Color.Yellow;
            this.DRWdp.Location = new System.Drawing.Point(37, 65);
            this.DRWdp.Maximum = 100F;
            this.DRWdp.Minimum = 0F;
            this.DRWdp.Name = "DRWdp";
            this.DRWdp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWdp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWdp.RoundedEdge = 0;
            this.DRWdp.Size = new System.Drawing.Size(182, 17);
            this.DRWdp.TabIndex = 76;
            this.DRWdp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWdp.TextColor = System.Drawing.Color.White;
            this.DRWdp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWdp.Txt = null;
            this.DRWdp.Value = 50F;
            // 
            // DRWmp
            // 
            this.DRWmp.BackColor = System.Drawing.Color.Gray;
            this.DRWmp.Color1 = System.Drawing.Color.DarkBlue;
            this.DRWmp.Color2 = System.Drawing.Color.Blue;
            this.DRWmp.Location = new System.Drawing.Point(37, 42);
            this.DRWmp.Maximum = 100F;
            this.DRWmp.Minimum = 0F;
            this.DRWmp.Name = "DRWmp";
            this.DRWmp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWmp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWmp.RoundedEdge = 0;
            this.DRWmp.Size = new System.Drawing.Size(182, 17);
            this.DRWmp.TabIndex = 75;
            this.DRWmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWmp.TextColor = System.Drawing.Color.White;
            this.DRWmp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWmp.Txt = null;
            this.DRWmp.Value = 50F;
            // 
            // DRWhp
            // 
            this.DRWhp.BackColor = System.Drawing.Color.Gray;
            this.DRWhp.Color1 = System.Drawing.Color.DarkRed;
            this.DRWhp.Color2 = System.Drawing.Color.Red;
            this.DRWhp.Location = new System.Drawing.Point(37, 19);
            this.DRWhp.Maximum = 100F;
            this.DRWhp.Minimum = 0F;
            this.DRWhp.Name = "DRWhp";
            this.DRWhp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.DRWhp.OrientationText = System.Windows.Forms.Orientation.Horizontal;
            this.DRWhp.RoundedEdge = 0;
            this.DRWhp.Size = new System.Drawing.Size(182, 17);
            this.DRWhp.TabIndex = 74;
            this.DRWhp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DRWhp.TextColor = System.Drawing.Color.White;
            this.DRWhp.TextFont = new System.Drawing.Font("Arial", 8F);
            this.DRWhp.Txt = "";
            this.DRWhp.Value = 50F;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(8, 93);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(23, 13);
            this.label54.TabIndex = 52;
            this.label54.Text = "XP";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(8, 116);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(23, 13);
            this.label53.TabIndex = 51;
            this.label53.Text = "Fly";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(7, 71);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(24, 13);
            this.label31.TabIndex = 48;
            this.label31.Text = "DP";
            // 
            // PNLdplevel
            // 
            this.PNLdplevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PNLdplevel.Location = new System.Drawing.Point(225, 65);
            this.PNLdplevel.Name = "PNLdplevel";
            this.PNLdplevel.Size = new System.Drawing.Size(19, 19);
            this.PNLdplevel.TabIndex = 43;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(7, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(24, 13);
            this.label32.TabIndex = 49;
            this.label32.Text = "HP";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(6, 46);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 13);
            this.label33.TabIndex = 50;
            this.label33.Text = "MP";
            // 
            // PNLclass
            // 
            this.PNLclass.BackColor = System.Drawing.Color.Transparent;
            this.PNLclass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PNLclass.Location = new System.Drawing.Point(225, 14);
            this.PNLclass.Name = "PNLclass";
            this.PNLclass.Size = new System.Drawing.Size(45, 45);
            this.PNLclass.TabIndex = 39;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TCplayer);
            this.tabControl1.Controls.Add(this.TCtarget);
            this.tabControl1.Location = new System.Drawing.Point(425, 167);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(309, 125);
            this.tabControl1.TabIndex = 27;
            // 
            // TCplayer
            // 
            this.TCplayer.BackColor = System.Drawing.SystemColors.Control;
            this.TCplayer.Controls.Add(this.LBflytype);
            this.TCplayer.Controls.Add(this.LBloggin);
            this.TCplayer.Location = new System.Drawing.Point(4, 22);
            this.TCplayer.Name = "TCplayer";
            this.TCplayer.Padding = new System.Windows.Forms.Padding(3);
            this.TCplayer.Size = new System.Drawing.Size(301, 99);
            this.TCplayer.TabIndex = 0;
            this.TCplayer.Text = "Player";
            // 
            // LBflytype
            // 
            this.LBflytype.AutoSize = true;
            this.LBflytype.Location = new System.Drawing.Point(6, 67);
            this.LBflytype.Name = "LBflytype";
            this.LBflytype.Size = new System.Drawing.Size(50, 13);
            this.LBflytype.TabIndex = 32;
            this.LBflytype.Text = "Fly Type:";
            // 
            // LBloggin
            // 
            this.LBloggin.AutoSize = true;
            this.LBloggin.Location = new System.Drawing.Point(6, 42);
            this.LBloggin.Name = "LBloggin";
            this.LBloggin.Size = new System.Drawing.Size(42, 13);
            this.LBloggin.TabIndex = 27;
            this.LBloggin.Text = "Loggin:";
            // 
            // TCtarget
            // 
            this.TCtarget.BackColor = System.Drawing.SystemColors.Control;
            this.TCtarget.Location = new System.Drawing.Point(4, 22);
            this.TCtarget.Name = "TCtarget";
            this.TCtarget.Padding = new System.Windows.Forms.Padding(3);
            this.TCtarget.Size = new System.Drawing.Size(301, 99);
            this.TCtarget.TabIndex = 1;
            this.TCtarget.Text = "Target";
            // 
            // TBPWaypoint
            // 
            this.TBPWaypoint.BackColor = System.Drawing.SystemColors.Control;
            this.TBPWaypoint.Controls.Add(this.groupBox2);
            this.TBPWaypoint.Controls.Add(this.groupBox1);
            this.TBPWaypoint.Location = new System.Drawing.Point(4, 25);
            this.TBPWaypoint.Name = "TBPWaypoint";
            this.TBPWaypoint.Size = new System.Drawing.Size(745, 473);
            this.TBPWaypoint.TabIndex = 2;
            this.TBPWaypoint.Text = "Waypoint";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TBdeathway);
            this.groupBox2.Controls.Add(this.BTNdeathbrowse);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(327, 58);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Death Waypoint";
            // 
            // TBdeathway
            // 
            this.TBdeathway.Location = new System.Drawing.Point(6, 19);
            this.TBdeathway.Name = "TBdeathway";
            this.TBdeathway.Size = new System.Drawing.Size(230, 20);
            this.TBdeathway.TabIndex = 0;
            // 
            // BTNdeathbrowse
            // 
            this.BTNdeathbrowse.Location = new System.Drawing.Point(242, 19);
            this.BTNdeathbrowse.Name = "BTNdeathbrowse";
            this.BTNdeathbrowse.Size = new System.Drawing.Size(75, 23);
            this.BTNdeathbrowse.TabIndex = 1;
            this.BTNdeathbrowse.Text = "Browse";
            this.BTNdeathbrowse.UseVisualStyleBackColor = true;
            this.BTNdeathbrowse.Click += new System.EventHandler(this.BTNdeathbrowse_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TBactionway);
            this.groupBox1.Controls.Add(this.BTNwaybrowse);
            this.groupBox1.Location = new System.Drawing.Point(336, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 58);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Action Waypoint";
            // 
            // TBactionway
            // 
            this.TBactionway.Location = new System.Drawing.Point(6, 19);
            this.TBactionway.Name = "TBactionway";
            this.TBactionway.Size = new System.Drawing.Size(230, 20);
            this.TBactionway.TabIndex = 0;
            // 
            // BTNwaybrowse
            // 
            this.BTNwaybrowse.Location = new System.Drawing.Point(242, 19);
            this.BTNwaybrowse.Name = "BTNwaybrowse";
            this.BTNwaybrowse.Size = new System.Drawing.Size(75, 23);
            this.BTNwaybrowse.TabIndex = 1;
            this.BTNwaybrowse.Text = "Browse";
            this.BTNwaybrowse.UseVisualStyleBackColor = true;
            this.BTNwaybrowse.Click += new System.EventHandler(this.BTNwaybrowse_Click);
            // 
            // TBPPotion
            // 
            this.TBPPotion.BackColor = System.Drawing.SystemColors.Control;
            this.TBPPotion.Controls.Add(this.AddPotionKey);
            this.TBPPotion.Controls.Add(this.label8);
            this.TBPPotion.Controls.Add(this.AddPotionPercent);
            this.TBPPotion.Controls.Add(this.AddPotionEffect);
            this.TBPPotion.Controls.Add(this.label7);
            this.TBPPotion.Controls.Add(this.DGVpotion);
            this.TBPPotion.Controls.Add(this.label15);
            this.TBPPotion.Controls.Add(this.label17);
            this.TBPPotion.Controls.Add(this.label18);
            this.TBPPotion.Controls.Add(this.AddPotionCooldown);
            this.TBPPotion.Controls.Add(this.BTNpotionadd);
            this.TBPPotion.Controls.Add(this.AddPotionSkillbar);
            this.TBPPotion.Location = new System.Drawing.Point(4, 25);
            this.TBPPotion.Name = "TBPPotion";
            this.TBPPotion.Size = new System.Drawing.Size(745, 473);
            this.TBPPotion.TabIndex = 3;
            this.TBPPotion.Text = "Potion";
            // 
            // AddPotionKey
            // 
            this.AddPotionKey.Location = new System.Drawing.Point(144, 9);
            this.AddPotionKey.MultiKeys = false;
            this.AddPotionKey.Name = "AddPotionKey";
            this.AddPotionKey.SetText = "Wait on key down";
            this.AddPotionKey.Size = new System.Drawing.Size(54, 21);
            this.AddPotionKey.TabIndex = 111;
            this.AddPotionKey.ToolTip = true;
            this.AddPotionKey.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(330, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 110;
            this.label8.Text = "Percent";
            // 
            // AddPotionPercent
            // 
            this.AddPotionPercent.Location = new System.Drawing.Point(380, 9);
            this.AddPotionPercent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AddPotionPercent.Name = "AddPotionPercent";
            this.AddPotionPercent.Size = new System.Drawing.Size(55, 20);
            this.AddPotionPercent.TabIndex = 109;
            this.AddPotionPercent.Value = new decimal(new int[] {
            99,
            0,
            0,
            0});
            // 
            // AddPotionEffect
            // 
            this.AddPotionEffect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AddPotionEffect.FormattingEnabled = true;
            this.AddPotionEffect.Items.AddRange(new object[] {
            "HP",
            "MP",
            "Vigor",
            "Flight"});
            this.AddPotionEffect.Location = new System.Drawing.Point(257, 8);
            this.AddPotionEffect.Name = "AddPotionEffect";
            this.AddPotionEffect.Size = new System.Drawing.Size(54, 21);
            this.AddPotionEffect.TabIndex = 107;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(216, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 106;
            this.label7.Text = "Effect";
            // 
            // DGVpotion
            // 
            this.DGVpotion.AllowUserToAddRows = false;
            this.DGVpotion.AllowUserToResizeColumns = false;
            this.DGVpotion.AllowUserToResizeRows = false;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVpotion.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle26;
            this.DGVpotion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVpotion.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DGVpotion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DGVpotion.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVpotion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVpotion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PotionSkillbar,
            this.PotionKey,
            this.PotionEffect,
            this.PotionPercent,
            this.PotionCooldown,
            this.PotionCastTime,
            this.PotionLastUsed});
            this.DGVpotion.ContextMenuStrip = this.DGVskillcontext;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVpotion.DefaultCellStyle = dataGridViewCellStyle32;
            this.DGVpotion.Location = new System.Drawing.Point(19, 86);
            this.DGVpotion.MultiSelect = false;
            this.DGVpotion.Name = "DGVpotion";
            this.DGVpotion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVpotion.RowHeadersVisible = false;
            this.DGVpotion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVpotion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVpotion.Size = new System.Drawing.Size(679, 146);
            this.DGVpotion.TabIndex = 13;
            this.DGVpotion.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DGVpotion_MouseClick);
            // 
            // PotionSkillbar
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PotionSkillbar.DefaultCellStyle = dataGridViewCellStyle27;
            this.PotionSkillbar.HeaderText = "Skillbar";
            this.PotionSkillbar.Name = "PotionSkillbar";
            this.PotionSkillbar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PotionSkillbar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionKey
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PotionKey.DefaultCellStyle = dataGridViewCellStyle28;
            this.PotionKey.HeaderText = "Key";
            this.PotionKey.Name = "PotionKey";
            this.PotionKey.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PotionKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionEffect
            // 
            this.PotionEffect.HeaderText = "Effect";
            this.PotionEffect.Name = "PotionEffect";
            // 
            // PotionPercent
            // 
            this.PotionPercent.HeaderText = "Percent";
            this.PotionPercent.Name = "PotionPercent";
            // 
            // PotionCooldown
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PotionCooldown.DefaultCellStyle = dataGridViewCellStyle29;
            this.PotionCooldown.HeaderText = "Cooldown";
            this.PotionCooldown.MaxInputLength = 3;
            this.PotionCooldown.Name = "PotionCooldown";
            this.PotionCooldown.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionCastTime
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PotionCastTime.DefaultCellStyle = dataGridViewCellStyle30;
            this.PotionCastTime.HeaderText = "CastTime";
            this.PotionCastTime.MaxInputLength = 3;
            this.PotionCastTime.Name = "PotionCastTime";
            this.PotionCastTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PotionLastUsed
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PotionLastUsed.DefaultCellStyle = dataGridViewCellStyle31;
            this.PotionLastUsed.HeaderText = "Last Used";
            this.PotionLastUsed.MaxInputLength = 50;
            this.PotionLastUsed.Name = "PotionLastUsed";
            this.PotionLastUsed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PotionLastUsed.Visible = false;
            // 
            // DGVskillcontext
            // 
            this.DGVskillcontext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.upToolStripMenuItem,
            this.downToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.DGVskillcontext.Name = "DGVskillcontext";
            this.DGVskillcontext.Size = new System.Drawing.Size(118, 70);
            // 
            // upToolStripMenuItem
            // 
            this.upToolStripMenuItem.Name = "upToolStripMenuItem";
            this.upToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.upToolStripMenuItem.Text = "Up";
            this.upToolStripMenuItem.Click += new System.EventHandler(this.upToolStripMenuItem_Click);
            // 
            // downToolStripMenuItem
            // 
            this.downToolStripMenuItem.Name = "downToolStripMenuItem";
            this.downToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.downToolStripMenuItem.Text = "Down";
            this.downToolStripMenuItem.Click += new System.EventHandler(this.downToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(113, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 104;
            this.label15.Text = "Key";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(441, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 102;
            this.label17.Text = "Cooldown";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 101;
            this.label18.Text = "Skillbar";
            // 
            // AddPotionCooldown
            // 
            this.AddPotionCooldown.Location = new System.Drawing.Point(501, 9);
            this.AddPotionCooldown.Maximum = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            this.AddPotionCooldown.Name = "AddPotionCooldown";
            this.AddPotionCooldown.Size = new System.Drawing.Size(55, 20);
            this.AddPotionCooldown.TabIndex = 97;
            this.AddPotionCooldown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // BTNpotionadd
            // 
            this.BTNpotionadd.Location = new System.Drawing.Point(6, 39);
            this.BTNpotionadd.Name = "BTNpotionadd";
            this.BTNpotionadd.Size = new System.Drawing.Size(75, 23);
            this.BTNpotionadd.TabIndex = 94;
            this.BTNpotionadd.Text = "Dodaj";
            this.BTNpotionadd.UseVisualStyleBackColor = true;
            this.BTNpotionadd.Click += new System.EventHandler(this.BTNpotionadd_Click);
            // 
            // AddPotionSkillbar
            // 
            this.AddPotionSkillbar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AddPotionSkillbar.FormattingEnabled = true;
            this.AddPotionSkillbar.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.AddPotionSkillbar.Location = new System.Drawing.Point(50, 8);
            this.AddPotionSkillbar.Name = "AddPotionSkillbar";
            this.AddPotionSkillbar.Size = new System.Drawing.Size(54, 21);
            this.AddPotionSkillbar.TabIndex = 95;
            // 
            // TBPSkill
            // 
            this.TBPSkill.BackColor = System.Drawing.SystemColors.Control;
            this.TBPSkill.Controls.Add(this.AddKey);
            this.TBPSkill.Controls.Add(this.CBOnCombat);
            this.TBPSkill.Controls.Add(this.groupBox6);
            this.TBPSkill.Controls.Add(this.groupBox3);
            this.TBPSkill.Controls.Add(this.label51);
            this.TBPSkill.Controls.Add(this.label48);
            this.TBPSkill.Controls.Add(this.label47);
            this.TBPSkill.Controls.Add(this.label46);
            this.TBPSkill.Controls.Add(this.label45);
            this.TBPSkill.Controls.Add(this.label44);
            this.TBPSkill.Controls.Add(this.AddChainLevel);
            this.TBPSkill.Controls.Add(this.AddChainWait);
            this.TBPSkill.Controls.Add(this.AddCastTime);
            this.TBPSkill.Controls.Add(this.AddCooldown);
            this.TBPSkill.Controls.Add(this.BTNskilladd);
            this.TBPSkill.Controls.Add(this.AddSkillbar);
            this.TBPSkill.Location = new System.Drawing.Point(4, 25);
            this.TBPSkill.Name = "TBPSkill";
            this.TBPSkill.Size = new System.Drawing.Size(745, 473);
            this.TBPSkill.TabIndex = 4;
            this.TBPSkill.Text = "Skill";
            // 
            // AddKey
            // 
            this.AddKey.Location = new System.Drawing.Point(148, 14);
            this.AddKey.MultiKeys = false;
            this.AddKey.Name = "AddKey";
            this.AddKey.SetText = "Wait on key down";
            this.AddKey.Size = new System.Drawing.Size(54, 21);
            this.AddKey.TabIndex = 112;
            this.AddKey.ToolTip = true;
            this.AddKey.UseVisualStyleBackColor = true;
            // 
            // CBOnCombat
            // 
            this.CBOnCombat.AutoSize = true;
            this.CBOnCombat.Location = new System.Drawing.Point(614, 42);
            this.CBOnCombat.Name = "CBOnCombat";
            this.CBOnCombat.Size = new System.Drawing.Size(76, 17);
            this.CBOnCombat.TabIndex = 94;
            this.CBOnCombat.Text = "OnCombat";
            this.CBOnCombat.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.DGVskill);
            this.groupBox6.Location = new System.Drawing.Point(23, 82);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(695, 177);
            this.groupBox6.TabIndex = 93;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Attack Skill";
            // 
            // DGVskill
            // 
            this.DGVskill.AllowUserToAddRows = false;
            this.DGVskill.AllowUserToResizeColumns = false;
            this.DGVskill.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVskill.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVskill.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVskill.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DGVskill.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DGVskill.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVskill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVskill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AttackSkillbar,
            this.AttackKey,
            this.AttackCooldown,
            this.AttackCastTime,
            this.AttackChain,
            this.AttackChainWait,
            this.AttackLastUsed});
            this.DGVskill.ContextMenuStrip = this.DGVskillcontext;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVskill.DefaultCellStyle = dataGridViewCellStyle39;
            this.DGVskill.Location = new System.Drawing.Point(7, 25);
            this.DGVskill.MultiSelect = false;
            this.DGVskill.Name = "DGVskill";
            this.DGVskill.ReadOnly = true;
            this.DGVskill.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVskill.RowHeadersVisible = false;
            this.DGVskill.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVskill.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVskill.Size = new System.Drawing.Size(679, 146);
            this.DGVskill.TabIndex = 13;
            // 
            // AttackSkillbar
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackSkillbar.DefaultCellStyle = dataGridViewCellStyle7;
            this.AttackSkillbar.HeaderText = "Skillbar";
            this.AttackSkillbar.MaxInputLength = 10;
            this.AttackSkillbar.Name = "AttackSkillbar";
            this.AttackSkillbar.ReadOnly = true;
            this.AttackSkillbar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackKey
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackKey.DefaultCellStyle = dataGridViewCellStyle33;
            this.AttackKey.HeaderText = "Key";
            this.AttackKey.MaxInputLength = 5;
            this.AttackKey.Name = "AttackKey";
            this.AttackKey.ReadOnly = true;
            this.AttackKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackCooldown
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackCooldown.DefaultCellStyle = dataGridViewCellStyle34;
            this.AttackCooldown.HeaderText = "Cooldown";
            this.AttackCooldown.MaxInputLength = 3;
            this.AttackCooldown.Name = "AttackCooldown";
            this.AttackCooldown.ReadOnly = true;
            this.AttackCooldown.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackCastTime
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackCastTime.DefaultCellStyle = dataGridViewCellStyle35;
            this.AttackCastTime.HeaderText = "CastTime";
            this.AttackCastTime.MaxInputLength = 3;
            this.AttackCastTime.Name = "AttackCastTime";
            this.AttackCastTime.ReadOnly = true;
            this.AttackCastTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackChain
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackChain.DefaultCellStyle = dataGridViewCellStyle36;
            this.AttackChain.HeaderText = "Chain Level";
            this.AttackChain.MaxInputLength = 1;
            this.AttackChain.Name = "AttackChain";
            this.AttackChain.ReadOnly = true;
            this.AttackChain.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AttackChain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackChainWait
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackChainWait.DefaultCellStyle = dataGridViewCellStyle37;
            this.AttackChainWait.HeaderText = "Chain Wait";
            this.AttackChainWait.MaxInputLength = 2;
            this.AttackChainWait.Name = "AttackChainWait";
            this.AttackChainWait.ReadOnly = true;
            this.AttackChainWait.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttackLastUsed
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AttackLastUsed.DefaultCellStyle = dataGridViewCellStyle38;
            this.AttackLastUsed.HeaderText = "Last Used";
            this.AttackLastUsed.MaxInputLength = 50;
            this.AttackLastUsed.Name = "AttackLastUsed";
            this.AttackLastUsed.ReadOnly = true;
            this.AttackLastUsed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AttackLastUsed.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DGVbuff);
            this.groupBox3.Location = new System.Drawing.Point(23, 265);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(695, 177);
            this.groupBox3.TabIndex = 92;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Buff";
            // 
            // DGVbuff
            // 
            this.DGVbuff.AllowUserToAddRows = false;
            this.DGVbuff.AllowUserToResizeColumns = false;
            this.DGVbuff.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVbuff.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.DGVbuff.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVbuff.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DGVbuff.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DGVbuff.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVbuff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVbuff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuffSkillbar,
            this.BuffKey,
            this.BuffCooldown,
            this.BuffCastTime,
            this.BuffChainLevel,
            this.BuffChainWait,
            this.BuffOnCombat,
            this.BuffLastUsed});
            this.DGVbuff.ContextMenuStrip = this.DGVskillcontext;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle46.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVbuff.DefaultCellStyle = dataGridViewCellStyle46;
            this.DGVbuff.Location = new System.Drawing.Point(6, 20);
            this.DGVbuff.MultiSelect = false;
            this.DGVbuff.Name = "DGVbuff";
            this.DGVbuff.ReadOnly = true;
            this.DGVbuff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVbuff.RowHeadersVisible = false;
            this.DGVbuff.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVbuff.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVbuff.Size = new System.Drawing.Size(680, 151);
            this.DGVbuff.TabIndex = 91;
            // 
            // BuffSkillbar
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffSkillbar.DefaultCellStyle = dataGridViewCellStyle16;
            this.BuffSkillbar.HeaderText = "Skillbar";
            this.BuffSkillbar.MaxInputLength = 10;
            this.BuffSkillbar.Name = "BuffSkillbar";
            this.BuffSkillbar.ReadOnly = true;
            this.BuffSkillbar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffKey
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffKey.DefaultCellStyle = dataGridViewCellStyle40;
            this.BuffKey.HeaderText = "Key";
            this.BuffKey.MaxInputLength = 5;
            this.BuffKey.Name = "BuffKey";
            this.BuffKey.ReadOnly = true;
            this.BuffKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffCooldown
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffCooldown.DefaultCellStyle = dataGridViewCellStyle41;
            this.BuffCooldown.HeaderText = "Cooldown";
            this.BuffCooldown.MaxInputLength = 3;
            this.BuffCooldown.Name = "BuffCooldown";
            this.BuffCooldown.ReadOnly = true;
            this.BuffCooldown.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffCastTime
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffCastTime.DefaultCellStyle = dataGridViewCellStyle42;
            this.BuffCastTime.HeaderText = "CastTime";
            this.BuffCastTime.MaxInputLength = 3;
            this.BuffCastTime.Name = "BuffCastTime";
            this.BuffCastTime.ReadOnly = true;
            this.BuffCastTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BuffChainLevel
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffChainLevel.DefaultCellStyle = dataGridViewCellStyle43;
            this.BuffChainLevel.HeaderText = "Chain Level";
            this.BuffChainLevel.MaxInputLength = 1;
            this.BuffChainLevel.Name = "BuffChainLevel";
            this.BuffChainLevel.ReadOnly = true;
            this.BuffChainLevel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BuffChainLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BuffChainLevel.Visible = false;
            // 
            // BuffChainWait
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffChainWait.DefaultCellStyle = dataGridViewCellStyle44;
            this.BuffChainWait.HeaderText = "Chain Wait";
            this.BuffChainWait.MaxInputLength = 2;
            this.BuffChainWait.Name = "BuffChainWait";
            this.BuffChainWait.ReadOnly = true;
            this.BuffChainWait.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BuffChainWait.Visible = false;
            // 
            // BuffOnCombat
            // 
            this.BuffOnCombat.HeaderText = "OnCombat";
            this.BuffOnCombat.Name = "BuffOnCombat";
            this.BuffOnCombat.ReadOnly = true;
            this.BuffOnCombat.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BuffOnCombat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // BuffLastUsed
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuffLastUsed.DefaultCellStyle = dataGridViewCellStyle45;
            this.BuffLastUsed.HeaderText = "Last Used";
            this.BuffLastUsed.MaxInputLength = 50;
            this.BuffLastUsed.Name = "BuffLastUsed";
            this.BuffLastUsed.ReadOnly = true;
            this.BuffLastUsed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BuffLastUsed.Visible = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(573, 19);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(59, 13);
            this.label51.TabIndex = 89;
            this.label51.Text = "Chain Wait";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(326, 19);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(54, 13);
            this.label48.TabIndex = 88;
            this.label48.Text = "Cast Time";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(117, 19);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(25, 13);
            this.label47.TabIndex = 87;
            this.label47.Text = "Key";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(444, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(63, 13);
            this.label46.TabIndex = 86;
            this.label46.Text = "Chain Level";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(205, 19);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 85;
            this.label45.Text = "Cooldown";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 19);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(41, 13);
            this.label44.TabIndex = 84;
            this.label44.Text = "Skillbar";
            // 
            // AddChainLevel
            // 
            this.AddChainLevel.Location = new System.Drawing.Point(513, 16);
            this.AddChainLevel.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AddChainLevel.Name = "AddChainLevel";
            this.AddChainLevel.Size = new System.Drawing.Size(54, 20);
            this.AddChainLevel.TabIndex = 83;
            // 
            // AddChainWait
            // 
            this.AddChainWait.Location = new System.Drawing.Point(638, 16);
            this.AddChainWait.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AddChainWait.Name = "AddChainWait";
            this.AddChainWait.Size = new System.Drawing.Size(52, 20);
            this.AddChainWait.TabIndex = 81;
            this.AddChainWait.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // AddCastTime
            // 
            this.AddCastTime.Location = new System.Drawing.Point(386, 16);
            this.AddCastTime.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.AddCastTime.Name = "AddCastTime";
            this.AddCastTime.Size = new System.Drawing.Size(52, 20);
            this.AddCastTime.TabIndex = 80;
            this.AddCastTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // AddCooldown
            // 
            this.AddCooldown.Location = new System.Drawing.Point(265, 16);
            this.AddCooldown.Maximum = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            this.AddCooldown.Name = "AddCooldown";
            this.AddCooldown.Size = new System.Drawing.Size(55, 20);
            this.AddCooldown.TabIndex = 79;
            this.AddCooldown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // BTNskilladd
            // 
            this.BTNskilladd.Location = new System.Drawing.Point(10, 47);
            this.BTNskilladd.Name = "BTNskilladd";
            this.BTNskilladd.Size = new System.Drawing.Size(75, 23);
            this.BTNskilladd.TabIndex = 13;
            this.BTNskilladd.Text = "Dodaj";
            this.BTNskilladd.UseVisualStyleBackColor = true;
            this.BTNskilladd.Click += new System.EventHandler(this.BTNskilladd_Click);
            // 
            // AddSkillbar
            // 
            this.AddSkillbar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AddSkillbar.FormattingEnabled = true;
            this.AddSkillbar.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.AddSkillbar.Location = new System.Drawing.Point(54, 16);
            this.AddSkillbar.Name = "AddSkillbar";
            this.AddSkillbar.Size = new System.Drawing.Size(54, 21);
            this.AddSkillbar.TabIndex = 71;
            // 
            // TBPHunting
            // 
            this.TBPHunting.BackColor = System.Drawing.SystemColors.Control;
            this.TBPHunting.Location = new System.Drawing.Point(4, 25);
            this.TBPHunting.Name = "TBPHunting";
            this.TBPHunting.Size = new System.Drawing.Size(745, 473);
            this.TBPHunting.TabIndex = 5;
            this.TBPHunting.Text = "Hunting";
            // 
            // TBPScript
            // 
            this.TBPScript.Controls.Add(this.label3);
            this.TBPScript.Controls.Add(this.CBscriptadd);
            this.TBPScript.Controls.Add(this.BTNscriptadd);
            this.TBPScript.Controls.Add(this.BTNscriptload);
            this.TBPScript.Controls.Add(this.BTNscriptsave);
            this.TBPScript.Controls.Add(this.NRscriptstep);
            this.TBPScript.Controls.Add(this.label38);
            this.TBPScript.Controls.Add(this.BTNrecord);
            this.TBPScript.Controls.Add(this.BTNscriptclear);
            this.TBPScript.Controls.Add(this.RTBscript);
            this.TBPScript.Location = new System.Drawing.Point(4, 25);
            this.TBPScript.Name = "TBPScript";
            this.TBPScript.Size = new System.Drawing.Size(745, 473);
            this.TBPScript.TabIndex = 8;
            this.TBPScript.Text = "Script";
            this.TBPScript.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(283, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Add";
            // 
            // CBscriptadd
            // 
            this.CBscriptadd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBscriptadd.FormattingEnabled = true;
            this.CBscriptadd.Items.AddRange(new object[] {
            "go",
            "fly",
            "land"});
            this.CBscriptadd.Location = new System.Drawing.Point(315, 132);
            this.CBscriptadd.Name = "CBscriptadd";
            this.CBscriptadd.Size = new System.Drawing.Size(43, 21);
            this.CBscriptadd.TabIndex = 9;
            this.CBscriptadd.SelectedIndexChanged += new System.EventHandler(this.CBscriptadd_SelectedIndexChanged);
            this.CBscriptadd.MouseLeave += new System.EventHandler(this.CBscriptadd_MouseLeave);
            this.CBscriptadd.MouseHover += new System.EventHandler(this.CBscriptadd_MouseHover);
            // 
            // BTNscriptadd
            // 
            this.BTNscriptadd.Location = new System.Drawing.Point(367, 162);
            this.BTNscriptadd.Name = "BTNscriptadd";
            this.BTNscriptadd.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptadd.TabIndex = 8;
            this.BTNscriptadd.Text = "Add";
            this.BTNscriptadd.UseVisualStyleBackColor = true;
            this.BTNscriptadd.Click += new System.EventHandler(this.BTNscriptadd_Click);
            // 
            // BTNscriptload
            // 
            this.BTNscriptload.Location = new System.Drawing.Point(283, 103);
            this.BTNscriptload.Name = "BTNscriptload";
            this.BTNscriptload.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptload.TabIndex = 7;
            this.BTNscriptload.Text = "Load";
            this.BTNscriptload.UseVisualStyleBackColor = true;
            this.BTNscriptload.Click += new System.EventHandler(this.BTNscriptload_Click);
            // 
            // BTNscriptsave
            // 
            this.BTNscriptsave.Location = new System.Drawing.Point(283, 73);
            this.BTNscriptsave.Name = "BTNscriptsave";
            this.BTNscriptsave.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptsave.TabIndex = 6;
            this.BTNscriptsave.Text = "Save";
            this.BTNscriptsave.UseVisualStyleBackColor = true;
            this.BTNscriptsave.Click += new System.EventHandler(this.BTNscriptsave_Click);
            // 
            // NRscriptstep
            // 
            this.NRscriptstep.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NRscriptstep.Location = new System.Drawing.Point(422, 49);
            this.NRscriptstep.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.NRscriptstep.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NRscriptstep.Name = "NRscriptstep";
            this.NRscriptstep.Size = new System.Drawing.Size(39, 20);
            this.NRscriptstep.TabIndex = 5;
            this.NRscriptstep.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(364, 49);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(60, 13);
            this.label38.TabIndex = 4;
            this.label38.Text = "Every step ";
            // 
            // BTNrecord
            // 
            this.BTNrecord.Location = new System.Drawing.Point(283, 44);
            this.BTNrecord.Name = "BTNrecord";
            this.BTNrecord.Size = new System.Drawing.Size(75, 23);
            this.BTNrecord.TabIndex = 2;
            this.BTNrecord.Text = "Record";
            this.BTNrecord.UseVisualStyleBackColor = true;
            this.BTNrecord.Click += new System.EventHandler(this.BTNrecord_Click);
            // 
            // BTNscriptclear
            // 
            this.BTNscriptclear.Location = new System.Drawing.Point(283, 14);
            this.BTNscriptclear.Name = "BTNscriptclear";
            this.BTNscriptclear.Size = new System.Drawing.Size(75, 23);
            this.BTNscriptclear.TabIndex = 1;
            this.BTNscriptclear.Text = "Clear";
            this.BTNscriptclear.UseVisualStyleBackColor = true;
            this.BTNscriptclear.Click += new System.EventHandler(this.BTNscriptclear_Click);
            // 
            // RTBscript
            // 
            this.RTBscript.BackColor = System.Drawing.SystemColors.Control;
            this.RTBscript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RTBscript.Cursor = System.Windows.Forms.Cursors.Default;
            this.RTBscript.DetectUrls = false;
            this.RTBscript.Location = new System.Drawing.Point(4, 4);
            this.RTBscript.MaxLength = 0;
            this.RTBscript.Name = "RTBscript";
            this.RTBscript.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RTBscript.Size = new System.Drawing.Size(273, 397);
            this.RTBscript.TabIndex = 0;
            this.RTBscript.Text = "";
            // 
            // TBPKey
            // 
            this.TBPKey.Controls.Add(this.CBland);
            this.TBPKey.Controls.Add(this.CBfly);
            this.TBPKey.Controls.Add(this.label1);
            this.TBPKey.Controls.Add(this.label2);
            this.TBPKey.Controls.Add(this.CBselectyourself);
            this.TBPKey.Controls.Add(this.CBrest);
            this.TBPKey.Controls.Add(this.CBloot);
            this.TBPKey.Controls.Add(this.CBselect);
            this.TBPKey.Controls.Add(this.CBforward);
            this.TBPKey.Controls.Add(this.label43);
            this.TBPKey.Controls.Add(this.label6);
            this.TBPKey.Controls.Add(this.label4);
            this.TBPKey.Controls.Add(this.label40);
            this.TBPKey.Controls.Add(this.label39);
            this.TBPKey.Location = new System.Drawing.Point(4, 25);
            this.TBPKey.Name = "TBPKey";
            this.TBPKey.Size = new System.Drawing.Size(745, 473);
            this.TBPKey.TabIndex = 9;
            this.TBPKey.Text = "Key";
            this.TBPKey.UseVisualStyleBackColor = true;
            // 
            // CBland
            // 
            this.CBland.Location = new System.Drawing.Point(400, 63);
            this.CBland.MultiKeys = false;
            this.CBland.Name = "CBland";
            this.CBland.SetText = "Wait on key down";
            this.CBland.Size = new System.Drawing.Size(121, 21);
            this.CBland.TabIndex = 22;
            this.CBland.ToolTip = true;
            this.CBland.UseVisualStyleBackColor = true;
            // 
            // CBfly
            // 
            this.CBfly.Location = new System.Drawing.Point(400, 35);
            this.CBfly.MultiKeys = false;
            this.CBfly.Name = "CBfly";
            this.CBfly.SetText = "Wait on key down";
            this.CBfly.Size = new System.Drawing.Size(121, 21);
            this.CBfly.TabIndex = 21;
            this.CBfly.ToolTip = true;
            this.CBfly.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(309, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Fly";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Land";
            // 
            // CBselectyourself
            // 
            this.CBselectyourself.Location = new System.Drawing.Point(95, 120);
            this.CBselectyourself.MultiKeys = false;
            this.CBselectyourself.Name = "CBselectyourself";
            this.CBselectyourself.SetText = "Wait on key down";
            this.CBselectyourself.Size = new System.Drawing.Size(121, 21);
            this.CBselectyourself.TabIndex = 18;
            this.CBselectyourself.ToolTip = true;
            this.CBselectyourself.UseVisualStyleBackColor = true;
            // 
            // CBrest
            // 
            this.CBrest.Location = new System.Drawing.Point(95, 92);
            this.CBrest.MultiKeys = false;
            this.CBrest.Name = "CBrest";
            this.CBrest.SetText = "Wait on key down";
            this.CBrest.Size = new System.Drawing.Size(121, 21);
            this.CBrest.TabIndex = 17;
            this.CBrest.ToolTip = true;
            this.CBrest.UseVisualStyleBackColor = true;
            // 
            // CBloot
            // 
            this.CBloot.Location = new System.Drawing.Point(95, 66);
            this.CBloot.MultiKeys = false;
            this.CBloot.Name = "CBloot";
            this.CBloot.SetText = "Wait on key down";
            this.CBloot.Size = new System.Drawing.Size(121, 21);
            this.CBloot.TabIndex = 16;
            this.CBloot.ToolTip = true;
            this.CBloot.UseVisualStyleBackColor = true;
            // 
            // CBselect
            // 
            this.CBselect.Location = new System.Drawing.Point(95, 39);
            this.CBselect.MultiKeys = false;
            this.CBselect.Name = "CBselect";
            this.CBselect.SetText = "Wait on key down";
            this.CBselect.Size = new System.Drawing.Size(121, 21);
            this.CBselect.TabIndex = 15;
            this.CBselect.ToolTip = true;
            this.CBselect.UseVisualStyleBackColor = true;
            // 
            // CBforward
            // 
            this.CBforward.Location = new System.Drawing.Point(95, 12);
            this.CBforward.MultiKeys = false;
            this.CBforward.Name = "CBforward";
            this.CBforward.SetText = "Wait on key down";
            this.CBforward.Size = new System.Drawing.Size(121, 21);
            this.CBforward.TabIndex = 14;
            this.CBforward.ToolTip = true;
            this.CBforward.UseVisualStyleBackColor = true;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(4, 96);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 13);
            this.label43.TabIndex = 13;
            this.label43.Text = "Rest";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Select Yourself";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Loot";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(4, 43);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(71, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "Select Target";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(70, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Auto Forward";
            // 
            // TBPLog
            // 
            this.TBPLog.BackColor = System.Drawing.SystemColors.Control;
            this.TBPLog.Controls.Add(this.BTNlogclear);
            this.TBPLog.Controls.Add(this.RTBlog);
            this.TBPLog.Controls.Add(this.label37);
            this.TBPLog.Controls.Add(this.label36);
            this.TBPLog.Controls.Add(this.label35);
            this.TBPLog.Controls.Add(this.label34);
            this.TBPLog.Controls.Add(this.LogUse);
            this.TBPLog.Controls.Add(this.LogGather);
            this.TBPLog.Controls.Add(this.LogHunting);
            this.TBPLog.Controls.Add(this.LogSystem);
            this.TBPLog.Location = new System.Drawing.Point(4, 25);
            this.TBPLog.Name = "TBPLog";
            this.TBPLog.Padding = new System.Windows.Forms.Padding(3);
            this.TBPLog.Size = new System.Drawing.Size(745, 473);
            this.TBPLog.TabIndex = 1;
            this.TBPLog.Text = "Log";
            // 
            // BTNlogclear
            // 
            this.BTNlogclear.Location = new System.Drawing.Point(6, 378);
            this.BTNlogclear.Name = "BTNlogclear";
            this.BTNlogclear.Size = new System.Drawing.Size(581, 23);
            this.BTNlogclear.TabIndex = 11;
            this.BTNlogclear.Text = "Clear";
            this.BTNlogclear.UseVisualStyleBackColor = true;
            this.BTNlogclear.Click += new System.EventHandler(this.BTNlogclear_Click);
            // 
            // RTBlog
            // 
            this.RTBlog.BackColor = System.Drawing.SystemColors.Control;
            this.RTBlog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RTBlog.Cursor = System.Windows.Forms.Cursors.Default;
            this.RTBlog.DetectUrls = false;
            this.RTBlog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RTBlog.Location = new System.Drawing.Point(6, 6);
            this.RTBlog.Name = "RTBlog";
            this.RTBlog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RTBlog.Size = new System.Drawing.Size(581, 366);
            this.RTBlog.TabIndex = 10;
            this.RTBlog.Text = "";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label37.Location = new System.Drawing.Point(632, 104);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(38, 20);
            this.label37.TabIndex = 8;
            this.label37.Text = "Use";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.Location = new System.Drawing.Point(611, 75);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 20);
            this.label36.TabIndex = 6;
            this.label36.Text = "Gather";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label35.Location = new System.Drawing.Point(605, 46);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(65, 20);
            this.label35.TabIndex = 4;
            this.label35.Text = "Hunting";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.Location = new System.Drawing.Point(608, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(62, 20);
            this.label34.TabIndex = 2;
            this.label34.Text = "System";
            // 
            // LogUse
            // 
            this.LogUse.BackColor = System.Drawing.Color.Blue;
            this.LogUse.Location = new System.Drawing.Point(676, 104);
            this.LogUse.Name = "LogUse";
            this.LogUse.Size = new System.Drawing.Size(29, 20);
            this.LogUse.TabIndex = 9;
            this.LogUse.TabStop = false;
            this.LogUse.Click += new System.EventHandler(this.LogUse_Click);
            // 
            // LogGather
            // 
            this.LogGather.BackColor = System.Drawing.Color.Green;
            this.LogGather.Location = new System.Drawing.Point(676, 75);
            this.LogGather.Name = "LogGather";
            this.LogGather.Size = new System.Drawing.Size(29, 20);
            this.LogGather.TabIndex = 7;
            this.LogGather.TabStop = false;
            this.LogGather.Click += new System.EventHandler(this.LogGather_Click);
            // 
            // LogHunting
            // 
            this.LogHunting.BackColor = System.Drawing.Color.Red;
            this.LogHunting.Location = new System.Drawing.Point(676, 46);
            this.LogHunting.Name = "LogHunting";
            this.LogHunting.Size = new System.Drawing.Size(29, 20);
            this.LogHunting.TabIndex = 5;
            this.LogHunting.TabStop = false;
            this.LogHunting.Click += new System.EventHandler(this.LogHunting_Click);
            // 
            // LogSystem
            // 
            this.LogSystem.BackColor = System.Drawing.Color.Purple;
            this.LogSystem.Location = new System.Drawing.Point(676, 19);
            this.LogSystem.Name = "LogSystem";
            this.LogSystem.Size = new System.Drawing.Size(29, 20);
            this.LogSystem.TabIndex = 3;
            this.LogSystem.TabStop = false;
            this.LogSystem.Click += new System.EventHandler(this.LogSystem_Click);
            // 
            // TBPAethertapping
            // 
            this.TBPAethertapping.Controls.Add(this.groupBox5);
            this.TBPAethertapping.Location = new System.Drawing.Point(4, 25);
            this.TBPAethertapping.Name = "TBPAethertapping";
            this.TBPAethertapping.Size = new System.Drawing.Size(745, 473);
            this.TBPAethertapping.TabIndex = 10;
            this.TBPAethertapping.Text = "Aethertapping";
            this.TBPAethertapping.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.BTNAddAetherSafeSpot);
            this.groupBox5.Controls.Add(this.ListAetherSafeSpot);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(739, 207);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Aethertapping";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.CBaetherReturn);
            this.groupBox8.Location = new System.Drawing.Point(599, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(134, 47);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Option";
            // 
            // CBaetherReturn
            // 
            this.CBaetherReturn.AutoSize = true;
            this.CBaetherReturn.Location = new System.Drawing.Point(6, 19);
            this.CBaetherReturn.Name = "CBaetherReturn";
            this.CBaetherReturn.Size = new System.Drawing.Size(115, 17);
            this.CBaetherReturn.TabIndex = 10;
            this.CBaetherReturn.Text = "Return if script end";
            this.CBaetherReturn.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(181, 68);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // BTNAddAetherSafeSpot
            // 
            this.BTNAddAetherSafeSpot.Location = new System.Drawing.Point(6, 159);
            this.BTNAddAetherSafeSpot.Name = "BTNAddAetherSafeSpot";
            this.BTNAddAetherSafeSpot.Size = new System.Drawing.Size(135, 23);
            this.BTNAddAetherSafeSpot.TabIndex = 8;
            this.BTNAddAetherSafeSpot.Text = "Add Safe Spot";
            this.BTNAddAetherSafeSpot.UseVisualStyleBackColor = true;
            this.BTNAddAetherSafeSpot.Click += new System.EventHandler(this.BTNAddAtherSafeSpot_Click);
            // 
            // ListAetherSafeSpot
            // 
            this.ListAetherSafeSpot.ContextMenuStrip = this.CMSAetherSaveSpot;
            this.ListAetherSafeSpot.FormattingEnabled = true;
            this.ListAetherSafeSpot.Location = new System.Drawing.Point(6, 45);
            this.ListAetherSafeSpot.Name = "ListAetherSafeSpot";
            this.ListAetherSafeSpot.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ListAetherSafeSpot.Size = new System.Drawing.Size(135, 108);
            this.ListAetherSafeSpot.TabIndex = 7;
            // 
            // CMSAetherSaveSpot
            // 
            this.CMSAetherSaveSpot.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMdelete});
            this.CMSAetherSaveSpot.Name = "CMSAetherSaveSpot";
            this.CMSAetherSaveSpot.Size = new System.Drawing.Size(108, 26);
            // 
            // TSMdelete
            // 
            this.TSMdelete.Name = "TSMdelete";
            this.TSMdelete.Size = new System.Drawing.Size(107, 22);
            this.TSMdelete.Text = "Delete";
            this.TSMdelete.Click += new System.EventHandler(this.TSMdelete_Click);
            // 
            // BTNStartBot
            // 
            this.BTNStartBot.Location = new System.Drawing.Point(12, 529);
            this.BTNStartBot.Name = "BTNStartBot";
            this.BTNStartBot.Size = new System.Drawing.Size(753, 23);
            this.BTNStartBot.TabIndex = 10;
            this.BTNStartBot.Text = "Start Bot";
            this.BTNStartBot.UseVisualStyleBackColor = true;
            this.BTNStartBot.Click += new System.EventHandler(this.BTNStartBot_Click);
            // 
            // TimerScript
            // 
            this.TimerScript.Tick += new System.EventHandler(this.TimerScript_Tick);
            // 
            // bOTToolStripMenuItem
            // 
            this.bOTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MIexit,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.bOTToolStripMenuItem.Name = "bOTToolStripMenuItem";
            this.bOTToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.bOTToolStripMenuItem.Text = "BOT";
            // 
            // MIexit
            // 
            this.MIexit.Name = "MIexit";
            this.MIexit.Size = new System.Drawing.Size(140, 22);
            this.MIexit.Text = "Exit";
            this.MIexit.Click += new System.EventHandler(this.MIexit_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.saveToolStripMenuItem.Text = "Save Setting";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadToolStripMenuItem.Text = "Load Setting";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // mainToolStripMenuItem
            // 
            this.mainToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MIprofile,
            this.MItargetInfo,
            this.MIalwaysOnTop});
            this.mainToolStripMenuItem.Name = "mainToolStripMenuItem";
            this.mainToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.mainToolStripMenuItem.Text = "Tools";
            // 
            // MIprofile
            // 
            this.MIprofile.Name = "MIprofile";
            this.MIprofile.Size = new System.Drawing.Size(151, 22);
            this.MIprofile.Text = "Profile";
            this.MIprofile.Click += new System.EventHandler(this.MIprofile_Click);
            // 
            // MItargetInfo
            // 
            this.MItargetInfo.Name = "MItargetInfo";
            this.MItargetInfo.Size = new System.Drawing.Size(151, 22);
            this.MItargetInfo.Text = "Target info";
            this.MItargetInfo.Click += new System.EventHandler(this.MItargetInfo_Click);
            // 
            // MIalwaysOnTop
            // 
            this.MIalwaysOnTop.Name = "MIalwaysOnTop";
            this.MIalwaysOnTop.Size = new System.Drawing.Size(151, 22);
            this.MIalwaysOnTop.Text = "Always on Top";
            this.MIalwaysOnTop.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
            // 
            // TrayIcon
            // 
            this.TrayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.TrayIcon.BalloonTipText = "You have successfully minimized MyBot";
            this.TrayIcon.BalloonTipTitle = "Minimize to Tray App";
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "MyBot";
            this.TrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseDoubleClick);
            // 
            // CMSskill
            // 
            this.CMSskill.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attackToolStripMenuItem,
            this.buffToolStripMenuItem});
            this.CMSskill.Name = "CMSskill";
            this.CMSskill.Size = new System.Drawing.Size(109, 48);
            // 
            // attackToolStripMenuItem
            // 
            this.attackToolStripMenuItem.Name = "attackToolStripMenuItem";
            this.attackToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.attackToolStripMenuItem.Text = "Attack";
            this.attackToolStripMenuItem.Click += new System.EventHandler(this.attackToolStripMenuItem_Click);
            // 
            // buffToolStripMenuItem
            // 
            this.buffToolStripMenuItem.Name = "buffToolStripMenuItem";
            this.buffToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.buffToolStripMenuItem.Text = "Buff";
            this.buffToolStripMenuItem.Click += new System.EventHandler(this.buffToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 555);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(775, 22);
            this.statusStrip1.TabIndex = 46;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatCpu
            // 
            this.StatCpu.Name = "StatCpu";
            this.StatCpu.Size = new System.Drawing.Size(33, 17);
            this.StatCpu.Text = "CPU:";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel3.Text = "0";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bOTToolStripMenuItem1,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(775, 24);
            this.menuStrip1.TabIndex = 47;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bOTToolStripMenuItem1
            // 
            this.bOTToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.saveSettingToolStripMenuItem,
            this.loadSettingToolStripMenuItem});
            this.bOTToolStripMenuItem1.Name = "bOTToolStripMenuItem1";
            this.bOTToolStripMenuItem1.Size = new System.Drawing.Size(41, 20);
            this.bOTToolStripMenuItem1.Text = "BOT";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.MIexit_Click);
            // 
            // saveSettingToolStripMenuItem
            // 
            this.saveSettingToolStripMenuItem.Name = "saveSettingToolStripMenuItem";
            this.saveSettingToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.saveSettingToolStripMenuItem.Text = "Save Setting";
            this.saveSettingToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadSettingToolStripMenuItem
            // 
            this.loadSettingToolStripMenuItem.Name = "loadSettingToolStripMenuItem";
            this.loadSettingToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadSettingToolStripMenuItem.Text = "Load Setting";
            this.loadSettingToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profiToolStripMenuItem,
            this.targetInfoToolStripMenuItem,
            this.alwaysOnTopToolStripMenuItem,
            this.metroUiToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // profiToolStripMenuItem
            // 
            this.profiToolStripMenuItem.Name = "profiToolStripMenuItem";
            this.profiToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.profiToolStripMenuItem.Text = "Profile";
            this.profiToolStripMenuItem.Click += new System.EventHandler(this.MIprofile_Click);
            // 
            // targetInfoToolStripMenuItem
            // 
            this.targetInfoToolStripMenuItem.Name = "targetInfoToolStripMenuItem";
            this.targetInfoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.targetInfoToolStripMenuItem.Text = "Target info";
            this.targetInfoToolStripMenuItem.Click += new System.EventHandler(this.MItargetInfo_Click);
            // 
            // alwaysOnTopToolStripMenuItem
            // 
            this.alwaysOnTopToolStripMenuItem.Name = "alwaysOnTopToolStripMenuItem";
            this.alwaysOnTopToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.alwaysOnTopToolStripMenuItem.Text = "Always on top";
            this.alwaysOnTopToolStripMenuItem.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
            // 
            // metroUiToolStripMenuItem
            // 
            this.metroUiToolStripMenuItem.Name = "metroUiToolStripMenuItem";
            this.metroUiToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.metroUiToolStripMenuItem.Text = "MetroUi";
            this.metroUiToolStripMenuItem.Click += new System.EventHandler(this.metroUiToolStripMenuItem_Click);
            // 
            // MyBot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 577);
            this.Controls.Add(this.BTNStartBot);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.TBCMainWindow);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MyBot";
            this.Text = "MyBot- Aion FreeToPlay";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MyBot_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MyBot_FormClosed);
            this.Load += new System.EventHandler(this.MyBot_Load);
            this.Resize += new System.EventHandler(this.TrayMinimizerForm_Resize);
            this.TBCMainWindow.ResumeLayout(false);
            this.TBPInformation.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.GBtarget.ResumeLayout(false);
            this.GBtarget.PerformLayout();
            this.GBplayer.ResumeLayout(false);
            this.GBplayer.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.TCplayer.ResumeLayout(false);
            this.TCplayer.PerformLayout();
            this.TBPWaypoint.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TBPPotion.ResumeLayout(false);
            this.TBPPotion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddPotionPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpotion)).EndInit();
            this.DGVskillcontext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AddPotionCooldown)).EndInit();
            this.TBPSkill.ResumeLayout(false);
            this.TBPSkill.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVskill)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVbuff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddChainWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCastTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddCooldown)).EndInit();
            this.TBPScript.ResumeLayout(false);
            this.TBPScript.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NRscriptstep)).EndInit();
            this.TBPKey.ResumeLayout(false);
            this.TBPKey.PerformLayout();
            this.TBPLog.ResumeLayout(false);
            this.TBPLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogHunting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogSystem)).EndInit();
            this.TBPAethertapping.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.CMSAetherSaveSpot.ResumeLayout(false);
            this.CMSskill.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TBCMainWindow;
        private System.Windows.Forms.TabPage TBPLog;
        private System.Windows.Forms.TabPage TBPWaypoint;
        private System.Windows.Forms.TabPage TBPPotion;
        private System.Windows.Forms.TabPage TBPSkill;
        private System.Windows.Forms.TabPage TBPHunting;
        private System.Windows.Forms.ToolTip ToolTipStat;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabPage TBPScript;
        private System.Windows.Forms.Button BTNscriptclear;
        private System.Windows.Forms.Button BTNrecord;
        private System.Windows.Forms.Label label38;
        private System.ComponentModel.BackgroundWorker BWscript;
        private System.Windows.Forms.Timer TimerScript;
        private System.Windows.Forms.Button BTNscriptload;
        private System.Windows.Forms.Button BTNscriptsave;
        private System.Windows.Forms.Button BTNwaybrowse;
        public System.Windows.Forms.RichTextBox RTBlog;
        public System.Windows.Forms.PictureBox LogSystem;
        public System.Windows.Forms.PictureBox LogGather;
        public System.Windows.Forms.PictureBox LogHunting;
        public System.Windows.Forms.PictureBox LogUse;
        private System.Windows.Forms.TabPage TBPKey;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        public System.Windows.Forms.TextBox TBactionway;
        private System.Windows.Forms.Button BTNlogclear;
        private System.Windows.Forms.ToolStripMenuItem mainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MItargetInfo;
        private System.Windows.Forms.ToolStripMenuItem MIalwaysOnTop;
        public System.Windows.Forms.ToolStripMenuItem MIprofile;
        private System.Windows.Forms.TabPage TBPInformation;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ToolStripMenuItem bOTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MIexit;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox TBdeathway;
        private System.Windows.Forms.Button BTNdeathbrowse;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.RichTextBox RTBscript;
        public System.Windows.Forms.NumericUpDown NRscriptstep;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BTNscriptadd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.Button BTNskilladd;
        public System.Windows.Forms.ComboBox AddSkillbar;
        private System.Windows.Forms.ContextMenuStrip DGVskillcontext;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown AddChainWait;
        private System.Windows.Forms.NumericUpDown AddCastTime;
        private System.Windows.Forms.NumericUpDown AddCooldown;
        private System.Windows.Forms.NumericUpDown AddChainLevel;
        private System.Windows.Forms.ToolStripMenuItem upToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downToolStripMenuItem;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.DataGridView DGVskill;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.DataGridView DGVbuff;
        private System.Windows.Forms.ContextMenuStrip CMSskill;
        private System.Windows.Forms.ToolStripMenuItem attackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buffToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button BTNfullscreen;
        private System.Windows.Forms.TextBox TBwindowx;
        private System.Windows.Forms.TextBox TBwindowy;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button BTNresize;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox TBwindowheight;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TBwindowwidth;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown AddPotionCooldown;
        private System.Windows.Forms.Button BTNpotionadd;
        public System.Windows.Forms.ComboBox AddPotionSkillbar;
        public System.Windows.Forms.DataGridView DGVpotion;
        public System.Windows.Forms.ComboBox AddPotionEffect;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown AddPotionPercent;
        private System.Windows.Forms.CheckBox BTNHide;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        public System.Windows.Forms.ToolStripStatusLabel StatCpu;
        private System.Windows.Forms.CheckBox CBOnCombat;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TCplayer;
        public System.Windows.Forms.Label LBflytype;
        public System.Windows.Forms.Label LBloggin;
        private System.Windows.Forms.TabPage TCtarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackSkillbar;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackCooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackCastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackChain;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackChainWait;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttackLastUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionSkillbar;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionEffect;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionCooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionCastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn PotionLastUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffSkillbar;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffCooldown;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffCastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffChainLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffChainWait;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BuffOnCombat;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuffLastUsed;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bOTToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem targetInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alwaysOnTopToolStripMenuItem;
        public System.Windows.Forms.GroupBox GBtarget;
        public System.Windows.Forms.GroupBox GBplayer;
        public System.Windows.Forms.Panel PNLdplevel;
        public System.Windows.Forms.Panel PNLclass;
        public MB.exProgressBar.exProgressBar DRWhp;
        public MB.exProgressBar.exProgressBar DRWtargetmp;
        public MB.exProgressBar.exProgressBar DRWtargethp;
        public MB.exProgressBar.exProgressBar DRWfly;
        public MB.exProgressBar.exProgressBar DRWexp;
        public MB.exProgressBar.exProgressBar DRWdp;
        public MB.exProgressBar.exProgressBar DRWmp;
        public MB.KeyButton.KeyButton CBselectyourself;
        public MB.KeyButton.KeyButton CBrest;
        public MB.KeyButton.KeyButton CBloot;
        public MB.KeyButton.KeyButton CBselect;
        public MB.KeyButton.KeyButton CBforward;
        private MB.KeyButton.KeyButton AddPotionKey;
        private MB.KeyButton.KeyButton AddKey;
        public MB.KeyButton.KeyButton CBland;
        public MB.KeyButton.KeyButton CBfly;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CBscriptadd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage TBPAethertapping;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button BTNAddAetherSafeSpot;
        private System.Windows.Forms.ContextMenuStrip CMSAetherSaveSpot;
        private System.Windows.Forms.ToolStripMenuItem TSMdelete;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.ListBox ListAetherSafeSpot;
        private System.Windows.Forms.GroupBox groupBox8;
        public System.Windows.Forms.CheckBox CBaetherReturn;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button BTNStartBot;
        private System.Windows.Forms.ToolStripMenuItem metroUiToolStripMenuItem;
    }
}

