﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BOT
{
    public class Script
    {


        #region Property
        private List<string[]> parameter = new List<string[]>();
        private List<string> command = new List<string>();
        private List<PointXYZ> scriptList = new List<PointXYZ>();
        private string[] script;

        public List<string> Command
        {
            get { return command; }
            internal set { command = value; }
        }

        public List<string[]> Parameter
        {
            get { return parameter; }
            internal set { parameter = value; }
        }

        public bool IsLooped
        {
            get
            {
                if (scriptList.Last().Distance(scriptList.First()) < Properties.Settings.Default.MaxDistance)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public int Count
        {
            get { return FileScript.Count(); }
        }

        public string[] FileScript
        {
            get { return script; }
            set { script = value; }
        }
        #endregion Property

        #region Constructors

        public Script(string fileScript)
        {
            FileScript = File.ReadAllLines(fileScript);
            foreach (string line in FileScript)
            {
                Command.Add(GetCommand(line));
                Parameter.Add(GetParameters(line).Split(';'));
                if (Command.Last().Contains(Properties.ScriptSetting.Default.go))
                {
                    string[] par = Parameter.Last();
                    scriptList.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
                }
            }
        }
        public Script(string[] script)
        {
            FileScript = script;
            foreach (string line in script)
            {
                Command.Add(GetCommand(line));
                Parameter.Add(GetParameters(line).Split(';'));
                if (Command.Last().Contains(Properties.ScriptSetting.Default.go))
                {
                    string[] par = Parameter.Last();
                    scriptList.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
                }
            }
        }


        public Script(ListBox script)
        {
            foreach (string line in script.Items)
            {
                Command.Add(string.IsNullOrEmpty(GetCommand(line)) ? Properties.ScriptSetting.Default.go : GetCommand(line));
                Parameter.Add(GetParameters(line).Split(';'));
                if (Command.Last().Contains(Properties.ScriptSetting.Default.go))
                {
                    string[] par = Parameter.Last();
                    scriptList.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
                }
            }

            List<string> s = new List<string>();
            for (int i = 0; i < script.Items.Count; i++)
            {
                s.Add(Command[i] + "(" + Parameter[i] + ")");
            }

            FileScript = s.ToArray();
        }
        public Script(List<PointXYZ> script)
        {
            List<string> s = new List<string>();
            scriptList = script;
            foreach (PointXYZ line in script)
            {
                Command.Add(Properties.ScriptSetting.Default.go);
                Parameter.Add(new string[] { line.X.ToString(), line.Y.ToString(), line.Z.ToString() });

                s.Add(Command.Last() + "(" + parameter.ToString() + ")");

            }

            FileScript = s.ToArray();
        }
        #endregion Constructors

       

        private async Task Do(int i)
        {
            if (Command[i].Contains(Properties.ScriptSetting.Default.go))
            {
                if (Bot.Status == Bot.BotStatus.DeathWaypoint)
                {
                    await Go(float.Parse(Parameter[i][0]), float.Parse(Parameter[i][1]), float.Parse(Parameter[i][2]));
                }
                else if (Bot.Status == Bot.BotStatus.ActionWaypoint)
                {
                    await Hunting.Go(float.Parse(Parameter[i][0]), float.Parse(Parameter[i][1]), float.Parse(Parameter[i][2]));
                }
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.delay))
            {
                await Task.Delay(int.Parse(Parameter[i][0]));
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.stop))
            {
                Key.GoForward(false);
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.press))
            {
                Key.KeyPress(Parameter[i][0]);
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.buff))
            {
                await Buff.Use();
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.loot))
            {
                while (Value.Target.Has != 0 && Bot.Status != Bot.BotStatus.Idle)
                {
                    Key.Loot();
                    await Task.Delay(500);
                }
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.fly))
            {
                Key.Fly();
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.land))
            {
                Key.Land();
                await Task.Delay(1000);
            }
        }

        public async Task Start(bool repeat = false)
        {
            do
            {
                for (int i = FindNear().Index; i < Count && Bot.Status != Bot.BotStatus.Idle; i++)
                {
                    if (i == Count - 1 && repeat && !IsLooped)
                    {
                        for (; i >= 0 && Bot.Status != Bot.BotStatus.Idle; i--)
                        {
                            await Do(i);
                        }
                    }
                    else
                    {
                        await Do(i);
                        if (i == Count - 1 && repeat)
                            i = -1;
                    }
                    await Task.Delay(100);
                }
            } while (repeat && Bot.Status != Bot.BotStatus.Idle);
        }

        private async Task Go(float xCoord, float yCoord, float zCoord)
        {
            double dis = Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, xCoord, yCoord);
            while (dis > 5 && Bot.Status != Bot.BotStatus.Idle)
            {
                Key.GoForward();
                dis = Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, xCoord, yCoord);
                Value.Player.Camera.Set(xCoord, yCoord);
                await Task.Delay(100);
            }
        }

        public PointAndDistance FindNear(PointXYZ point = null)
        {
            point = point ?? new PointXYZ(Value.Player.Position.X, Value.Player.Position.Y, Value.Player.Position.Z);
            PointAndDistance p = new PointAndDistance();
            PointXYZ pointscript;
            for (int i = 0; i < FileScript.Length; i++)
            {
                if (GetCommand(FileScript[i]).Contains(Properties.ScriptSetting.Default.go))
                {
                    string[] s = GetParameters(FileScript[i]).Split(';');
                    pointscript = new PointXYZ(s[0], s[1], s[2]);
                    float dis2 = pointscript.Distance(point.X, point.Y, point.Z);
                    if (p.Distance > dis2)
                    {
                        p.Distance = dis2;
                        p.Point = pointscript;
                        p.Index = i;
                    }
                }
            }
            return p;
        }




        public Script CreatePathToSafeSpot(ListBox safelist)
        {

            List<PointXYZ> list = new List<PointXYZ>();
            List<PointXYZ> list2 = new List<PointXYZ>();



            PointXYZ safe = new Script(safelist).FindNear().Point;
            var nears = FindNear();
            var safes = FindNear(safe);
            PointXYZ nearScript = nears.Point;
            PointXYZ safeScript = safes.Point;

            int indexNearScript = nears.Index;
            int indexsafeScript = safes.Index;


            //Console.WriteLine("-----------------FindNear Class---------------------");
            //Console.WriteLine("safe " + safe);
            //Console.WriteLine("safeScript " + safeScript);
            //Console.WriteLine("nearScript " + nearScript);
            //Console.WriteLine("indexNearScript " + indexNearScript);
            //Console.WriteLine("indexsafeScript " + indexsafeScript);
            //Console.WriteLine("--------------------------------------------------");


            if (indexsafeScript - indexNearScript == 0)
            {
                list.Add(safe);
                return new Script(list);
            }

            for (int i = indexNearScript; i < scriptList.Count; i++)
            {
                list.Add(scriptList[i]);
                if (safeScript.Compare(scriptList[i]))
                {
                    list.Add(safe);
                    break;
                }

                if (i == scriptList.Count - 1 && !Program.bot.CBaetherReturn.Checked)
                    i = -1;
                else if (i == scriptList.Count - 1)
                    list.Clear();
            }

            for (int i = indexNearScript; i >= 0; i--)
            {
                list2.Add(scriptList[i]);
                if (safeScript.Compare(scriptList[i]))
                {
                    list2.Add(safe);
                    break;
                }

                if (i == 0 && !Program.bot.CBaetherReturn.Checked)
                    i = scriptList.Count;
                else if (i == 0)
                    list2.Clear();
            }

            if (!list2.Any() || (list.Count < list2.Count && list.Any()))
                return new Script(list);
            else
                return new Script(list);
        }


        private int IndexOf(List<PointXYZ> script, PointXYZ safeScript)
        {
            foreach (var item in script)
            {
                if (item.X == safeScript.X && item.Y == safeScript.Y && item.Z == safeScript.Z)
                {
                    return script.IndexOf(item);
                }
            }
            return -1;
        }


        private string GetParameters(string s)
        {
            return Regex.Match(s, @"(?<=\()(.*?)(?=\))").ToString();
        }

        private string GetCommand(string s)
        {
            return Regex.Match(s, @"[^(\)]*").ToString().ToLower();
        }


        public static bool CheckScript(Script s)
        {
            for (int i = 0; i < s.Count; i++)
            {
                if (s.Command[i].Contains(Properties.ScriptSetting.Default.go)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.delay)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.stop)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.press)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.buff)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.loot)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.fly)) { }
                else if (s.Command[i].Contains(Properties.ScriptSetting.Default.land)) { }
                else
                    return false;
            }
            return true;
        }
        public static bool CheckScript(string ss)
        {
            if (File.Exists(ss))
            {
                Script s = new Script(ss);
                for (int i = 0; i < s.Count; i++)
                {
                    if (s.Command[i].Contains(Properties.ScriptSetting.Default.go)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.delay)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.stop)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.press)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.buff)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.loot)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.fly)) { }
                    else if (s.Command[i].Contains(Properties.ScriptSetting.Default.land)) { }
                    else
                        return false;
                }
                return true;
            }
            return false;
        }




        public class PointAndDistance
        {
            PointXYZ point = new PointXYZ();
            float distance = float.MaxValue;
            int index = -1;
            public PointXYZ Point
            {
                get
                {
                    return point;
                }

                set
                {
                    point = value;
                }
            }

            public float Distance
            {
                get
                {
                    return distance;
                }

                set
                {
                    distance = value;
                }
            }

            public int Index
            {
                get
                {
                    return index;
                }

                set
                {
                    index = value;
                }
            }
        }

    }





}
