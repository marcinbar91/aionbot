﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOT
{
    public class Party
    {
        public int Count;
        public int MaxHP;
        public int CurHP;
        public int MaxMP;
        public int CurMP;

        public int FlightMax;
        public int FlightCur;

        public string Name;



  
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Party objAsPart = obj as Party;
            if (objAsPart == null) return false;
            else
            {
                if (objAsPart == null) return false;
                return (this.Name.Equals(objAsPart.Name));
            }
        }

        public static List<Party> Members = new List<Party>();


        public static bool Exist(string name)
        {
            return Members.Contains(new Party() { Name = name });
        }

        public static void AddPartyMember(int cHP , string name)
        {
            Party party = new Party();
            party.CurHP = cHP;
            party.Name = name;
            Members.Add(party);
        }


        public static void RemovePartyMember(string name)
        {
            Members.Remove(new Party() { Name=name});
        }
    }
}
