﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MB.ProcessControl;
using System.Windows.Forms;
using System.Threading;

namespace BOT
{
    class Key
    {
        static Keybord ke = new Keybord(Launcher.processHandlekey);

        public static void KeyPress(Keys key)
        {
            //Launcher.aionProcess.SendMessage(Keys.D0);
            ke.SendMessage(key);
        }
        public static void KeyPress(string str)
        {
            ke.SendMessage(str);
        }

        public static void KeyPressDown(Keys key)
        {
            ke.SendMessageDown(key);
        }

        public static void KeyPressUp(Keys key)
        {
            ke.SendMessageUp(key);
        }
        public static void KeyPressDown(string key)
        {
            ke.SendMessageDown(key);
        }

        public static void KeyPressUp(string key)
        {
            ke.SendMessageUp(key);
        }

        public static void GoForward2(bool move = true)
        {
            if (move && Value.Player.Movement == 0)
            {
                ke.SendMessage(Program.bot.CBforward.GetKeys(0));
            }
            if (move == false && Value.Player.Movement == 4)
            {
                ke.SendMessage(Program.bot.CBforward.GetKeys(0));
            }
        }

        static object o = new object();
        private static int _lockFlag = 0; // 0 - free
        public async static void GoForward(bool move = true)
        {
            if (Interlocked.CompareExchange(ref _lockFlag, 1, 0) == 0)
            {
                // only 1 thread will enter here without locking the object/put the
                // other threads to sleep.

                Monitor.Enter(o);
                Keys forward = Program.bot.CBforward.GetKeys(0);
                float x = Value.Player.Position.X, y = Value.Player.Position.Y;
                await Task.Delay(500);
                bool cur;
                if (Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, x, y) == 0)
                    cur = false;
                else
                    cur = true;

                if (move && cur == false)
                {

                    KeyPress(forward);
                }
                else if (move == false && cur)
                    KeyPress(forward);
              //  Monitor.Exit(o);

                // free the lock.
                Interlocked.Decrement(ref _lockFlag);
            }
         
        }

        public static void SelectTarget()
        {
            KeyPress(Program.bot.CBselect.GetKeys(0));
        }
        public static void Loot()
        {
            KeyPress(Program.bot.CBloot.GetKeys(0));
        }
        public static void Rest()
        {
            KeyPress(Program.bot.CBrest.GetKeys(0));
        }
        public static void SelectYourself()
        {
            KeyPress(Program.bot.CBselectyourself.GetKeys(0));
        }
        public static void Fly()
        {
            KeyPress(Program.bot.CBfly.GetKeys(0));
        }
        public static void Land()
        {
            KeyPress(Program.bot.CBland.GetKeys(0));
        }



    }
}
